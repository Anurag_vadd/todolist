import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/lib/utils/date_time_to_string.dart';
import 'package:todolist_unmodified_dup/lib/utils/page.dart';
import 'package:todolist_unmodified_dup/widgets/inc/Calendar_screen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/group_screen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/modalsheet.dart';
import 'package:uuid/uuid.dart';
import '../../widgets/inc/input_screen.dart';
import '../../widgets/inc/starting_screen.dart';
import 'days_page_view.dart';
import 'days_page_controller.dart';
import 'package:flutter/services.dart';


notifyClick(){
  bool value;
  value = true;

  return value;
}




final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
final List<Notification> notifications = [];
class DayPageViewExample extends StatefulWidget {
  final DateTime date;
// final  Map<DateTime , List> events;

  const DayPageViewExample(this.date);

  @override
  _DayPageViewExampleState createState() => new _DayPageViewExampleState();
}

class _DayPageViewExampleState extends State<DayPageViewExample>     with WidgetsBindingObserver {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  final GlobalKey<_DayPageViewExampleState> _key = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//  _showSnackBar(String value) {
//
//    print("Show Snackbar here !");
//    final snackBar = new SnackBar(
//      content: new Text(value),
//      duration: new Duration(seconds: 3),
//      backgroundColor: Colors.grey,
//      action: new SnackBarAction(label: 'Ok', onPressed: (){
//        print('press Ok on SnackBar');
//      }),
//    );
//    //How to display Snackbar ?
//    _scaffoldKey.currentState.showSnackBar(snackBar);
//  }

  DaysPageController _daysPageController;
  TextEditingController controller = new TextEditingController();

  set setWanteKeep(var value) => wantKeepAlive;


List practiceFetch;

  Axis _scrollDirection;
  bool _pageSnapping;
  bool _reverse;

  static const Duration _duration = Duration(milliseconds: 400);
  static const Curve _curve = Curves.easeInOut;

  bool isSwitched = false;
  String identifier;



notification(){
  return
  isSwitched != isSwitched;
}



  incrementDay(){
      setter = setter.add(Duration(days: 1));
      print(setter);
    return setter;
  }

  decrementDay(){
      setter = setter.subtract(Duration(days: 1));
      print(setter);
    return setter;
  }

  void _showModalSheet() {
    showModalBottomSheet(
              shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return bottom();// return a StatefulWidget widget
        });
  }

  @override
  void dispose() {
    super.dispose();
//    _DayPageViewExampleState().setWanteKeep = false;
WidgetsBinding.instance.removeObserver(_DayPageViewExampleState());
//    WidgetsBinding.instance.initInstances();
    //this method not called when user press android back button or quit
    print('dispose');
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    var initializationSettingsAndroid =
    new AndroidInitializationSettings('flutter_logo');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);


    _daysPageController = new DaysPageController(
      firstDayOnInitialPage: widget.date ?? DateTime.now(),
      daysPerPage: 1
    );



    _scrollDirection = Axis.horizontal;
    _pageSnapping = true;
    _reverse = false;

  }
  final _formKey = GlobalKey<FormState>();

  getDate(DateTime date){
   return date;
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("PayLoad"),
          content: Text("Payload : $payload"),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
//    final todoappState = Provider.of<todoState>(context);


    SystemChrome.setEnabledSystemUIOverlays([]);

  incrementDay(){
        setter = setter.add(Duration(days: 1));
        print(setter);
      return setter;
    }

    decrementDay(){
        setter = setter.subtract(Duration(days: 1));
        print(setter);
      return setter;
    }
    var randomizer = new Random(); // can get a seed as a parameter

    // Integer between 0 and 100 (0 can be 100 not)
    var num = randomizer.nextInt(100);

    Future _showNotificationWithDefaultSound() async {
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
          'your channel id', 'your channel name', 'your channel description',
          importance: Importance.Max, priority: Priority.High);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
        num,
        'New Post',
        'How to Show Notification in Flutter',
//        scheduledTime,
        platformChannelSpecifics,

        payload: 'Default_Sound',

      );
    }
// Meth



    void _animate2Pageforward() => _daysPageController.animateToDay(
      (incrementDay()),
      curve: _curve,
      duration: _duration,
    );

    void _animate2Pagenegative() => _daysPageController.animateToDay(
      (decrementDay()),
      curve: _curve,
      duration: _duration,
    );
    String myPassword;

//    final secureStore = new FlutterSecureStorage();

//    readSecureStorage()async{
//
//      myPassword = await secureStore.read(key: "userData");
//
//      Map list = await secureStore.readAll();
//      print("PRINTING MAP = $list");
//    }

    return new Scaffold(
      resizeToAvoidBottomPadding: false,
        key: _scaffoldKey,

        floatingActionButton: Stack(

          children: <Widget>[
            Positioned(
              bottom: -14,
              child: InkWell(
                onTap: ()async{
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyHomePage(),
                      ));
                },
                child: Container(
                color: Colors.red,
                  width: MediaQuery.of(context).size.width *0.4,
                  height: 50,
//           color: Colors.black,
//           child:,
                ),
              ),
            ),
            Positioned(
              bottom: -14,
              right: -8,
              child: InkWell(
                onTap: (){
                  _showModalSheet();
                },
                child: Container(
                  color: Colors.red,
                  width: MediaQuery.of(context).size.width *0.35,
                  height: 50,
//           color: Colors.black,
//           child:,
                ),
              ),
            ),

            Positioned(
              right: 135.5,
              top: 675,
              child:
              FloatingActionButton(
                heroTag: UniqueKey(),

                backgroundColor: Colors.red,
                onPressed: () async{



                  List<dynamic> result1 = await Navigator.push( // string which stores the user entered value
                      context,
                      MaterialPageRoute(

//                        maintainState: true,
                        builder: (context) {

                         return InputScreen(setter); // this is the third screen
                        }, //screen which has TextField
                      ));


                  returnDate(){
                    if(result1[4] == null){
                      return setter.toIso8601String();
                    }else{
                      return result1[4];
                    }
                  }

                  String id = await secureStore.read(key: "userId");
print("PRINTING USER ID = $id");

                  Post taskPost = new Post(
//                      markComplete: false.toString(),
                      taskName: result1[0],
                      groupID: result1[3],
                      description: result1[1],
                      date: returnDate().toString(),
                      priority: result1[2].toString(),
                    userId: id
                  );

//                  print("PRINTING TASKPOST DPVE = ${taskPost.toMapUpdateWithoutDescription()}");
print("PRINTING URL = ${baseURL + createTodoUrl + "/" + id}");

                  if(result1[1] == null && result1[3] == null){
                    Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciptionAndGroup());
                    print("Printing P = $p");
                      loadMore = true;

                      numberOfTasksLeft = numberOfTasksLeft - 1;


                  }else if(result1[1] != null && result1[3] == null){
                    loadMore = true;

                    numberOfTasksLeft = numberOfTasksLeft - 1;
                    Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutGroup());
                    print("Printing P = ${p.taskName}");

                  }else if(result1[1]== null && result1[3] != null){
                    loadMore = true;
                    numberOfTasksLeft = numberOfTasksLeft - 1;
                    Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciption());


                  }else if(result1[1] != null && result1[3] != null){
                    loadMore = true;

                    numberOfTasksLeft = numberOfTasksLeft - 1;
                    Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreate());


                  }




                  Timer _timer;
                  _timer = new Timer(const Duration(microseconds: 1), () {
                        loadMore = false;
                    });

                },
                tooltip: 'Add a task',
                child: Icon(Icons.add),
                elevation: 0.0,
              ),
            ),

            Positioned(
              right: 56,
              top: 125,
              child: Transform.scale(
                scale: 0.8,
                child: FloatingActionButton(
                  heroTag: UniqueKey(),
                  backgroundColor: Colors.red,
                  onPressed: (){
                      _animate2Pagenegative();
//                      readSecureStorage();
                  },
                  tooltip: 'Previous Day',
                  child: Icon(Icons.arrow_back),
                  elevation: 0.0,
                ),
              ),

            ),

            Positioned(

              right: 0,
              top: 40,
              child: Transform.scale(
                scale: 1.2,
                child: IconButton(
                  key: UniqueKey(),
                  icon: Image.asset('assets/GroupGrid.png', width: 70, height: 70,),
                  tooltip: 'go to group screen',
                  onPressed: () {
                    Navigator.push( // string which stores the user entered value
                        context,
                        MaterialPageRoute(
                          builder: (context) => Group_Screen(), //screen which has TextField
                        ));
                  },
                ),
              )

            ),

            Positioned(

            right: 30,
              bottom: -9,

              child: IconButton(
                icon: Icon(Icons.menu, color: Colors.white,size: 30,),
                onPressed: () {
                  _showModalSheet();
                },
              ),

            ),
//
            Positioned(
                left: 50,
                bottom: 100,
                top: 701,
              child: IconButton(
                icon: Icon(Icons.calendar_today, color: Colors.white,size: 30,),
                onPressed: () {
                  Navigator.push( // string which stores the user entered value
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MyHomePage();
                        }, //screen which has TextField
                      ));
                },
              ),

            ),

            Positioned(
              right: 0,
              top: 125,
              child: Transform.scale(
                scale: 0.8,
                child: FloatingActionButton(
                  heroTag: UniqueKey(),
                  backgroundColor: Colors.red,
                  onPressed: () async{

                     _animate2Pageforward();
//                     readSecureStorage();
                  },
                  tooltip: 'Next Day',
                  child: Icon(Icons.arrow_forward),
                  elevation: 0.0,
                ),
              ),
            ),
          ],
        ),

backgroundColor: Color(whitecolor),
        body: new Column(
//          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
//          Text("hello"),
            new Expanded(
              child: DaysPageView(
                physics: new NeverScrollableScrollPhysics(),
                  dayoverride: getDate(widget.date),
                  scrollDirection: _scrollDirection,
                  pageSnapping: _pageSnapping,
                  reverse: _reverse,
                  controller: _daysPageController,
                  pageBuilder: _daysPageBuilder,
                )
            ),
            SingleChildScrollView(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  Expanded(

                    child: Image.asset(
                        "assets/navigation.png", color: Colors.red,
                    fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
  }


  Widget _daysPageBuilder(BuildContext context, List<DateTime> days) {
    return new Page(
      days: days,
      initialdayoverride: widget.date,
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => false;
}
