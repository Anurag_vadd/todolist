import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todolist_unmodified_dup/all_database_files/DATA.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/lib/utils/date_time_to_string.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/widgets/inc/descriptionScreenFromGroups.dart';
import 'package:todolist_unmodified_dup/widgets/inc/group_details.dart';
import 'package:todolist_unmodified_dup/widgets/inc/profile_screen.dart';
import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';
import 'Calendar_screen.dart';
import 'Description_Screen.dart';
import 'Feedback_screen.dart';
import 'practice_screens/date_formatter.dart';
import 'input_screen.dart';
import 'modalsheet.dart';
import 'package:http/http.dart' as http;

bool loadMoreGroupScreenTasks = false;

int completedTasks;
bool loadMoreGroup = false;

bool showSnackBarCreateGroup = false;

class Group_Screen extends StatefulWidget {

//  const Group_Screen({Key key, this.posts}) : super(key: key);
  @override
  _Group_ScreenState createState() => _Group_ScreenState();
}

class _Group_ScreenState extends State<Group_Screen> with TickerProviderStateMixin{
  TabController controller;
  bool isSwitched = false;
  bool completed = false;
  AnimationController animationcontroller;
  Animation animation;
  bool iconOnClick = false;

ScrollController scrollController = new ScrollController();
  TextEditingController textcontroller = new TextEditingController();
  TextEditingController cardController = new TextEditingController();

//  List<dataArray> posts;
  var isLoading = false;
  Future _getTaskAsync;
/// =============================================================================================================================

  List<Post> items;
  bool _loadingMore;
  bool _hasMoreItems;
  int _maxItems = 30;
  int _numItemsPage = 10;
  Future _initialLoad;

  Future _loadMoreItems() async {
    final totalItems = items.length;
    await Future.delayed(Duration(seconds: 3), () {
      for (var i = 0; i < _numItemsPage; i++) {

        getTaskValue(limit, page).then((value) => {items.add(value)});
      }
    });

    _hasMoreItems = items.length < _maxItems;
  }




  ///-----------------------------------------------------------------------------------------------------------------------------------

  notification(){
    return
      isSwitched != isSwitched;
  }

int limit = 10;
  int page = 0;

  increaseOffset(){
  return page += 1;
  }
  Future _getTaskAsyncTasks;
  ///-----------------------------------------------------------------------------------------------------------------------------------
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getTaskAsync = getGroup();
    _getTaskAsyncTasks = getTaskInGroupScreen(limit, page);

    _initialLoad = Future.delayed(Duration(seconds: 0), () {
      items = List<Post>();
      for (var i = 0; i < _numItemsPage; i++) {
        getTaskValue(limit, page).then((value) => {items.add(value)});

      }
      _hasMoreItems = true;
    });
    controller = TabController(vsync: this, length: 2);

    animationcontroller = new AnimationController(
        duration: new Duration(milliseconds: 225),
        vsync: this
    );

    final CurvedAnimation curve = new CurvedAnimation(
        parent: animationcontroller,
        curve: Curves.easeOut
    );

    animation = new Tween(begin: 0.0, end: 1.0).animate(curve)
      ..addListener(() => setState(() {}));
    animationcontroller.forward(from: 0.0);
//    _readList();
  }

  ///-----------------------------------------------------------------------------------------------------------------------------------

  // ignore: non_constant_identifier_names

  bool marker = false;

  @override
  void dispose() {
    textcontroller.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();


  ///-----------------------------------------------------------------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    print("PRINTING BUILD grouop screen");

    final groupAppState = Provider.of<groupState>(context);

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    ///-----------------------------------------------------------------------------------------------------------------------------------
    _showSnackBar(String value) {
      print("Show Snackbar here !");
      final snackBar = new SnackBar(
        content: new Text(value),
        duration: new Duration(seconds: 3),
        backgroundColor: Colors.grey,
        action: new SnackBarAction(label: 'Ok', onPressed: (){
          print('press Ok on SnackBar');
        }),
      );
      //How to display Snackbar ?
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

void _showAddModalSheet(){
  showModalBottomSheet(context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      isScrollControlled: true,
//        useRootNavigator: true,
      builder: (builder) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
//        height: 500,
            height: MediaQuery.of(context).copyWith().size.height * 0.70,//        width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    TextField(
                      maxLength: 40,
                      decoration: InputDecoration(
                        labelText: "Group name"
                      ),
                      autofocus: true,
                      controller: cardController,
                      ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Transform.scale(
                          scale: 0.7,
                          child: FloatingActionButton(
                            elevation: 0,
                            heroTag: UniqueKey(),
                            child: Icon(Icons.add, color: Colors.white,),
                            backgroundColor: Colors.red,
                            onPressed: () async {
//setState(() {

                            var user_id = await secureStore.read(key: "userId");

  loadMoreGroup = true;

//});
                              if(cardController.text.isNotEmpty) {
//                                addItemCard(cardController.text);


                                dataArray groupPost = new dataArray(groupName: cardController.text, user_id: user_id);
                                dataArray p = await createPostGroup(baseURL + createGroupUrl, body: groupPost.toMapGroup());


                                Timer _timer;
                                _timer = new Timer(const Duration(seconds: 1), () {
                                  setState(() {
                                    loadMoreGroup = false;

                                  });
                                  });


                                Navigator.of(context).pop();
//                                dispose();
                              } else {
//                                _showSnackBar("Group name already exists");
                                Timer _timer;
                                _timer = new Timer(const Duration(seconds: 1), () {
                                  setState(() {
                                    loadMoreGroup = false;

                                  });
                                });


                                Navigator.of(context).pop();
                              }

//                              if(showSnackBarCreateGroup == true){
//                                print("PRINTING SHOWSNACKBARGROUPBOOL = $showSnackBarCreateGroup");
////_showSnackBar("Group name already exists");
//                                showDialog(
//                                    context: context,
//                                    builder: (BuildContext context) {
//                                      return AlertDialog(
//                                        content: Form(
//                                          key: _formKey,
//                                          child: Column(
//                                            mainAxisSize: MainAxisSize.min,
//                                            children: <Widget>[
//                                              Padding(
//                                                padding: EdgeInsets.all(8.0),
//                                                child: RedfontstyleBebas(text: "Thank you",size: 30,),
//                                              ),
//                                              SizedBox(height: 30,),
//                                              Padding(
//                                                padding: const EdgeInsets.all(8.0),
//                                                child: RaisedButton(
//                                                  child: Text("Ok"),
//                                                  onPressed: () {
//                                                    Navigator.pop(context);
//                                                  },
//                                                ),
//                                              )
//                                            ],
//                                          ),
//                                        ),
//                                      );
//                                    });
//                              }
                            },
                          ),
                        )
                      ],
                    )
                  ],
                ),
                )
              ],
            ),
          ),
        );
         }
      );
}


    Widget CardViewWidget(List<dataArray> data) {
//      print(data);
    Future.delayed(Duration(seconds: 3));
    Future<void> refresh() async {
      if (loadMoreGroup == true) {

        return getGroup().then((user) {
          setState(() {
            data.replaceRange(0, data.length, user);

          });
        });
      }
      else {
        return null;
      }
    }

    refresh();
      return Container(
        child: CustomScrollView(
            slivers: <Widget>[
              SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2
                  ),
                  delegate: new SliverChildBuilderDelegate((context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(4),
                      child: InkWell(
                        onLongPress: () {
                          showModalBottomSheet(context: context,
                              shape: RoundedRectangleBorder(

                                borderRadius: BorderRadius.circular(20.0),
                              ),

                              isScrollControlled: true,
//        useRootNavigator: true,
                              builder: (builder) {
                                return Container(
//        height: 500,
                                  height: 500,//        width: MediaQuery.of(context).size.width,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: SingleChildScrollView(
                                          child: Column(
                                            children: <Widget>[
                                              TextField(
                                                maxLength: 60,

                                                decoration: InputDecoration(
                                                    labelText: "Update Group name"
                                                ),
                                                autofocus: true,
                                                controller: cardController,
                                              ),

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: <Widget>[
                                                  Transform.scale(
                                                    scale: 0.7,
                                                    child: FloatingActionButton(
                                                      child: Icon(Icons.add, color: Colors.white,),
                                                      backgroundColor: Colors.red,
                                                      onPressed: () async {
                                                        print("data id = ${data[index].id}");
                                                        setState(() {
                                                          loadMoreGroup = true;

                                                        });

                                                        if(cardController.text == null){
                                                          Navigator.pop(context);

                                                          Timer _timer;
                                                          _timer = new Timer(const Duration(microseconds: 1), () {
                                                            setState(() {
                                                              loadMoreGroup = false;

                                                            });
                                                          });

                                                        }else if(cardController.text.isNotEmpty) {
                                                          dataArray groupPost = new dataArray(
                                                              groupName: cardController
                                                                  .text,
                                                              id: data[index].id);
                                                          dataArray p = await createPostGroup(baseURL + updateGroupsUrl, body: groupPost.toMapGroupUpdate());

                                                          Timer _timer;
                                                          _timer = new Timer(const Duration(microseconds: 1), () {
                                                            setState(() {

                                                              loadMoreGroup = false;
                                                              refresh();

                                                            });
                                                          });

                                                          Navigator.pop(context);
                                                        }
                                                      },
                                                    ),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              });
                          },
                        onDoubleTap: () async {
//
                          List<dynamic> result1 = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                groupDetails(
                                  data[index].groupName,
                                  data[index].id,

                                )
                              )
                          );
                        },
                        child: Container(
//                        padding: EdgeInsets.all(value),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: 0.3,

                                  color: Colors.black
                              )
                          ),
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 10,),
                              new ListTile(
                                onTap: (){
//                                String url = (baseURL + deleteGroup + data[index].id);
                                print(data[index].id);
                                  print("PRINTING URL = https://lafqwzv955.execute-api.ap-south-1.amazonaws.com/dev/v1.0/groups/delete_group/${data[index].id}");
                                },
                                title: RedfontstyleMont(
                                  text: data[index].groupName, size: 17,),



                                trailing: new Listener(
                                    key: new Key(UniqueKey().toString()),
                                    child: new Icon(Icons.close,
//                                        color: Color(#xCBCBCB),
                                    ),
                                    onPointerDown: (pointerEvent) async {
                                      var alert = new AlertDialog(
                                        title: new Text("Delete item"),
                                        content: new Text("Are you sure you want to delete this task?"),
                                        actions: <Widget>[
                                          new FlatButton(
                                              onPressed: () async {
                                                String id = await secureStore.read(key: "userId");

                                                commonDel("https://lafqwzv955.execute-api.ap-south-1.amazonaws.com/dev/v1.0/groups/delete_group/${data[index].id}/$id", data.asMap());

                                                setState(() {

                                                  data.removeAt(index);

                                                });

                                                Navigator.of(context, rootNavigator: true).pop('dialog');
                                              },
                                              child: new Text("Confirm")
                                          ),
                                          new FlatButton(
                                              onPressed: () {
                                                Navigator.of(context).pop(true);
                                              },
                                              child: new Text("Cancel"))
                                        ],
                                      );

                                      showDialog(context: context ,builder: (_) {
                                        return alert;
                                      });

                                      }
                                    ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                      childCount: data.length
                  )
              ),
            ]
        )
      );
    }

    bool returnBool(){
      return true;
    }


    void _showModalSheet() {
      showModalBottomSheet(
                shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
          context: context,
          builder: (BuildContext bc) {
            return bottom();// return a StatefulWidget widget
          });
    }
    ///-----------------------------------------------------------------------------------------------------------------------------------

    listViewWidget(List<Post> data) {



      bool _loadingMore = false;
      bool _hasMoreItems = true;
      int _maxItems = data.length;
      int _numItemsPage = 10;
      Future _initialLoad;

      Future _loadMoreItems() async {
        await Future.delayed(Duration(seconds: 3), () {
          for (var i = 0; i < 2; i++) {
            getTaskValue(limit, page + 1).then((value) => {data.add(value)});
          }
        });

        _hasMoreItems = data.length < _maxItems;
      }

//      numberOfTasksLeft = data.length;

      data.sort((a, b) => b.priority.compareTo(a.priority));/**/

      bool returnBool(){
        return true;
      }


      int returnInt(){
        return data.length;
      }

      Future<void> refresh() async {
        if (loadMoreGroupScreenTasks == true) {

          return getTaskInGroupScreen(10, 0).then((user) {
            setState(() {
              data.replaceRange(0, data.length, user);
//              todoappState.increment(numberOfTasksLeft);
            });
          });
        }
        else {
          return null;
        }
      }



      refresh();




      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
//          Row(
//            children: <Widget>[
//
//              SizedBox(width: 15,),
//              blackfontstyleBebasBold(text: "${returnInt()} Tasks more", size: 35,),
//            ],
//          ),
          SizedBox(height: 5,),
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListView.builder(
//                    hasMore: () => _hasMoreItems,
//                    loadMore: () async {
//                      // can shorten to "loadMore: _loadMoreItems" but this syntax is used to demonstrate that
//                      // functions with parameters can also be invoked if needed
//                      await _loadMoreItems();
//                    },
//                    onLoadMore: () async{
//                      setState(() {
//                        _loadingMore = true;
//                        increaseOffset();
//                      });
//
//
//                    },
//                    onLoadMoreFinished: () {
//                      setState(() {
//                        _loadingMore = false;
//                      });
//                    },
//                    loadMoreOffsetFromBottom: 2,
                    controller: scrollController,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: data.length,
                    itemBuilder: (BuildContext context, int index) {


//                  void _handleSubmittedUpdate(int index, Post post) {
//                    setState(() {
//                      data.removeWhere((element) {
//                        return element.taskName == post.taskName;
//                      });
////      db.close();
//                    });
//                  }
//                  completeTask(Post item, bool isDone, var index) async {
//                    Post newItemUpdated = Post.fromJson(
//                        {
//                          "name": item.taskName.toString(),
//                          // string result must be displayed here
//                          "date": DateTime.now().toString(),
//                          "_id": item.taskID.toString(),
//                          "group_id": item.groupID.toString(),
//                          "status": isDone.toString()
//                        });
//                    print(newItemUpdated);
//                    _handleSubmittedUpdate(index, newItemUpdated);
//                  }
//                  // ignore: non_constant_identifier_names
//

                      Widget returnFAB() {
                        if (data[index].priority == 1) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.green,
                                onPressed: () {
                                  setState(() {
                                    /// IMPLEMENT SET PRIORITY HERE
                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }

                        if (data[index].priority == 2) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.lightGreenAccent,
                                onPressed: () {
                                  setState(() {
                                    /// IMPLEMENT SET PRIORITY HERE
                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                        if (data[index].priority == 3) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.yellow,
                                onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                        if (data[index].priority == 4) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.orange,
                                onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                        if (data[index].priority == 5) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.red,
                                onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                      }

                      Widget returnText() {

                        return blackfontstyleMont(
                            text: data[index].taskName.toString(), size: 20);


                      }

//bool confirmDismiss = false;
                      return Container(
                        child: InkWell(

                          onTap: () async {
loadMoreGroupScreenTasks = true;


                            List<dynamic> result1 = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Description_Screen(

                                          data[index].taskName,
//                                      data[index].markComplete,

//                                          data[index].date,
                                          data[index].taskID,
                                          data[index].priority,

                                          data[index].groupID,
//                                          data[index].description,
//                                           data[index].subtasks,
                                          dateToStringtime(setter),
                                        data[index].description
                                      ),
                                )
                            );
                            print("11111111111111111111111");
//                            print(data[index].markComplete.runtimeType);
returnDate(){
  if(result1[5] == null){
    return DateTime.utc(2001,1,1,1);
  }else{
    return result1[5];
  }
}

                            returnGroupId(){
                              if(result1[3] == null){
                                return data[index].groupID;
                              }else{
                                return result1[3];
                              }
                            }

                            Post taskPost = new Post(
//                                  markComplete: data[index].markComplete,
                                taskName: result1[0],
                                groupID: returnGroupId().toString(),
                                description: result1[1],
                                taskID: result1[4],
                                date: returnDate().toString(),
                                priority: result1[2].toString());

                            if(result1[1] != null && result1[3] != null) {


                              print("PRINTING TAKPOST FROM GROUP SCREEN WITH BOTH = ${taskPost.toMapUpdateCreate()}");


                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateCreate());
//                              setState(() {
//                                data.removeAt(index);
//                              });
refresh();
                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMoreGroupScreenTasks = false;
                              });
                            }else if(result1[1] == null && result1[3] != null){
//                              Post taskPost = new Post(
////                                  markComplete: data[index].markComplete,
//                                  taskName: true.toString(),
//                                  groupID: result1[3],
////                                  description: result1[1],
//                                  taskID: result1[4],
//                                  date: DateTime.now().toString(),
//                                  priority: result1[2].toString());

//                              print("PRINTING TAKPOST = ${taskPost.toMapUpdate()}");

                              print("PRINTING TAKPOST FROM GROUP SCREEN WITHOUT DESCRIPTION= ${taskPost.toMapUpdateCreate()}");

                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateWithoutDescription());
//                              setState(() {
//                                data.removeAt(index);
//                              });
                              refresh();

                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMoreGroupScreenTasks = false;
                              });
                            }else if(result1[1] != null && result1[3] == null){
//                              Post taskPost = new Post(
////                                  markComplete: data[index].markComplete,
//                                  taskName: true.toString(),
////                                  groupID: result1[3],
//                                  description: result1[1],
//                                  taskID: result1[4],
//                                  date: DateTime.now().toString(),
//                                  priority: result1[2].toString());

//                              print("PRINTING TAKPOST = ${taskPost.toMapUpdate()}");

                              print("PRINTING TAKPOST FROM GROUP SCREEN WITHOUT GROUP= ${taskPost.toMapUpdateCreate()}");

                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateWithoutGroup());
//                              setState(() {
//                                data.removeAt(index);
//                              });
                              refresh();

                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMoreGroupScreenTasks = false;
                              });
                            }else if(result1[1] == null && result1[4] == null){

//                              Post taskPost = new Post(
////                                  markComplete: data[index].markComplete,
//                                  taskName: true.toString(),
////                                  groupID: result1[3],
////                                  description: result1[1],
//                                  taskID: result1[4],
//                                  date: DateTime.now().toString(),
//                                  priority: result1[2].toString());

                              print("PRINTING TAKPOST FROM GROUP SCREEN WITHOUT BOTH = ${taskPost.toMapUpdateCreate()}");

                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateWithoutGroupAndDescription());
//                              setState(() {
//                                data.removeAt(index);
//                              });
                              refresh();

                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMoreGroupScreenTasks = false;
                              });
                            }



                          },

                          child: Dismissible(


                            key: UniqueKey(),
                            onDismissed: (direction) async {

                              if (direction.index == 2) {

                                deletePost(
                                  baseURL + deleteTodoUrl, data[index].taskID.toString(),);


                                setState(() {
//                                  todoappState.decrement(numberOfTasksLeft);

                                  data.removeAt(index);
refresh();
                                });


                              } else {
//String markasComplete = "true";
                                deletePost(
                                  baseURL + deleteTodoUrl, data[index].taskID.toString(),);


                                setState(() {
//                                  todoappState.decrement(numberOfTasksLeft);

                                  data.removeAt(index);
                                  refresh();
                                });

                              }
//                  setState(() {
//                    marker =! marker;
//
//                    if(marker == false){
//                      uncompleteTask(data[index], false, index);
//                    }else if(marker == true){
//                      completeTask(data[index], true, index);
//                    }
//                  });

                            },
                            secondaryBackground: Container(
                              transform: new Matrix4.identity()
                                ..scale(animation.value, 1.0),
                              child: Icon(Icons.delete, color: Colors.black,),
                              color: Colors.transparent,
                              alignment: Alignment.centerLeft,
                            ),
                            background: Container(
                              child: Icon(Icons.done),
                              color: Colors.transparent,
                              alignment: Alignment.centerLeft,
                            ),
                            child: Container(
                              width: double.infinity,
                              child: ListTile(
                                leading: returnFAB(),
                                title: returnText(),

                              ),
                            ),
                          ),
                        ),
                      );
                    }

                ),
              ],
            ),
          ),

        ],
      );
    }



    return Material(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color(whitecolor),
        resizeToAvoidBottomPadding: false,
        floatingActionButton: Stack(
          children: <Widget>[
            Positioned(
              bottom: -14,
              child: InkWell(
                onTap: ()async{
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyHomePage(),
                      ));
                },
                child: Container(
                  color: Colors.red,
                  width: MediaQuery.of(context).size.width *0.4,
                  height: 50,
//           child:,
                ),
              ),
            ),

//
            Positioned(
//              right: 0,
              left: 50,
              bottom: 100,
              top: 701,
//              top: 20,
              child: IconButton(
                icon: Icon(Icons.calendar_today, color: Colors.white,size: 30,),
                onPressed: () {
                  Navigator.push( // string which stores the user entered value
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MyHomePage();
                        }, //screen which has TextField
                      ));
                },
              ),

            ),
            Positioned(
              bottom: -14,
              right: -8,
              child: InkWell(
                onTap: (){
                  _showModalSheet();
                },
                child: Container(
                  color: Colors.red,
                  width: MediaQuery.of(context).size.width *0.35,
                  height: 50,

                ),
              ),
            ),
            Positioned(
              right: 135.5,
              top: 675,
              child: FloatingActionButton(
                heroTag: UniqueKey(),

                backgroundColor: Colors.red,
                onPressed: () async{

                if(controller.index ==0) {

                  setState(() {
                    _showAddModalSheet();

                  });
                }else if(controller.index == 1){
                    List<dynamic> result1 = await Navigator.push( // string which stores the user entered value
                        context,
                        MaterialPageRoute(

//                        maintainState: true,
                          builder: (context) {

                            return InputScreen(setter); // this is the third screen
                          }, //screen which has TextField
                        ));


                    returnDate(){
                      if(result1[4] == null){
                        return setter;
                      }else{
                        return result1[4];
                      }
                    }

                    Post taskPost = new Post(
//                      markComplete: false.toString(),
                        taskName: result1[0],
                        groupID: result1[3],
                        description: result1[1],
                        date: returnDate().toString(),
                        priority: result1[2].toString()
                    );

//                  print("PRINTING TASKPOST DPVE = ${taskPost.toMapUpdateWithoutDescription()}");


                    if(result1[1] == null && result1[3] == null){
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciptionAndGroup());
                      print("Printing P = $p");
                      loadMoreGroupScreenTasks = true;

//                      numberOfTasksLeft = numberOfTasksLeft - 1;


                    }else if(result1[1] != null && result1[3] == null){
                      loadMoreGroupScreenTasks = true;

//                      numberOfTasksLeft = numberOfTasksLeft - 1;
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutGroup());
                      print("Printing P = ${p.taskName}");

                    }else if(result1[1]== null && result1[3] != null){
                      loadMoreGroupScreenTasks = true;
//                      numberOfTasksLeft = numberOfTasksLeft - 1;
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciption());


                    }else if(result1[1] != null && result1[3] != null){
                      loadMoreGroupScreenTasks = true;

//                      numberOfTasksLeft = numberOfTasksLeft - 1;
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreate());


                    }




                    Timer _timer;
                    _timer = new Timer(const Duration(microseconds: 1), () {
                      loadMoreGroupScreenTasks = false;
                    });

                 }
//                controller.dispose();
//                dispose();
                },
                tooltip: 'Add a task',
                child: Icon(Icons.add),
                elevation: 0.0,
              ),
            ),
            Positioned(
//              right: 0,
//              left: 50,
              right: 30,
//              top: 0,
              bottom: -9,
//              top: 700,
//              top: 20,
              child: IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.white,size: 30,
                ),
                onPressed: () {
                  _showModalSheet();
//                  fetchData();
                },
              ),
            ),
          ],
        ),
        body: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[

                            RedfontstyleBebas(text: "ALL", size: 40,),
//                            SizedBox(width: 175,),
                            Transform.scale(
                              scale: 0.8,
                              child: FloatingActionButton(
                                elevation: 0.0,
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                                backgroundColor: Colors.red,
                                foregroundColor: Colors.white,
                                child: Icon(
                                  Icons.arrow_back,
                                  color: Colors.white,
                                  size: 30,
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
SizedBox(height: 20,),
                      TabBar(
indicatorSize: TabBarIndicatorSize.label,

                        labelColor: Colors.red,
                        indicatorColor: Colors.red,
                        controller: controller,
                        tabs: <Widget>[
                          RedfontstyleMont(text: "GROUPS", size: 25,),
                          RedfontstyleMont(text: "LIST",size: 25,)
                        ],

                      ),
                      SizedBox(height: 10,),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            maxHeight: 500,
                            minHeight: 100
                        ),
                        child: TabBarView(

//              physics: NeverScrollableScrollPhysics(),
                          controller: controller,
                          children: <Widget>[
                            FutureBuilder(
                              future: _getTaskAsync,
                              builder: (context, snapshot){
                                if(snapshot.hasData){
                                  return CardViewWidget(snapshot.data);

                                }else{
                                  return  Center(child: CircularProgressIndicator());
                                }

                              },

                            ),

/// -----------------------------------------------------------------------------------------------------------------------------------

                            SingleChildScrollView(
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 500,
                                child: ConstrainedBox(
                                  constraints: new BoxConstraints(
                                   maxHeight: 500,
                                   minHeight: 100
                                   ),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: <Widget>[

                                        FutureBuilder(
                                          future: _getTaskAsyncTasks,
                                          builder: (context, snapshot){
                                            if(snapshot.hasData){
                                              return listViewWidget(snapshot.data);

                                            }else{
                                                  return Text("");

                                            }

                                          },

                                        ),

//                                      FutureBuilder(
//                                        future: _initialLoad,
//                                        builder: (context, snapshot) {
//                                          switch (snapshot.connectionState) {
//                                            case ConnectionState.waiting:
//                                              return Center(child: CircularProgressIndicator());
//                                            case ConnectionState.done:
//                                              return SingleChildScrollView(
//
//                                                child: Container(
//                                                  width: MediaQuery.of(context).size.width,
//                                                  height: 300,
//                                                  child: IncrementallyLoadingListView(
//                                                    hasMore: () => _hasMoreItems,
//                                                    itemCount: () => items.length,
//                                                    loadMore: () async {
//                                                      // can shorten to "loadMore: _loadMoreItems" but this syntax is used to demonstrate that
//                                                      // functions with parameters can also be invoked if needed
//                                                      await _loadMoreItems();
//                                                    },
//                                                    onLoadMore: () {
//                                                      setState(() {
//                                                        _loadingMore = true;
//                                                      });
//                                                    },
//                                                    onLoadMoreFinished: () {
//                                                      setState(() {
//                                                        _loadingMore = false;
//                                                      });
//                                                    },
//                                                    loadMoreOffsetFromBottom: 2,
//                                                    itemBuilder: (context, index) {
//                                                      final item = items[index];
//                                                      if ((_loadingMore ?? false) && index == items.length - 1) {
//                                                        return Column(
//                                                          children: <Widget>[
//                                                            blackfontstyleMont(text: item.taskName,),
//                                                           ],
//                                                        );
//                                                      }
//                                                      return blackfontstyleMont(text: item.taskName,);
//                                                    },
//                                                  ),
//                                                ),
//                                              );
//                                            default:
//                                              return Text('Something went wrong');
//                                          }
//                                        },
//                                      ),
//                                      FutureBuilder(
//                                        future: _getTaskAsyncDone,
//                                        builder: (context, snapshot){
//                                          if(snapshot.hasData){
//
//                                            return ListViewWidgetDone(snapshot.data);
//
//                                          }else{
//                                            return Text("");
//                                          }
//                                        },
//                                      ),
//                                      ListTile(
//                                        trailing: Icon(Icons.keyboard_arrow_down),
//                                        title: blackfontstyleBebas(text: "Completed tasks ($completedTasks)", size: 20,),
//                                      )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
//                SizedBox(height: 10,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        child: Image.asset("assets/navigation.png",
                          fit: BoxFit.fill,
                          color: Colors.red,
                        )
                    )

                  ],
                ),
              ],
            )
      )
    );
//          },
//        )
//      ),
//    );
  }
}


//
//ListView.builder(
//controller: scrollController,
//scrollDirection: Axis.vertical,
//shrinkWrap: true,
//itemCount: Itemlist.length,
//itemBuilder: (BuildContext context, int index){
//
//return Container(
//child: GestureDetector(
//
//onTap: ()async{
////                                                    await Navigator.push(
////                                                        context,
////                                                        MaterialPageRoute(
////                                                          builder: (context) => Description_Screen(Itemlist[index].itemName, Itemlist[index].taskComplete, Itemlist.length, Itemlist.indexOf(Itemlist[index])),
////                                                        )
////                                                    );
//},
//
//child: Dismissible(
//key: Key(UniqueKey().toString()),
//onDismissed: (direction) async{
//if(direction.index ==2){
//setState(() {
//deleteItem(Itemlist[index].id, index);
//});
//}else{
//setState(() {
//print(Itemlist.indexOf(Itemlist[index]));
//marker =! marker;
//if(marker == false){
//uncompleteTask(Itemlist[index], false, index);
//}else if(marker == true){
//completeTask(Itemlist[index], true, index);
//}
//});
//}
//},
//secondaryBackground: Container(
//transform: new Matrix4.identity()..scale(animation.value, 1.0),
//child: Icon(Icons.delete),
//alignment: Alignment.centerLeft,
//),
//background: Container(
//child: Icon(Icons.done),
//alignment: Alignment.centerLeft,
//),
//child: Container(
//width: double.infinity,
//child: Hero(
//tag: UniqueKey(),
//child: GestureDetector(
//onTap: ()async{
////                                                              await Navigator.push(
////                                                                  context,
////                                                                  MaterialPageRoute(
////                                                                    builder: (context) => Description_Screen(Itemlist[index].itemName, Itemlist[index].taskComplete, Itemlist.length, Itemlist[index].id),
////                                                                  )
////                                                              );
//},
//child: ListTile(
//title: Itemlist[index],
////                                                              subtitle: Padding(
////                                                                    padding: const EdgeInsets.all(8.0),
////                                                                    child: blackfontstyleMont(text: DateTime.now().toString(), size: 10,),
////                                                              ),
//)
//),
//),
//),
//),
//),
//);
//}
//),