import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/lib/days_page_view/day_page_view_example.dart';
import 'package:todolist_unmodified_dup/lib/utils/all.dart';
import 'package:todolist_unmodified_dup/widgets/inc/About.dart';
import 'package:todolist_unmodified_dup/widgets/inc/group_screen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/loginscreen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/profile_screen.dart';

import 'Feedback_screen.dart';
import '../../all_database_files/provider/all_classes.dart';
import 'practice_screens/graph_screen_2.dart';




class bottom extends StatefulWidget {
  @override
  _bottomState createState() => _bottomState();
}

class _bottomState extends State<bottom> {
  bool isSwitched = false;

  void _onSwitchChanged(bool value) {
//    setState(() {
    isSwitched = value;
//    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
          color: Color(redcolor),
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20.0),
              topRight: const Radius.circular(20.0)
          )
      ),

//    color: Color(redcolor),
      height: MediaQuery.of(context).copyWith().size.height * 0.40,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(child: Image.asset('assets/Capture1.PNG',),),
          ListTile(
            onTap: ()async{
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return profile_Screen();
                    }, //screen which has TextField
                  ));
            },
            leading: Padding(
              padding: const EdgeInsets.only(left: 13),
              child: Icon(
                Icons.person ,
                color: Color(whitecolor),
              ),
            ),
            title: whitefontstyleBebas(text: "Profile", size: 20,),
          ),

//          ListTile(
//            onTap: (){
//              Navigator.push( // string which stores the user entered value
//                  context,
//                  MaterialPageRoute(
//                    builder: (context) {
//                      return Graph_screen_2();
//                    }, //screen which has TextField
//                  ));
//            },
//            leading: Padding(
//              padding: const EdgeInsets.only(left: 13),
//              child: Icon(
//                Icons.insert_chart,
//                color: Color(whitecolor),
//              ),
//            ),
//            title: whitefontstyleBebas(text: "Productivity", size: 20,),
//          ),

//          SwitchListTile(
//            selected: true,
//            activeColor: Colors.black,
//            secondary: Padding(
//              padding: const EdgeInsets.only(left: 14),
//              child: Icon(Icons.notifications, color: Colors.white,),
//            ),
//            title: whitefontstyleBebas(text: "Notification", size: 20,), // just a custom font, otherwise a regular Text widget
//            value: isSwitched,
//            onChanged: (bool value){
//              setState(() {
//                _onSwitchChanged(value);
//                firebaseMessaging.requestNotificationPermissions();
//              });
//            },
//          ),

          ListTile(
            onTap: (){
              Navigator.push( // string which stores the user entered value
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return feedback_Screen();
                    }, //screen which has TextField
                  ));
            },
            leading: Padding(
              padding: const EdgeInsets.only(left: 13),
              child: Icon(Icons.feedback
                , color: Color(whitecolor),
              ),
            ),
            title: whitefontstyleBebas(text: "Feedback", size: 20,),
          ),
          ListTile(
            onTap: (){
              Navigator.push( // string which stores the user entered value
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return About_screen();
                    }, //screen which has TextField
                  ));
            },
            leading: Padding(
              padding: const EdgeInsets.only(left: 13),
              child: Icon(Icons.info
                , color: Color(whitecolor),
              ),
            ),
            title: whitefontstyleBebas(text: "About", size: 20,),
          ),
          ListTile(
            onTap: (){
              signOutGoogle();
              secureStore.deleteAll().then((value) => {
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                    LoginPage()), (Route<dynamic> route) => false),
              });
            },
            leading: Padding(
              padding: const EdgeInsets.only(left: 13),
              child: Icon(Icons.subdirectory_arrow_left
                , color: Color(whitecolor),
              ),
            ),
            title: whitefontstyleBebas(text: "Logout", size: 20,),
          ),
        ],
      ),
    );
  }
}

///=============================================================================================================================


class repeatSheet extends StatefulWidget {
  dynamic date;

  repeatSheet(this.date);
  @override
  _repeatSheetState createState() => _repeatSheetState();
}

class _repeatSheetState extends State<repeatSheet> {
TextEditingController numberontroller = new TextEditingController();
String dropdownValue; // for day $i
String dropdownRepeat; // for repeat first,second,etc.
String dropdownDays; // for 2nd dropdownitme

int dateValue;
bool isSelected = false;
String monthDay;


List dates = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];



Widget showSizedBox(){
  if(dropdownValue == "Week"){
    return SizedBox(height: 60,);
  }else{
    return SizedBox(height: 10,);
  }
}

//String dayvalue;
//returnList(){
//  for(int i = 1; i <=31; i++){
//    dayvalue = "Day $i";
//    return dayvalue;
//  }
//}

  List<DropdownMenuItem<String>> dayValue;

Widget buildDropdownButtonDay() {
  return new  DropdownButton<String>(
      hint: Text('Choose'),
      onChanged: (String changedValue) {
        monthDay=changedValue;
        setState(() {
          monthDay = changedValue;
//                  print(newValue);
        });
      },
      value: monthDay,
      items: dayValue
  );

}

Widget positionedFab(){
  if(dropdownValue == "Week"){
    return Visibility(
      maintainSize: false,
      visible: true,
      child: Stack(
//          alignment: AlignmentGeometry,
        children: <Widget>[
          Positioned(
            right: 270,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[0]) + 1),
                child: Text(dates[0], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 1;
                  });
                },
              ),
            ),
          ),

          Positioned(
            right: 225,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[1]) + 1),
                child: Text(dates[1], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 2;
                  });
                },
              ),
            ),
          ),

          Positioned(
            right: 180,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[2]) + 1),
                child: Text(dates[2], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 3;
                  });
                },
              ),
            ),
          ),

          Positioned(
            right: 135,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[3]) + 1),
                child: Text(dates[3], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 4;
                  });
                },
              ),
            ),
          ),

          Positioned(
            right: 90,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[4]) + 1),
                child: Text(dates[4], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 5;
                  });
                },
              ),
            ),
          ),

          Positioned(
            right: 45,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[5]) + 1),
                child: Text(dates[5], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 6;
                  });
                },
              ),
            ),
          ),

          Positioned(
            right: 0,
            top: 80,
            child: Transform.scale(
              scale: 0.80,
              child: FloatingActionButton(
                elevation: 0,
                backgroundColor: returnColor(dates.indexOf(dates[6]) + 1),
                child: Text(dates[6], style: TextStyle(color: Colors.black)),
                onPressed: () {
                  setState(() {
                    dateValue = 7;
                  });
                },
              ),
            ),
          ),


        ],
      ),
    );
  }else {
    return Container(child: Text(""),);
  }
}

Widget returnMonth(){
  if(dropdownValue == "Month"){
    return Column(
      children: <Widget>[
        ListTile(
          leading: Transform.scale(
          scale: 0.6,
            child: InkWell(
            onTap: () {
              setState(() {
                isSelected = !isSelected;
              });
             },
              child: Container(
              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
                 child: Padding(
                     padding: const EdgeInsets.all(10.0),
                     child: isSelected
                    ? Icon(
                  Icons.check,
                  size: 30.0,
                  color: Colors.white,
                )
                    : Icon(
                  Icons.check_box_outline_blank,
                  size: 30.0,
                  color: Colors.blue,
                ),
              ),
            ),
          ),
        ),
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                decoration: new BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: new BorderRadius.circular(8.0),
                  border: new Border.all(
                    width: 5.0,
                    color: Colors.grey.shade200,
                  ),
                ),
                child: buildDropdownButtonDay()),
          ),
        ),

        ListTile(
          leading: Transform.scale(
            scale: 0.6,
            child: InkWell(
              onTap: () {
                setState(() {
                  isSelected = !isSelected;
                });
              },
              child: Container(
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: !isSelected
                      ? Icon(
                    Icons.check,
                    size: 30.0,
                    color: Colors.white,
                  )
                      : Icon(
                    Icons.check_box_outline_blank,
                    size: 30.0,
                    color: Colors.blue,
                  ),
                ),
              ),
            ),
          ),
          title: Container(
            decoration: new BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: new BorderRadius.circular(8.0),
              border: new Border.all(
                width: 5.0,
                color: Colors.grey.shade200,
              ),
            ),
            child: DropdownButton<String>(
                hint: Text('Choose'),
                onChanged: (String changedValue) {
                  dropdownRepeat=changedValue;
                  setState(() {
                    dropdownRepeat;
//                  print(newValue);
                  });
                },
                value: dropdownRepeat,
                items: <String>["First", "Second", "Third", "Fourth", "Last"].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList()),
          ),

          trailing: Container(
            decoration: new BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: new BorderRadius.circular(8.0),
              border: new Border.all(
                width: 5.0,
                color: Colors.grey.shade200,
              ),
            ),
            child: DropdownButton<String>(
                hint: Text('Choose'),
                onChanged: (String changedValue) {
                  dropdownDays=changedValue;
                  setState(() {
                    dropdownDays;
//                  print(newValue);
                  });
                },
                value: dropdownDays,
                items: <String>["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList()),
          ),
        )
      ],
    );
  }else{
    return Container(child: Text(""),);
  }
}

Color returnColor(int value){
  for(int i = 1; i<dates.length; i++){
    if(dateValue == value){
      return Colors.red;
    }else{
      return Colors.white;
    }
  }
}
//
//Widget monthDay(){
//  if(dropdownValue == "Month"){
//    return
//  }
//}
  
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime;


  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2018),
        lastDate: DateTime(2040));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    TimeOfDay _selectedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    setState(() {
      selectedTime = _selectedTime;
    });
  }


Widget buildDropdownButton() {
  return new  DropdownButton<String>(
      hint: Text('Choose'),
      onChanged: (String changedValue) {
        dropdownValue=changedValue;
        setState(() {
          dropdownValue;
//                  print(newValue);
        });
      },
      value: dropdownValue,
      items: <String>['Day', 'Week', 'Month', 'Year'].map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(value),
        );
      }).toList());
}

@override
  void initState() {
    numberontroller.text = 1.toString();
  dayValue = List<DropdownMenuItem<String>>.generate(
      31,
          (int index) => DropdownMenuItem(
        value: "Day ${index + 1}", //added
        child: Text("Day ${index + 1}"),
      )).toList();
    super.initState();

    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    firebaseMessaging.getToken().then((token){
      print(token);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Container(
//      padding: EdgeInsets.all(8),
      decoration: new BoxDecoration(
          color: Color(whitecolor),
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20.0),
              topRight: const Radius.circular(20.0)
          )
      ),

//    color: Color(redcolor),
      height: MediaQuery.of(context).copyWith().size.height * 0.50,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        floatingActionButton: positionedFab(),

        body: Column(

          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//          Center(child: Image.asset('lib/assets/Capture1.PNG',),),
          SizedBox(height: 20,),

            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 25,),
               Text("Every", style: TextStyle(color: Colors.black, fontSize: 20),),

                SizedBox(width: 30,),

                Container(
                  decoration: new BoxDecoration(
                    color: Colors.grey.shade200,
                    borderRadius: new BorderRadius.circular(8.0),
                    border: new Border.all(
                      width: 5.0,
                      color: Colors.grey.shade200,
                    ),
                  ),
//                  color: Colors.grey.shade200,
                  height: 35,
                  width: 35,
                  child: TextField(
decoration: InputDecoration(fillColor: Colors.grey.shade200),
                    autofocus: false,
//                        focusNode: FocusNode(canRequestFocus: false),
                    controller: numberontroller,

                    inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                    keyboardType: TextInputType.number,
                  ),
                ),

                SizedBox(width: 30,),


                Container(
                    decoration: new BoxDecoration(
                      color: Colors.grey.shade200,
                      borderRadius: new BorderRadius.circular(8.0),
                      border: new Border.all(
                        width: 5.0,
                        color: Colors.grey.shade200,
                      ),
                    ),
                  height: 35,
                  width: 100,
//                    color: Colors.grey.shade200,
                  child: buildDropdownButton()
                )
              ],
            ),
                showSizedBox(),

                returnMonth(),

                Row(
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: 25,),

                    Text("Start", style: TextStyle( color: Colors.black, fontSize: 17),),

                    SizedBox(width: 20,),

                    Container(
                      decoration: new BoxDecoration(
                        color: Colors.grey.shade200,
                        borderRadius: new BorderRadius.circular(8.0),
                        border: new Border.all(
                          width: 5.0,
                          color: Colors.grey.shade200,
                        ),
                      ),
                      child: InkWell(
                          onTap: ()=> _selectDate(context),
                          child: Text(dateToStringtime(selectedDate) ?? widget.date, style: TextStyle( color: Colors.grey.shade600, fontSize: 17),)),
                    )
                  ],
                ),

            SizedBox(height: 20,),

            Padding(
              padding: const EdgeInsets.only(left: 22),
              child: Text("First Occurence will be ${dateToStringtime(selectedDate)}", style: TextStyle( color: Colors.black, fontSize: 17),),
            ),

            SizedBox(height: 20,),

            Row(
              children: <Widget>[
                SizedBox(width: 22,),
                Container(
                  decoration: new BoxDecoration(
                    color: Colors.grey.shade200,
                    borderRadius: new BorderRadius.circular(8.0),
                    border: new Border.all(
                      width: 5.0,
                      color: Colors.grey.shade200,
                    ),
                  ),
                  child: InkWell(
                    onTap: () => _selectTime(context),
                    child: Text("Set Time", style: TextStyle( color: Colors.grey.shade600, fontSize: 18),),
                  ),
                ),
              ],
            )

          ],
        ),
      ),
    );
  }
}
