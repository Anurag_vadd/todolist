
import 'package:rxdart/rxdart.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';

class ABloc {
  /// Input from the user
  var _inputSubject = PublishSubject<DateTime>();
  Sink<DateTime> get inputSink => _inputSubject.sink;

  /// Output for the user
  var _outputSubject = PublishSubject<DateTime>();
  Stream<DateTime> get outputStream => _outputSubject.stream;

  ABloc() {
    /// Get the input stream, add 1 and return to the view
    _inputSubject
        .stream
        .listen((value) => _outputSubject.add(setter.add(Duration(days: 1))));
  }
}