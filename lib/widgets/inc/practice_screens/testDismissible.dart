//import '//dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/widgets/inc/practice_screens/practice_lazyLoading.dart';
import 'package:http/http.dart' as http;


class BackendServicePractice {
  static Future<List<PostModel>> getPosts(offset, limit) async {
    final responseBody = (await http.get(
        'http://jsonplaceholder.typicode.com/posts?_start=$offset&_limit=$limit')).body;

    // The response body is an array of items
    return PostModel.fromJsonList(json.decode(responseBody));
  }

}

class testDismissible extends StatefulWidget {
  @override
  _testDismissibleState createState() => _testDismissibleState();
}

class _testDismissibleState extends State<testDismissible> {
  List data;

  Future<String> getData() async {
    var response = await http.get(
        Uri.encodeFull("https://jsonplaceholder.typicode.com/posts"),
        headers: {
          "Accept": "application/json"
        }
    );

    this.setState(() {
      data = json.decode(response.body);
    });

    print(data[1]["title"]);

    return "Success!";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
this.getData();
  }

  @override
  Widget build(BuildContext context) {
    final todoappState = Provider.of<todoState>(context);

    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, int index){

          return Container(
            child: Dismissible(
              key: UniqueKey(),
                onDismissed: (direction)async{
//                  Timer(Duration(seconds: 0, minutes: 0, microseconds: 0, milliseconds: 0), () {
                    setState(() {
//                              numberOfTasksLeft  = numberOfTasksLeft -  1;
                      todoappState.decrement(index);

                      data.removeAt(index);
                    });

//                  });
                },
                child: Text(data[index]["title"].toString())),
          );
        },
      ),
      )
      );
  }
}
