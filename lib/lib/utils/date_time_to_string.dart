
import 'package:intl/intl.dart';

String dateToString(DateTime date) {
  return
    "${date.year.toString().padLeft(4, "0")}."
      "${date.month.toString().padLeft(2, "0")}."
      "${date.day.toString().padLeft(2, "0")}";
}

String dateToStringtime(DateTime date) {
  return new DateFormat.MMMEd().format(date);
}

