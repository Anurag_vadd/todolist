import 'dart:convert';
//import 'dart:js';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/lib/days_page_view/day_page_view_example.dart';
import 'package:todolist_unmodified_dup/widgets/inc/loginscreen.dart';

import '../../main.dart';



class Login{
  final String mail;
//  final String phone;
//  final String userName;
  final String password;

  Login(
      this.mail,
//      this.phone,
//      this.userName,
      this.password,
      );

  Login.fromJson(Map jsonMap)
      : mail = jsonMap['mail'],
//        phone = jsonMap['phone'],
//        userName = jsonMap['username'],
        password = jsonMap['password'];


  Map toMap(){
    var mapGroup = new Map<String, dynamic>();
    mapGroup["mail"] = mail;
//    mapGroup['phone'] = phone;
//    mapGroup['username'] = userName;
    mapGroup['password'] = password;

    return mapGroup;

  }

}

final secureStore = new FlutterSecureStorage();

Future<Login> createLogin(String url, {Map body}) async {
//  print(body);

  return http.post(url, body: body).then((http.Response response) async {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code = $statusCode");
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data = $statusCode");
    }

//{
// "error":"0",
// "message":"User Exists",
// "data":{
//      "userdata":{
//                  "status":true,
//                  "_id":"5e3bb6b24a4ec50008f5c783",
//                  "username":"justforstuff",
//                  "mail":"accjustforstuff@gmail.com",
//                  "phone":"123456789"
//                  },
// "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJRCI6IjVlM2JiNmIyNGE0ZWM1MDAwOGY1Yzc4MyIsIm1haWwiOiJhY2NqdXN0Zm9yc3R1ZmZAZ21haWwuY29tIn0sImlhdCI6MTU4MDk3Mzk0MSwiZXhwIjoxNjEyNTA5OTQxfQ.t6Jw5NoyJWMoA29Xr20UuTjRfbkEUgUIWHiwgil0dVM"
//  }
// }


    var data = json.decode(response.body);


    if(statusCode == 401){
      validatorPassword = false;
    }

    if(statusCode == 200){
      String token = data["data"]["token"];
      print("PRINTING Token = $token");


      String id = data["data"]["userdata"]["_id"];
      print("PRINTING ID = $id");

      String username = data["data"]["userdata"]["username"];
      print("PRINTING ID = $username");

      String mail = data["data"]["userdata"]["mail"];
      print("PRINTING ID = $mail");

      String phone = data["data"]["userdata"]["phone"];
      print("PRINTING ID = $phone");


      navigateTo = true;
      await secureStore.write(key: "userToken", value: token);
      await secureStore.write(key: "userId", value: id);
      await secureStore.write(key: "userUsername", value: username);
      await secureStore.write(key: "userMail", value: mail);
      await secureStore.write(key: "userPhone", value: phone);
      Map list = await secureStore.readAll();

      print("PRINTING IF SECURE STORE WORK = $list");
    }

print("PRINTING LOGIN RESPONSE = ${response.body}");
    return Login.fromJson(json.decode(response.body));
  });
}

class UserDetails {
//  Verification verification;
//  bool status;
  String id;
//  String password;
  String username;
  String mail;
  String phone;
//  DateTime signupDate;
//  DateTime updatedDate;
//  int v;

  UserDetails({
//    this.verification,
//    this.status,
    this.id,
//    this.password,
    this.username,
    this.mail,
    this.phone,
//    this.signupDate,
//    this.updatedDate,
//    this.v,
  });

  factory UserDetails.fromRawJson(String str) => UserDetails.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
//    verification: Verification.fromJson(json["verification"]),
//    status: json["status"],
    id: json["_id"],
//    password: json["password"],
    username: json["username"],
    mail: json["mail"],
    phone: json["phone"],
//    signupDate: DateTime.parse(json["signupDate"]),
//    updatedDate: DateTime.parse(json["updatedDate"]),
//    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
//    "verification": verification.toJson(),
//    "status": status,
    "_id": id,
//    "password": password,
    "username": username,
    "mail": mail,
    "phone": phone,
//    "signupDate": signupDate.toIso8601String(),
//    "updatedDate": updatedDate.toIso8601String(),
//    "__v": v,
  };
}


Future<List<UserDetails>> getUser({Map body}) async {
//  print(body);
List<UserDetails> list;
String userId = await secureStore.read(key: "userId");
print("PRINTING GET USER DETAILS URL = ${baseURL + getUserURL(userId)}");
  return http.get(baseURL + getUserURL(userId)).then((http.Response response) async {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR GET USER DETAILS = $statusCode");
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data = $statusCode");
    }

//{
// "error":"0",
// "message":"User Exists",
// "data":{
//      "userdata":{
//                  "status":true,
//                  "_id":"5e3bb6b24a4ec50008f5c783",
//                  "username":"justforstuff",
//                  "mail":"accjustforstuff@gmail.com",
//                  "phone":"123456789"
//                  },
// "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJRCI6IjVlM2JiNmIyNGE0ZWM1MDAwOGY1Yzc4MyIsIm1haWwiOiJhY2NqdXN0Zm9yc3R1ZmZAZ21haWwuY29tIn0sImlhdCI6MTU4MDk3Mzk0MSwiZXhwIjoxNjEyNTA5OTQxfQ.t6Jw5NoyJWMoA29Xr20UuTjRfbkEUgUIWHiwgil0dVM"
//  }
// }


    var data = json.decode(response.body);

    var rest = data["data"] as List;
    list = rest.map<UserDetails>((json) {
//      print("Printing JSON = ${json + 1 }");

      return UserDetails.fromJson(json);
    }).toList();

    Future.delayed((Duration(seconds: 5)));
    print("PRINTING USER DETAILS RESPONSE = ${response.body}");
    return list;
  });
}



