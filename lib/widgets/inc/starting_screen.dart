//import '//dart:async';
import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/lib/days_page_view/days_page_controller.dart';
import 'package:todolist_unmodified_dup/lib/utils/date_time_to_string.dart';
import 'package:todolist_unmodified_dup/widgets/inc/input_screen.dart';
import 'Description_Screen.dart';
import '../../all_database_files/provider/all_classes.dart';
import 'group_screen.dart';


bool loadMore = false;

int showSnackBarForCreateTask;

int numberOfTasksLeft;
DateTime setter;
// ignore: camel_case_types
class Starting_screen extends StatefulWidget{

  final DateTime makeTextString;
  const Starting_screen(this.makeTextString, {this.id});

  final String id;

  @override
  _Starting_screenState createState() => _Starting_screenState();
}

// ignore: camel_case_types
class _Starting_screenState extends State<Starting_screen> with  TickerProviderStateMixin{





  List settervalue;
  ScrollController scrollController = new ScrollController();

  AnimationController controller;
  Animation animation;

  Widget displayTasks;
  Future _getTaskAsync;
  Future _getTaskAsyncDone;
  @override
  void initState() {

    super.initState();
    print("PRINTING SETTER = $setter");



    Stopwatch stopwatchafter = new Stopwatch()..start();
    print("INSIDE BUILD initstate = ${widget.makeTextString.toString()}");
    print('build executed in ${stopwatchafter.elapsed}');
    _getTaskAsync = getTask(1);
    controller = new AnimationController(
      duration: new Duration(milliseconds: 225),
      vsync: this,
    );

    final CurvedAnimation curve =
    new CurvedAnimation(parent: controller, curve: Curves.easeOut);

    animation = new Tween(begin: 0.0, end: 0.1).animate(curve)
      ..addListener(() => setState(() {}));
    controller.forward(from: 0.0);
    settermethod();

  }



  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }


  bool marker = false;

  settermethod() async {
    setter = widget.makeTextString;
  }





  @override
  Widget build(BuildContext context) {


    Stopwatch stopwatchbefore = new Stopwatch()..start();
    final todoappState = Provider.of<todoState>(context);

    SystemChrome.setEnabledSystemUIOverlays([]);
//    SystemChrome.setEnabledSystemUIOverlays([]);
//    final todostate = Provider.of<todoState>(context);

//    List<Datum> dataList = List();

    bool _isLoading = false;

    Future loadyourData() async {
      setState(() {
        _isLoading = true;
      });
    }


    listViewWidget(List<Post> data) {



      bool _loadingMore;
      bool _hasMoreItems;
      int _maxItems = data.length;
      int _numItemsPage = 10;
      Future _initialLoad;

      Future _loadMoreItems() async {
        await Future.delayed(Duration(seconds: 3), () {
          for (var i = 0; i < _numItemsPage; i++) {
//          getTask().then((value) => data.add());
          }
        });

        _hasMoreItems = data.length < _maxItems;
      }


      numberOfTasksLeft = data.length;

      data.sort((a, b) => b.priority.compareTo(a.priority));

      bool returnBool(){
        return true;
      }


      int returnInt(){
        return data.length;
      }

      Future<void> refresh() async {
        if (loadMore == true) {

          return getTask(1).then((user) {
            setState(() {
              data.replaceRange(0, data.length, user);
              todoappState.increment(numberOfTasksLeft);
            });
          });
        }
        else {
          return null;
        }
      }



      refresh();




      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 28,),
          Row(
            children: <Widget>[

              SizedBox(width: 20,),
              blackfontstyleBebas(text: "${returnInt()} TASKS MORE", size: 30,),
            ],
          ),
          SizedBox(height: 26,),
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IncrementallyLoadingListView(
                    hasMore: returnBool,
                    loadMore: () async {
                      // can shorten to "loadMore: _loadMoreItems" but this syntax is used to demonstrate that
                      // functions with parameters can also be invoked if needed
                      await _loadMoreItems();
                    },
                    onLoadMore: () {
                      setState(() {
                        _loadingMore = true;
                      });
                    },
                    onLoadMoreFinished: () {
                      setState(() {
                        _loadingMore = false;
                      });
                    },
                    loadMoreOffsetFromBottom: 2,
                    controller: scrollController,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: returnInt,
                    itemBuilder: (BuildContext context, int index) {


//

                      Widget returnFAB() {
                        if (data[index].priority == 1) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.green,
                                onPressed: () {
                                  setState(() {
                                    /// IMPLEMENT SET PRIORITY HERE
                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }

                        if (data[index].priority == 2) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.lightGreenAccent,
                                onPressed: () {
                                  setState(() {
                                    /// IMPLEMENT SET PRIORITY HERE
                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                        if (data[index].priority == 3) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.yellow,
                                onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                        if (data[index].priority == 4) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.orange,
                                onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                        if (data[index].priority == 5) {
                          return Transform.scale(
                            scale: 0.4,
                            child: Container(
                              child: FloatingActionButton(

                                heroTag: UniqueKey(),
                                backgroundColor: Colors.red,
                                onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                                },
                                tooltip: 'very Casual',
                                child: Text(""),
                                elevation: 0.0,
                              ),
                            ),
                          );
                        }
                      }

                      Widget returnText() {

                        return blackfontstyleMont(
                            text: data[index].taskName.toString(), size: 18);


                      }

//bool confirmDismiss = false;
                      return Transform.scale(
                        scale: 1.1,
                        child: InkWell(

                          onTap: () async {
print("PRINTING TASKID ON NAVIGATION = ${data[index].taskID}");

                            List<dynamic> result1 = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Description_Screen(
                                          data[index].taskName,
//                                      data[index].markComplete,

//                                          data[index].date,
                                          data[index].taskID,
                                          data[index].priority,

                                          data[index].groupID,
//                                          data[index].description,

//                                           data[index].subtasks,
                                          dateToStringtime(setter),
                                        data[index].description
                                      ),
                                )
                            );
                            print("11111111111111111111111");
//                            print(data[index].markComplete.runtimeType);
loadMore = true;
if(result1 == null){
  loadMore = false;
}else{



                            returnDate(){
                              if(result1[5] == null){
                                return data[index].date;
                              }else{
                                return result1[5];
                              }
                            }

                            var id = await secureStore.read(key: "userId");

                            Post taskPost = new Post(
//                                markComplete: data[index].markComplete,
                                taskName: result1[0],
                                groupID: result1[3],
                                description: result1[1],
                                taskID: result1[4],
                                date: returnDate().toString(),
                                priority: result1[2].toString(),
                              userId: id
                            );

//
// PRINTING DATATOSENDBACK = [updating task with no dec and grp, , 3, 5e429ebb9953480007c0f0c9, null, 2020-02-11 18:09:52.505695]
// 11111111111111111111111
// PRINTING TAKPOST = {group_id: 5e429ebb9953480007c0f0c9, todo_card_id: null, date: 2020-02-11 18:10:12.267528, title: true, priority: 3}

  //[new task, , 3, null, 2020-02-13 00:00:00.000]
//                            [new task1235, , 4, 5e4382efdc0d3f0007287091, null, 2020-02-12 10:23:12.870155]
                            if(result1[1] == null && result1[4] == null){
//
//                              Post taskPost = new Post(
//                                  markComplete: data[index].markComplete,
//                                  taskName: true.toString(),
////                                  groupID: result1[3],
////                                  description: result1[1],
//                                  taskID: result1[4],
//                                  date: DateTime.now().toString(),
//                                  priority: result1[2].toString());

                              print("PRINTING TAKPOST WITHOUT BOTH = ${taskPost.toMapUpdateWithoutGroupAndDescription()}");

                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateWithoutGroupAndDescription());
                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMore = false;
                              });

                            }
                            else if(result1[1] == null && result1[3] != null){
//                              Post taskPost = new Post(
//                                  markComplete: data[index].markComplete,
//                                  taskName: true.toString(),
//                                  groupID: result1[3],
////                                  description: result1[1],
//                                  taskID: result1[4],
//                                  date: DateTime.now().toString(),
//                                  priority: result1[2].toString());

                              print("PRINTING TAKPOST WITHOUT DESC = ${taskPost.toMapUpdateWithoutDescription()}");

                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateWithoutDescription());
                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMore = false;
                              });

                            }else if(result1[1] != null && result1[3] == null){
//                              Post taskPost = new Post(
//                                  markComplete: data[index].markComplete,
//                                  taskName: true.toString(),
////                                  groupID: result1[3],
//                                  description: result1[1],
//                                  taskID: result1[4],
//                                  date: DateTime.now().toString(),
//                                  priority: result1[2].toString());

                              print("PRINTING TAKPOST WITHOUT GRUP = ${taskPost.toMapUpdateWithoutGroup()}");

                              Post p = await updatePost(
                                  baseURL + updateTodoUrl,
                                  body: taskPost.toMapUpdateWithoutGroup());
                              Timer _timer;
                              _timer = new Timer(const Duration(microseconds: 1), () {
                                loadMore = false;
                              });

                            }else       if(result1[1] != null && result1[3] != null) {


                            print("PRINTING TAKPOST WITH BOTH = ${taskPost
                                .toMapUpdateCreate()}");

                            Post p = await updatePost(
                            baseURL + updateTodoUrl,
                            body: taskPost.toMapUpdateCreate());
                            Timer _timer;
                            _timer = new Timer(const Duration(microseconds: 1), () {
                              loadMore = false;
                            });


                            }

}
                          },

                          child: Dismissible(


                            key: UniqueKey(),
                            onDismissed: (direction) async {

                              if (direction.index == 2) {

                                deletePost(
                                  baseURL + deleteTodoUrl, data[index].taskID.toString(),);


                                setState(() {
                                  todoappState.decrement(numberOfTasksLeft);

                                  data.removeAt(index);
                                });


                              } else {
//String markasComplete = "true";

                                deletePost(
                                  baseURL + deleteTodoUrl, data[index].taskID.toString(),);


                                setState(() {
                                  todoappState.decrement(numberOfTasksLeft);

                                  data.removeAt(index);
                                });

                              }
//                  setState(() {
//                    marker =! marker;
//
//                    if(marker == false){
//                      uncompleteTask(data[index], false, index);
//                    }else if(marker == true){
//                      completeTask(data[index], true, index);
//                    }
//                  });

                            },
                            secondaryBackground:  Container(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 26),
                                child: Icon(Icons.delete, color: Colors.grey,),
                              ),
//                                color: Colors.black,
                              alignment: Alignment.centerRight,
                            ),
                            background: Container(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 26),
                                child: Icon(Icons.delete, color: Colors.grey,),
                              ),
//                              color: Colors.grey,
                              alignment: Alignment.centerLeft,
                            ),
                            child: Container(
//                              width: double.infinity,
//                            padding: Ed,
                              child: ListTile(
                                leading: returnFAB(),
                                title: returnText(),

                              ),
                            ),
                          ),
                        ),
                      );
                    }

                ),
              ],
            ),
          ),

        ],
      );
    }

//    print("PRINTING IN BUILD");

    return Scaffold(
backgroundColor: Color(whitecolor),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20,),

//                  Row(
//                    children: <Widget>[
//                      SizedBox(width: 15,),
//                      blackfontstyleBebas(
//                        text: "TASKS FOR TODAY - ", size: 20,),
//                    ],
//                  ),
//                  SizedBox(height: 20,),
                SizedBox(height: 7,),
                  Row(
                    children: <Widget>[
                      SizedBox(width: 15,),
                      RedfontstyleBebas(
                        text: dateToStringtime(widget.makeTextString).toUpperCase(), size: 40,),
                    ],
                  ),
//                    SizedBox(height: 110,),
                  SizedBox(height: 13,),

//
                  FutureBuilder(
                    future: _getTaskAsync,
                    builder: (context, snapshot){
                      if(snapshot.hasData){

                        return listViewWidget(snapshot.data);

                      }else{
                        return  Center(child: CircularProgressIndicator());
                      }
                    },
                  ),


                ]
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => false;

}