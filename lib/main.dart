import 'dart:math';

import 'package:charts_flutter/flutter.dart';
import 'package:fancy_on_boarding/fancy_on_boarding.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/widgets/inc/Calendar_screen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/practice_screens/practice_lazyLoading.dart';
import 'package:todolist_unmodified_dup/widgets/inc/practice_screens/testDismissible.dart';
import 'package:todolist_unmodified_dup/widgets/inc/sign_up_Screen.dart';
import 'all_database_files/provider/appState.dart';
import 'lib/days_page_view/day_page_view_example.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'widgets/inc/loginscreen.dart';
Map<DateTime, List> event;

void main() {
    initializeDateFormatting().then((_) {
        runApp(
            MyApp());

      }
    );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => groupState()),
        ChangeNotifierProvider(create: (_) => todoState()),
        ChangeNotifierProvider(create: (_) => profileState()),
        ChangeNotifierProvider(create: (_) => subTaskState()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: LoginPage(),
      ),
    );
  }
}


class MyAppOnBoard extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            initialRoute: '/',
            //Add Route to the main Page.
            routes: {'/mainPage': (context) => DayPageViewExample(DateTime.now(), )},
            title: 'Fancy OnBoarding Demo',
            theme: ThemeData(primarySwatch: Colors.teal, fontFamily: 'Nunito'),
            home: MyHomePage(title: 'Fancy OnBoarding HomePage'),
        );
    }
}

class MyHomePage extends StatefulWidget {
    MyHomePage({Key key, this.title}) : super(key: key);

    final String title;

    @override
    _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    //Create a list of PageModel to be set on the onBoarding Screens.
    final pageList = [
        PageModel(
            color: Colors.blue,
            heroAssetPath: 'lib/assets/asset 1.jpeg',
            title: Text('Hotels SVG',
                style: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    fontSize: 34.0,
                )),
            body: Text('All hotels and hostels are sorted by hospitality rating',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                )),
            iconAssetPath: 'assets/svg/key.svg',
            heroAssetColor: Colors.white),
        PageModel(
            color: Colors.indigo,
            heroAssetPath: 'lib/assets/asset 2.jpeg',
            title: Text('Banks SVG',
                style: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    fontSize: 34.0,
                )),
            body: Text(
                'We carefully verify all banks before adding them into the app',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                )),
            iconAssetPath: 'assets/svg/cards.svg',
            heroAssetColor: Colors.white),
        PageModel(
            color: Colors.amber,
            heroAssetPath: 'lib/assets/asset 3.jpeg',
            title: Text('Store SVG',
                style: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    fontSize: 34.0,
                )),
            body: Text('All local stores are categorized for your convenience',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                )),
            iconAssetPath: 'assets/svg/cart.svg',
        ),
    ];

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            //Pass pageList and the mainPage route.
            body: FancyOnBoarding(
                doneButtonText: "Done",
                skipButtonText: "Skip",
                pageList: pageList,
                onDoneButtonPressed: () =>
                    Navigator.of(context).pushReplacementNamed('/mainPage'),
                onSkipButtonPressed: () =>
                    Navigator.of(context).pushReplacementNamed('/mainPage'),
            ),
        );
    }
}
//void main() {
//   {
//        runApp(MaterialApp(
//            debugShowCheckedModeBanner: false,
//            home: MonthPageViewExample()));
//    }
//}

