
import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:todolist_unmodified_dup/all_database_files/DATA.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/main.dart';
import 'package:todolist_unmodified_dup/widgets/inc/profile_screen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';
import 'Feedback_screen.dart';
import '../../all_database_files/provider/all_classes.dart';
import 'input_screen.dart';
import '../../lib/days_page_view/day_page_view_example.dart';
import '../../lib/days_page_view/days_page_controller.dart';
import '../../lib/utils/date_time_to_string.dart';
import 'modalsheet.dart';

import 'package:http/http.dart' as http;

String next;
String previous;
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Table Calendar Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Table Calendar Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: ${first} + ${last}''');
  }


  Map<DateTime, List> _events;
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;
  bool isSwitched = false;
  Future _getTaskAsync;


  notification(){
    return
      isSwitched != isSwitched;
  }

  void _showModalSheet() {
    showModalBottomSheet(
              shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return bottom();// return a StatefulWidget widget
        });
  }

  Future initiateFetch() async{
//    Future rt
  var id= await secureStore.read(key: "userId");
  Future.delayed(Duration(seconds: 3));
  return fetchPostCalendar(body: {"month" : returnMonth(), "year" : jsonEncode(_calendarController.selectedDay.year), "user_id" : id});
  }



  @override
  void initState() {
    super.initState();
//    getTask();
//    getTaskPractice();
//    _buildEventList();
    _calendarController = CalendarController();
//    initiateFetch().then((value) =>);

//    Future.delayed(Duration(seconds: 3)).then((_) => {
//      print("PRINTING AFTER FUTURE DELAYED"),
//      _getTaskAsync = fetchPostCalendar(
//          body:
//                 {
//                    "month" : returnMonth(),
//                    "year" :  jsonEncode(_calendarController.selectedDay.year)
//                }
//      )

//    });
//_getTaskAsync = initiateFetch();
    final _selectedDay = setter;
    _events = {};


    _selectedEvents = _events[_selectedDay] ?? [];

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();

getTask().then((val) => setState(() {
_events = val;  // _events is the map
print("PRINTING EVENTS = $_events");
print("PRINTING EVENTSval = $val");

}));
//
//    getTaskPractice().then((val) => setState(() {
//  _selectedEvents = val;
//  print("PRINTING SELECTED EVENTS = $_selectedEvents");
//  print("PRINTING EVENTSval = $val");
//
//}));


  }

  returnMonth(){

    if(_calendarController.selectedDay.month == 1){
      return "january";
    }else

    if(_calendarController.selectedDay.month == 2){
      return "february";
    }else
    if(_calendarController.selectedDay.month == 3){
      return "march";
    }else
    if(_calendarController.selectedDay.month == 4){
      return "april";
    }else
    if(_calendarController.selectedDay.month == 5){
      return "may";
    }else
    if(_calendarController.selectedDay.month == 6){
      return "june";
    }else
    if(_calendarController.selectedDay.month == 7){
      return "july";
    }else
    if(_calendarController.selectedDay.month == 8){
      return "august";
    }else
    if(_calendarController.selectedDay.month == 9){
      return "september";
    }else
    if(_calendarController.selectedDay.month == 10){
      return "october";
    }else
    if(_calendarController.selectedDay.month == 11){
      return "november";
    }else
    if(_calendarController.selectedDay.month == 12){
      return "december";
    }
  }

  Future<Map<DateTime, List>> getTask() async {
    Map<DateTime, List> mapFetch = {};

    String token = await secureStore.read(key: "userToken");

    String id = await secureStore.read(key: "userId");

    await Future.delayed(const Duration(seconds: 0), () {});


    String link = baseURL + fetchTodoInCalendarUrl;
    print(link);
    print(id);
    var res = await http.post(link, headers: {"Accept": "application/json", "x-access-code" : token}, body: {"month": returnMonth(), "year" : jsonEncode(_calendarController.selectedDay.year), "user_id" : id});
    if (res.statusCode == 200) {
        Welcome event = welcomeFromJson(res.body);
    }

    Welcome event = welcomeFromJson(res.body);
setState(() {
  for (int i = 0; i < event.data.length; i++) {
    mapFetch[event.data[i].date] = [event.data[i].title];

  }
});
    return mapFetch;
  }

//  Future<List> getTaskPractice() async {
//    List mapFetch = [];
//
//    await Future.delayed(const Duration(seconds: 1), () {});
//
//    String link = fetchTodoInCalendarUrl;
//    var res = await http.post(link, headers: {"Accept": "application/json", "Content-Type":  "application/x-www-form-urlencoded"}, body: {"month" : returnMonth(), "year" : jsonEncode(_calendarController.selectedDay.year)});
//    if (res.statusCode == 200) {
//      var data = json.decode(res.body);
//      var rest = data['data'];
//      var error = data['error'];
//      print("this is error = $error");
////    print("PrintingREST = $rest");
//
//      print("PRINTING REST FOR TASKPRACTICE= $rest");
//      print("PRINTING RESBODY FOR TASK PRACTICE= ${res.body}");
//
//      final demoJsonMapEntries = rest.map((data) {
//        return MapEntry(DateTime.parse(data['date']), data['title']);
//      });
//
//      print("PRINTING DEMOJSONMAPENTRIES = $demoJsonMapEntries");
//
//      demoJsonMapEntries.forEach((e) {
//        // Normalize the `date` - this is necessary to ensure proper `Map` behavior
//        final key = DateTime.utc(e.key.year, e.key.month, e.key.day, 12);
//
//        _events.update(key, (list) => list..add(e.value), ifAbsent: () => [e.value]);
//
//        DatumCalendar event = welcomeFromJsonDatumCalendar(res.body);
////        for (int i = 0; i < res.body.length; i++) {
////          mapFetch[event.date][i] = [event.title][i];  // tried to do this but since event.date is a datetime, this statement returns an error
////
////        }
//
//      });
//    }
//
//
//    Welcome event = welcomeFromJson(res.body);
//    setState(() {
////      for (int i = 0; i < event.data.length; i++) {
////        mapFetch[event.data] = [event.data[i].title];
////
////      }
//
//    });
//    return mapFetch;
//  }


  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  _addEvent(String text) {

    final day = _calendarController.selectedDay;
    final event = text;

    setState(() {

      _events.update(
        day, (previousEvents) {
        return previousEvents..add(event);

      },

        ifAbsent: () => [event],
      );

    });
    _selectedEvents = _events[day];
  }

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');

    setState(() {
      _selectedEvents = events;
      _buildEventList();

    });
  }

  @override
  Widget build(BuildContext context) {

    final groupAppState = Provider.of<groupState>(context);

    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Scaffold(

          floatingActionButton: Stack(
            children: <Widget>[
              Positioned(
//              right: 0,
                left: 50,
                bottom: 100,
                top: 701,
//              top: 20,
                child: IconButton(
                  icon: Icon(Icons.calendar_today, color: Colors.white,size: 30,),
                  onPressed: () {
                    setState(() {
                      _calendarController.setCalendarFormat(CalendarFormat.month);
                    });
                  },
                ),

              ),
              Positioned(
//              right: 0,
//              left: 50,
                right: 30,
//              top: 0,
                bottom: -9,
//              top: 700,
//              top: 20,
                child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    color: Colors.white,size: 30,
                  ),
                  onPressed: () {
                    _showModalSheet();
                  },
                ),
              ),
              Positioned(
                right: 135.5,
                top: 675,
                child: FloatingActionButton(
                  elevation: 0.0,

                  onPressed: ()async {
//
print("PRINTING SELECTED DAY = ${_calendarController.selectedDay}");
//                    if(showSnackBarForCreateTask == 401){
//                      _showSnackBar("Task name already exists");
////                    Timer _timerNew;
////                    _timerNew = new Timer(const Duration(seconds: 2), () {
////                      showDialog(
////                          context: context,
////                          builder: (BuildContext context) {
////                            return AlertDialog(
////                              content: Form(
////                                key: _formKey,1
////                                child: Column(
////                                  mainAxisSize: MainAxisSize.min,
////                                  children: <Widget>[
////                                    Padding(
////                                      padding: EdgeInsets.all(8.0),
////                                      child: RedfontstyleBebas(text: "Thank you",size: 30,),
////                                    ),
////                                    SizedBox(height: 30,),
////                                    Padding(
////                                      padding: const EdgeInsets.all(8.0),
////                                      child: RaisedButton(
////                                        child: Text("Ok"),
////                                        onPressed: () {
////                                          Navigator.pop(context);
////                                        },
////                                      ),
////                                    )
////                                  ],
////                                ),
////                              ),
////                            );
////                          });
////                    });
//
//
//                    }

                    List<dynamic> result1 = await Navigator.push( // string which stores the user entered value
                        context,
                        MaterialPageRoute(

//                        maintainState: true,
                          builder: (context) {

                            return InputScreen(_calendarController.selectedDay); // this is the third screen
                          }, //screen which has TextField
                        ));


                    returnDate(){
                      if(result1[4] == null){
                        return _calendarController.selectedDay;
                      }else{
                        return result1[4];
                      }
                    }

                    Post taskPost = new Post(
//                        markComplete: false.toString(),
                        taskName: result1[0],
                        groupID: result1[3],
                        description: result1[1],
                        date: returnDate(),
                        priority: result1[2].toString()
                    );

                    print("PRINTING TASKPOST CALENDAR = $taskPost");
//                    Post taskPost = new Post(
////                    markComplete: false,
//                        taskName: result1[0], groupID: widget.groupID, description: result1[1],
//                        date: dateToString(setter),
//                        priority: result1[2].toString());
//                Post p = await createPost(baseURL + createTodo, body: taskPost.toMap());

                    if(result1[1] == null && result1[3] == null){
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciptionAndGroup());
                      print("Printing P = $p");
//                      loadmoreGroupDetails = true;

//                      numberOfTasksLeft = numberOfTasksLeft - 1;


                    }else if(result1[1] != null && result1[3] == null){
//                      loadmoreGroupDetails = true;

//                      numberOfTasksLeft = numberOfTasksLeft - 1;
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutGroup());
                      print("Printing P = ${p.taskName}");

                    }else if(result1[1]== null && result1[3] != null){
//                      loadmoreGroupDetails = true;
//                      numberOfTasksLeft = numberOfTasksLeft - 1;
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciption());


                    }else if(result1[1] != null && result1[3] != null){
//                      loadmoreGroupDetails = true;

//                      numberOfTasksLeft = numberOfTasksLeft - 1;
                      Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreate());


                    }



//                    Timer _timer;
//                    _timer = new Timer(const Duration(microseconds: 1), () {
////                      loadmoreGroupDetails = false;
//                    });

                  },
                  heroTag: UniqueKey(),
                  child: Icon(
                    Icons.add,
                    color: Color(whitecolor),
                  ),
                  backgroundColor: Color(redcolor),
                ),
              ),
            ],
          ),
          backgroundColor: Color(whitecolor),

          body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: (){
                          print("PRINTING SELECTED DAY ${_calendarController.selectedDay.toIso8601String()}");
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10.0, left: 15),
                          child: RedfontstyleBebas(text: "CALENDAR", size: 40,),
                        ),
                      ),
                      Padding(

                        padding: const EdgeInsets.all(12.0),
                        child: Transform.scale(
                          scale: 1.2,
                          child: IconButton(
                            key: UniqueKey(),
                            icon: Image.asset('assets/GroupGrid.png', width: 70, height: 70,),
                            tooltip: 'go to group screen',
                            onPressed: () async {
                              final PageRouteBuilder _route = PageRouteBuilder(
                                  pageBuilder: (BuildContext context, _, __) {
                                    //                                        print();
                                    String datevalue = _calendarController.selectedDay.toString();
                                    return DayPageViewExample(_calendarController.selectedDay,);
                                  });
                              await Navigator.pushAndRemoveUntil(context, _route, (Route<dynamic> r) => false);
                            },
                          ),
                        )
                      )
                    ],
                  ),
                  // Switch out 2 lines below to play with TableCalendar's settings
                  //-----------------------
//          _buildTableCalendar(),
                SizedBox(height: 15,),
                   _buildTableCalendarWithBuilders(),
                  const SizedBox(height: 8.0),
                ],
              ),

//            Expanded(
//               child: FutureBuilder(
//                 future: _getTaskAsync,
//                 builder: (context, snapshot){
//                   print("{PRINTING SNAPSHOT DATDA FOR CALENDAR = '${snapshot.data}");
//                   if(snapshot.hasData) {
//                     Future.delayed(const Duration(seconds: 1), () {});
//                     return  _buildEventList();
//
//
//                   }else{
////
//                     return  Center(child: CircularProgressIndicator());
//                   }
//                 },
//               ),
//            ),

              SingleChildScrollView(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        child: Image.asset("assets/navigation.png", color: Colors.red,)
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // Simple TableCalendar configuration (using Styles)


  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders() {

    return TableCalendar(

      locale: 'en_EN',
      calendarController: _calendarController,
      events: _events,
//      holidays: _holidays,
      initialCalendarFormat: CalendarFormat.month,

      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.blue[800]),
        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(

        formatButtonDecoration: BoxDecoration(
//          color: Colors.red,
          borderRadius: BorderRadius.circular(16.0),
          shape: BoxShape.circle
        ),
        centerHeaderTitle: false,
        formatButtonVisible: false,
      ),

      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {

          _buildEventList();
          return FadeTransition(


          opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.red,
              shape: BoxShape.circle,
              ),
              margin: const EdgeInsets.all(4.0),
//              padding: const EdgeInsets.only(top: 10.0, left: 12.0),

              padding: const EdgeInsets.all(10.5),
//              color: Colors.deepOrange[300],
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          );
        },

        todayDayBuilder: (context, date, _) {
          return Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
//                  color: Colors.blue
                border: Border.all(color: Colors.red)
            ),
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.all(9),
//            color: Colors.green,
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                  left: 1,
                  top: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          return children;
        },
      ),

      onDaySelected: (date, events) {
        _buildEventList();

        _onDaySelected(date, events);
        _animationController.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return Transform.scale(
      scale: 0.5,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: _calendarController.isSelected(date)
              ? Colors.brown[500]
              : _calendarController.isToday(date) ? Colors.red[300] : Colors.red[400],
        ),
        width: 16.0,
        height: 16.0,

      ),
    );
  }


  Widget _buildEventList() {
    return ListTile(
      subtitle: ListView.builder(
        itemCount: _selectedEvents.length,
        itemBuilder: (context, int index){
//          Future.delayed(const Duration(seconds: 3), () {});
            _buildEventList();

print("PRINTING SELECTED EVENTS = $_selectedEvents");

          return SingleChildScrollView(
            child: Column(
             children: <Widget>[
               ListTile(
                   leading: Transform.scale(
                     scale: 0.5,
                     child: FloatingActionButton(
                       elevation: 0.0,
                       heroTag: UniqueKey(),
                       onPressed: ()async{
                         print(_selectedEvents);
                         print("PRINTING CURERNT MONTH = ${_calendarController.focusedDay.month}");
                         print("PRINTNG CURRENT YEAR = ${_calendarController.focusedDay.year}");
                         print("testing listtile onpressed for ${_selectedEvents[index]}");
                         print("PRINTING NEXT = $next");
                         print("PRINTING PREVIOs = $previous");
//                         yield 1;
                       },
                       backgroundColor: Colors.blue,
                     ),
                   ),
                   title: blackfontstyleBebas(text: _selectedEvents[index].toString(),size: 20,)
               )
             ],
            ),
          );
          },
      ),
    );
  }



//  Widget _buildEventList() {
////    _buildEventList();
////  setState(() {
////    _buildEventList();
////  });
//    return ListView (
//      children: _selectedEvents
//          .map((event) => Container(
//        decoration: BoxDecoration(
//          border: Border.all(width: 0.8),
//          borderRadius: BorderRadius.circular(12.0),
//        ),
//        margin:
//        const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
//        child: ListTile(
//          title: Text(event.toString()),
//          onTap: () => print('$event tapped!'),
//        ),
//      ))
//          .toList(),
//    );
//  }
}
