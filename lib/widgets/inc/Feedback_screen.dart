import 'package:flutter/material.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';



class feedback_Screen extends StatefulWidget {
  @override
  _feedback_ScreenState createState() => _feedback_ScreenState();
}

class _feedback_ScreenState extends State<feedback_Screen> {
  final _formKey = GlobalKey<FormState>();

//returnText(){
//  if()
//}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      floatingActionButton: Stack(
        children: <Widget>[
          Positioned(
            right: 10.0,
            top: 30.0,
            child: Transform.scale(
              child: FloatingActionButton(
                heroTag: UniqueKey(),
                backgroundColor: Colors.red,
                onPressed: (){
               Navigator.pop(context);
                },
//                tooltip: 'Add a task',
                child: Icon(Icons.arrow_back),
                elevation: 0.0,
              ),
                scale: 0.8

            ),
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 14,),
Padding(
  padding: const EdgeInsets.only(top: 6, left: 16),
  child:   RedfontstyleBebas(text: "FEEDBACK", size: 40,),
),
SizedBox(height: 70,),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: RedfontstyleMont(text: "Help us to enhance your App Experience", size: 24,),
          ),
          SizedBox(height: 30,),
          Container(

            margin: EdgeInsets.all(16),
//            padding: EdgeInsets.only(left: 50),
//            width: MediaQuery.of(context).size.width * 0.8,
//            constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
            decoration:  BoxDecoration(

                shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(10),

                border: Border.all(color: Colors.grey, )
            ),
            child: TextFormField(
keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding:  EdgeInsets.all(8.0),
hoverColor: Colors.black
//enabledBorder: OutlineInputBorder(
//  borderSide: BorderSide(color: Colors.red, width: 5.0),
//),
              ),
            ),
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                onPressed: (){
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          content: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: RedfontstyleBebas(text: "Thank you",size: 30,),
                                ),
                                SizedBox(height: 30,),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    child: Text("Ok"),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      });
                },
                child: blackfontstyleBebas(text: "Send", size: 20,),
              ),
            ],
          )
        ],
      ),
    );
  }
}
