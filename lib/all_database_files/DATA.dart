import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/widgets/inc/group_screen.dart';

import 'Login_register/login.dart';
import 'all_APIs.dart';


dataArray eventFromJsonData(String str) => dataArray.fromJson(json.decode(str));

class dataArray {
//  final dynamic status;
   dynamic id;
   dynamic groupName;

//  final dynamic description;
  dynamic created_date;
//  final dynamic v;
dynamic user_id;

  dataArray({
    this.groupName,
    this.id,
//    this.created_date
  this.user_id
  });


  factory dataArray.fromJson(Map<String, dynamic> json) {
    return dataArray(
//        status: json['status'],
      id: json['_id'],
      groupName: json["name"],
        user_id: json["user_id"]

//        description: json['description'],
//        created_date: json['created_date'],
//        v: json['_v']
    );
  }

  factory dataArray.fromJsonPost(Map<String, dynamic> json) {
    return dataArray(
//        status: json['status'],
//      id: json['group_id'],
      groupName: json["name"],
user_id: json["user_id"]
//        description: json['description'],
//        created_date: json['created_date'],
//        v: json['_v']
    );
  }

  Map toMapGroup(){
    var mapGroup = new Map<String, dynamic>();
//    mapGroup['status'] = status;
//    mapGroup['_id'] = id;
    mapGroup["name"] = groupName;
//    mapGroup['description'] = description;
//    mapGroup['created_date'] = created_date;
//    mapGroup['_v'] = v;
mapGroup["user_id"] = user_id;
    return mapGroup;

  }

  Map toMapGroupUpdate(){
    var mapGroup = new Map<String, dynamic>();
//    mapGroup['status'] = status;
    mapGroup['group_id'] = id;
    mapGroup["name"] = groupName;
//    mapGroup['description'] = description;
//    mapGroup['created_date'] = created_date;
//    mapGroup['_v'] = v;

    return mapGroup;

  }

}

Future<List<dataArray>> loadJsonFromAsset() async {

  String id = await secureStore.read(key: "userId");

  String link =
      baseURL + fetchGroupsUrl + "/" + id;

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM LOAD JSON FROM ASSET = $token");

//  var id = await secureStore.read(key: "userId");

  var res = await http
      .get(Uri.encodeFull(link), headers: {"Accept": "application/json", "x-access-code" : token, "user_id" : id});
  final decoded = json.decode(res.body);

  try {
    return (decoded != null)
        ? decoded["data"]
        .map<dataArray>((item) => dataArray.fromJson(item))
        .toList()
        : [];

  } catch (e) {
    return [];
  }
}

Future<List<Post>> getTaskDescription(String groupId) async {
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM get task description = $token");

  String id = await secureStore.read(key: "userId");

  List<Post> list;
  String link =
      baseURL + groupTodoUrl(groupId) + "/" + id;
  var res = await http
      .get(Uri.encodeFull(link), headers: {"Accept": "application/json", "x-access-code" : token,});


  if(res.statusCode != 200){
    loadMoreGroup = false;
  }


  if (res.statusCode == 200) {
    var data = json.decode(res.body);
    var rest = data["data"] as List;
    var error = data['error'];
    print("this is error = $error");
//    print("PrintingREST = $rest");
    print("PRINTING RESBODY ${res.body}");
    list = rest.map<Post>((json) => Post.fromJsonCreate(json)).toList();
  }

  return list;

}

Future<List<dataArray>> getGroup() async {
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM GET Group = $token");

  Stopwatch stopwatchbefore = new Stopwatch()..start();

//    _isFetching = true;
//    notifyListeners();

  String id = await secureStore.read(key: "userId");

  List<dataArray> list;
  String link =
      baseURL + fetchGroupsUrl + "/" + id
  ;
  print("PRINTING GET GROUP LINK = $link");

  var res = await http
      .get(Uri.encodeFull(link),headers: {"Accept": "application/json" , "x-access-code" : token});
//  print(res.body);
  if(res.statusCode != 200){
    loadMoreGroup = false;
  }

  print("PRINTING RESBODY FOR fetch GROUP = ${res.body}");
  print('fetch GROUP executed in ${stopwatchbefore.elapsed}');


  if (res.statusCode == 200) {
    Stopwatch stopwatchafter = new Stopwatch()..start();

    var data = json.decode(res.body);
    var rest = data["data"] as List;
    var error = data['error'];
    print("this is error = $error");
//    print("PrintingREST = $rest");
    list = rest.map<dataArray>((json) => dataArray.fromJson(json)).toList();

    print('statuscode GROUP executed in ${stopwatchafter.elapsed}');
  }
//  print(list);
//  print("List Size: ${list.length}");
//    _isFetching = false;
//    notifyListeners();

  return list;
}






Future<dataArray> createPostGroup(String url, {Map body}) async {
//  print(body);
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM CREATE POST GROUP = $token");

  return http.post(url, body: body, headers: {"x-access-code" : token}).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code = $statusCode");
    print("THis is resbody for create group = ${response.body}");

    if(statusCode != 200){
      loadMoreGroup = false;
    }

    if(statusCode == 401){
      showSnackBarCreateGroup = true;
      print("PRINTING SHOWNACAKBAR = $showSnackBarCreateGroup");
    }

//    if(statusCode == 401)

    if (statusCode < 200 || statusCode > 400 || json == null) {
//      print(statusCode);
      throw new Exception("Error while creating group = $statusCode");
    }

//      notifyListeners();
    return dataArray.fromJsonPost(json.decode(response.body));
  });
}


Future<Map<String, dynamic>> commonDel(String url, Map jsonMap) async{
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM COMMON DEL = $token");
  Response response = await delete(url, headers: {"x-access-code" : token});

  if(response.statusCode != 200){
    loadMoreGroup = false;
  }
  print("RESPONSE COED = ${response.statusCode}");


}




//Future<dataArray> fetchPostCalendar() async {
//  Stopwatch stopwatchbefore = new Stopwatch()..start();
//
//  final response =
//  await http.get(baseURL+ fetchGroupsUrl);
//  print('fetch CALENDAR executed in ${stopwatchbefore.elapsed}');
//
//  if(response.statusCode != 200){
//    loadMoreGroup = false;
//  }
//  if (response.statusCode == 200) {
//
//
//    Stopwatch stopwatchafter = new Stopwatch()..start();
//    print(response.body);
//
//    print('statuscode CALENDR executed in ${stopwatchafter.elapsed}');
//    // If server returns an OK response, parse the JSON.
//    return dataArray.fromJson(json.decode(response.body));
//  } else {
//    // If that response was not OK, throw an error.
//    throw Exception('Failed to load post');
//  }
//}