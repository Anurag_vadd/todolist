import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:todolist_unmodified_dup/all_database_files/DATA.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import '../post_class.dart';

class todoState with ChangeNotifier{
  bool _isFetching = false;
String _jsonData = "";

  int _counter = 0;
  int get counter => _counter;
set counterSet(int value) => _counter;

  void increment(int value) {
    value++;
    notifyListeners();
  }

  void decrement(int value){
    value--;
    notifyListeners();
  }


    String get getDisplayText => _jsonData;
  bool get isFetching => _isFetching;


}

class groupState with ChangeNotifier{
  bool _isFetching = false;



  bool get isFetching => _isFetching;


}

class profileState with ChangeNotifier{
  bool _isFetching = false;

  bool get isFetching => _isFetching;




}

class subTaskState with ChangeNotifier{
  bool _isFetching = false;
  String _jsonData = "";


  Future<void> fetchGroupProvider() async{
    _isFetching = true;
    notifyListeners();

    var response = await http.get(baseURL + fetchGroupsUrl);
    if(response.statusCode == 200){
      _jsonData = response.body;
    }
    _isFetching = false;
    notifyListeners();
  }

  String get getDisplayText => _jsonData;
  bool get isFetching => _isFetching;
  List<dynamic> getResponseJson() {
    if (_jsonData.isNotEmpty) {
      Map<String, dynamic> json = jsonDecode(_jsonData);
      return json['data'];
    }
    return null;
  }

}