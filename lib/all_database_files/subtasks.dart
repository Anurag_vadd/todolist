import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';

import 'Login_register/login.dart';
import 'all_APIs.dart';



EventSubtask EventSubtaskFromJson(String str) => EventSubtask.fromJson(json.decode(str));

String EventSubtaskToJson(EventSubtask data) => json.encode(data.toJson());

class EventSubtask {
  dynamic error;
  dynamic message;
  List<DatumSubtask> data;

  EventSubtask({
    this.error,
    this.message,
    this.data,
  });

  factory EventSubtask.fromJson(Map<String, dynamic> json) => EventSubtask(
    error: json["error"],
    message: json["message"],
    data: List<dynamic>.from(json["data"].map((x) => DatumSubtask.fromJson(x))),
  );

  Map<dynamic, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())).toList(),
  };
}

DatumSubtask EventSubtaskConvert(String str) => DatumSubtask.fromJson(json.decode(str));

String EventSubtaskToJsonConvert(DatumSubtask data) => json.encode(data.toJsonCreate());

String EventSubtaskToJsonConvertWithout(DatumSubtask data) => json.encode(data.toJsonCreate());


class DatumSubtask {
  dynamic  status;
  dynamic  id;
  dynamic  groupId;
  dynamic    date;
  dynamic   title;
  dynamic  priority;
  dynamic    description;
  List<dynamic>        subtasks;
  dynamic  createdDate;
  dynamic     v;

  DatumSubtask({
    this.status,
    this.id,
    this.groupId,
    this.date,
    this.title,
    this.priority,
    this.description,
    this.subtasks,
    this.createdDate,
    this.v,
  });

  factory DatumSubtask.fromJson(Map<String, dynamic> json) => DatumSubtask(
    status: json["status"],
    id: json["_id"],
    groupId: json["group_id"],
    date: DateTime.parse(json["date"]),
    title: json["title"],
    priority: json["priority"],
    description: json["description"],
    subtasks: List<dynamic>.from(json["tasks"].map((x) => Subtask.fromJson(x))),
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  factory DatumSubtask.fromJsonCreate(Map<String, dynamic> json) => DatumSubtask(
    status: json["status"],
//    id: json["_id"],
    groupId: json["group_id"],
    date: DateTime.parse(json["date"]),
    title: json["title"],
    priority: json["priority"],
    description: json["description"],
    subtasks: List<dynamic>.from(json["tasks"].map((x) => Subtask.fromJsonNewTask(x))),
//    createdDate: DateTime.parse(json["created_date"]),
//    v: json["__v"],
  );

  factory DatumSubtask.fromJsonCreateWithoutDescription(Map<String, dynamic> json) => DatumSubtask(
    status: json["status"],
//    id: json["_id"],
    groupId: json["group_id"],
    date: DateTime.parse(json["date"]),
    title: json["title"],
    priority: json["priority"],
//    description: json["description"],
    subtasks: List<dynamic>.from(json["tasks"].map((x) => Subtask.fromJsonNewTask(x))),
//    createdDate: DateTime.parse(json["created_date"]),
//    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "_id": id,
    "group_id": groupId,
    "date": date,
    "title": title,
    "priority": priority,
    "description": description,
    "tasks": List<dynamic>.from(subtasks.map((x) => x.toJson())),
    "created_date": createdDate,
    "__v": v,
  };

  Map<String, dynamic> toJsonCreate() => {
    "status": status,
//    "_id": id,
    "group_id": groupId,
    "date": date,
    "title": title,
    "priority": priority,
    "description": description,
    "tasks": List<dynamic>.from(subtasks.map((x) => x.toJson())),
//    "created_date": createdDate,
//    "__v": v,
  };

  Map<String, dynamic> toJsonCreateWithoutDescription() => {
    "status": status,
//    "_id": id,
    "group_id": groupId,
    "date": date,
    "title": title,
    "priority": priority,
//    "description": description,
    "tasks": List<dynamic>.from(subtasks.map((x) => x.toJsonNew())),
//    "created_date": createdDate,
//    "__v": v,
  };
}

Subtask EventSubtaskConvertafter(String str) => Subtask.fromJson(json.decode(str));

String EventSubtaskToJsonConvertafter(Subtask data) => json.encode(data.toJson());

class Subtask {
 dynamic status;
 dynamic id;
 dynamic name;

  Subtask({
    this.status,
    this.id,
    this.name,
  });

  factory Subtask.fromJson(Map<dynamic, dynamic> json) => Subtask(
    status: json["status"],
    id: json["_id"],
    name: json["name"],
  );

 factory Subtask.fromJsonNewTask(Map<dynamic, dynamic> json) => Subtask(
   status: json["status"],
//   id: json["_id"],
   name: json["name"],
 );


 Map<String, dynamic> toJson() => { // for updating existing subtask
    "status": status,
    "_id": id,
    "name": name,
  };

 Map<String, dynamic> toJsonNew() => {  // for adding new subtask
   "status": status,
//   "_id": id,
   "name": name,
 };
}




Future getEventSubtasks(String url, String taskId) async {
  var id = await secureStore.read(key: "userId");

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM GET EVENT SUBTASK = $token");
  print("PRINTING LINK FOR GET EVENT SUBTASKS = $baseURL/todos/find_one_todo_detail");
  var res = await http.post("$baseURL/todos/find_one_todo_details", headers: {"Accept": "application/json", "x-access-code" : token}, body: {"todo_card_id" : taskId, "user_id" : id});
  if(res.statusCode != 200){
    loadMore = false;
  }
//print("PRINTING RES BODY RUNTYPE = ${jsonDecode(res.body).runtimeType}");
print("AFTER RUNTYPE FOR GET ONE TODO DETAILS = ${jsonDecode(res.body)}");
  var returnData = jsonDecode(res.body) as Map;

  return returnData;
}


Future getEventSubtasksList( String taskId) async {
  var id = await secureStore.read(key: "userId");


  List<Post> list;
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM GET EVENT SUBTASK LIST = $token");
  var res = await http.post(baseURL + getTodoDetailsUrl, headers: {"Accept": "application/json", "x-access-code" : token}, body: {"todo_card_id" : taskId, "user_id" : id});
  if(res.statusCode != 200){
    loadMore = false;
  }
//  print("PRINTING RES BODY RUNTYPE = ${jsonDecode(res.body).runtimeType}");
  print("AFTER RUNTYPE FOR GET SUBTASKS= ${jsonDecode(res.body)}");
  var returnData = jsonDecode(res.body) as Map;
var data = returnData["data"]["tasks"] as List;
list = data.map<Post>((json) {

  return Post.fromJsonCreate(json);
}).toList();
  return list;
}


Future deleteSubtasks(dynamic todocardId, dynamic taskId) async{

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM DELETE SUBTASK = $token");
  return http.post(baseURL + deleteSubtaskUrl, body: {"todo_card_id" : todocardId, "task_id" : taskId}, headers: {"x-access-code" : token}).then((http.Response response) {
    final int statusCode = response.statusCode;
    print("PRINTING STATUSCODE FOR DELETE SUBTASK = $statusCode");

    print("PRINTING RESBODY FOR DELETE SUBTASK = ${response.body}");
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }

    if(statusCode != 200){
      loadMore = false;
    }
    return Subtask.fromJson(json.decode(response.body));
  });
}


Future updateSubtaskRequest(dynamic todocardId, dynamic taskId) async {

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM UPDATE SUBTASK  = $token");
  return http.post(baseURL + updateSubtaskUrl, body: {"todo_card_id" : todocardId, "task_id" : taskId, "status" : "true"}, headers: {"x-access-code" : token}).then((http.Response response) {
    final int statusCode = response.statusCode;
    print("PRINTING STATUSCODE FOR DELETE SUBTASK = $statusCode");
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }

    if(statusCode != 200){
      loadMore = false;
    }
    return Subtask.fromJson(json.decode(response.body));
  });
}

Future addSubtasksRequest(dynamic todoCardId, dynamic taskName) async {

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM ADD SUBTASK = $token");
  return http.post(baseURL + addSubtaskURLUrl, body: {"todo_card_id" : todoCardId, "task_name" : taskName}, headers: {"x-access-code" : token}).then((http.Response response) {
    final int statusCode = response.statusCode;
    print("PRINTING STATUSCODE FOR add SUBTASK = $statusCode");
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }

    if(statusCode != 200){
      loadMore = false;
    }
    return Subtask.fromJson(json.decode(response.body));
  });
}


Future createTodoFromSubtask({String body})async{
  Stopwatch stopwatchbefore = new Stopwatch()..start();

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM CREATE TODO FROM SUBTASL = $token");

  return http.post(baseURL + createTodoUrl, body: body, headers: {"x-access-code" : token}).then((http.Response response) {
    print("PRINTING RESPONSE FOR CREADTODOSUBTASK= ${response.body}");
    final int statusCode = response.statusCode;
    print("PRINTING STATUSCODE = $statusCode");
    if(statusCode != 200){
      loadMore = false;
    }

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }


    print('fetch executed in ${stopwatchbefore.elapsed}');

    return DatumSubtask.fromJsonCreate(json.decode(response.body));
  });
}

Future createTodoFromSubtaskWithoutDescription({String body})async{
  Stopwatch stopwatchbefore = new Stopwatch()..start();


  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM CREATE TODO FROM SUBTASK WITHOUT DESC = $token");
  return http.post(baseURL + createTodoUrl, body: body, headers: {"x-access-code" : token}).then((http.Response response) {
    print("PRINTING RESPONSE FOR CREADTODOSUBTASK= ${response.body}");

    final int statusCode = response.statusCode;
    print("PRINTING STATUSCODE = $statusCode");
    if(statusCode != 200){
      loadMore = false;
    }
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }
//
//    if(statusCode != 200){
//      loadMore = false;
//    }
    print('fetch executed in ${stopwatchbefore.elapsed}');

    return DatumSubtask.fromJsonCreateWithoutDescription(json.decode(response.body));
  });
}