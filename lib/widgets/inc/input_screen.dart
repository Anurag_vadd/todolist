import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist_unmodified_dup/all_database_files/DATA.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/lib/utils/all.dart';
import 'package:todolist_unmodified_dup/widgets/inc/dropDownWidget.dart';
import 'package:todolist_unmodified_dup/widgets/inc/profile_screen.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';
import 'Feedback_screen.dart';
import 'package:http/http.dart' as http;

import 'modalsheet.dart';

dynamic taskID;
class InputScreen extends StatefulWidget {
  final DateTime date;

  const InputScreen( this.date);
  @override
  _InputScreenState createState() => _InputScreenState();
}

class _InputScreenState extends State<InputScreen> {

  List<dataArray> _list = [];
  dataArray _selectedMenuItem;

  String selected = "";
dynamic group_id;


  Future initJson() async {
    _list = await loadJsonFromAsset();

    setState(() {
      for(int i =0; i<=_list.length - 1; i++) {
        _selectedMenuItem = _list[i];
      }

    });

  }
  @override
  void initState() {
    initJson();
    super.initState();
  }


  ///```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
  ScrollController scrollController = new ScrollController();


  TextEditingController taskcontroller = new TextEditingController();
  TextEditingController newcontroller = new TextEditingController();
  TextEditingController descriptioncontroller = new TextEditingController();
  TextEditingController subtaskcontroller = new TextEditingController();
  bool _validate =false;

  List data = List();

  ignoreFunction(){
  print(taskcontroller.text);
  if(newcontroller.text.isEmpty || floatingButtonValue == null){
    return true;
  }else{
    return false;
  }
}



Widget returnText(){
  if(newcontroller.text.isEmpty || floatingButtonValue == null){
    return blackfontstyleBebasDone(text: "DONE", size: 20,);
  }else{
    return blackfontstyleBebas(text: "DONE",size: 20,);
  }
}

int floatingButtonValue;

  void setfloatingbuttonvalue1(){
    setState(() {
      floatingButtonValue = 1;
    });
  }

  void setfloatingbuttonvalue2(){
    setState(() {
      floatingButtonValue = 2;
    });
  }
  void setfloatingbuttonvalue3(){
    setState(() {
      floatingButtonValue = 3;
    });
  }
  void setfloatingbuttonvalue4(){
    setState(() {
      floatingButtonValue = 4;
    });
  }
  void setfloatingbuttonvalue5(){
    setState(() {
      floatingButtonValue = 5;
    });
  }

  setIcon1(int value){
    if(value == 1){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon2(int value){
    if(value == 2){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon3(int value){
    if(value == 3){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon4(int value){
    if(value == 4){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon5(int value){
    if(value == 5){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setBox1(int value){
    if(value == 1){
      return BoxDecoration(
        shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
        shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox2(int value){
    if(value == 2){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox3(int value){
    if(value == 3){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox4(int value){
    if(value == 4){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox5(int value){
    if(value == 5){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  var dateValuetosendback;
var notificationDuration;
  
  Future _selectDayAndTime(BuildContext context) async{


 DateTime _selectedDay = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2018),
        lastDate: DateTime(2030),
        builder: (BuildContext context, Widget child) => child
    );

//    TimeOfDay _selectedTime = await showTimePicker(
//      context: context,
//      initialTime: TimeOfDay.now(),
//
//    );


    if(_selectedDay != null) {
      //a little check
      print("PRINTING DATE TIME = $_selectedDay");
//      print("PRINTING  TIME = $_selectedTime");
//_selectedDay.add(Duration(hours: _selectedTime.hour, minutes: _selectedTime.minute));

      var dateValue = new DateTime(_selectedDay.year, _selectedDay.month, _selectedDay.day, );
      print(dateValue);
      dateValuetosendback = dateValue;
      
//      notificationDuration = DateTime.now().difference(dateValue).inMinutes;
      return dateValue;
//      print(_selectedDay);
//      return _selectedDay;
      //todo : ADD REMINDER HERE
    }
  }


  void _showModalSheet() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {

          return repeatSheet(widget.date);// return a StatefulWidget widget
        });
  }


  List<Widget> subtaskList = [];




  addSubtask(String item){
    subtaskList.add(
        ListTile(
          leading: Transform.scale(
            scale: 0.6,
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,

                  border: Border.all(color: Colors.red)
              ),
              child: FloatingActionButton(
                  heroTag: UniqueKey(),

                  foregroundColor: Colors.white,
                  backgroundColor: Colors.white,
                  elevation: 0,
                  child: Text("")
              ),
            ),
          ),
          title: blackfontstyleBebas(text: subtaskcontroller.text,),
        ));
  }

  returnDate(){
    if(dateValuetosendback == null){
      return widget.date;
    }else{
      return dateValuetosendback;
    }
  }


  @override
  Widget build(BuildContext context) {


    DropdownMenuItem<dataArray> buildDropdownMenuItem(dataArray item) {
      return DropdownMenuItem(

        value: item, // you must provide a value
        child: Text(item.groupName),
      );
    }

    Widget buildDropdownButton() {
      return Padding(
        padding: const EdgeInsets.only(left: 20),
        child: DropdownButton<dataArray>(
//            elevation: 1,
          hint: Text("Select one"),
//            isExpanded: true,
          underline: Container(
            height: 2,
            color: Colors.transparent,
          ),
          items: _list.map((item) => buildDropdownMenuItem(item)).toList(),
          value: _selectedMenuItem, // values should match
          onChanged: (dataArray item) {
            setState(() {

              return _selectedMenuItem = item;
            });
            print("Printing itemid = ${item.id}");
            group_id = item.id;
            return item.id;
          },
        ),
      );
    }




    returnDescription(String text){
      if(text == null){
        return null;
      }else if(text != null){
        return text;
      }
    }

    SystemChrome.setEnabledSystemUIOverlays([]);
///**/    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//      systemNavigationBarColor: Colors.blueGrey,
//      statusBarColor: Colors.transparent,
//    ));
    return Material(
      child: Scaffold(

              floatingActionButton: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                color: Colors.white,
                child: Stack(

                  children: <Widget>[

                    Positioned(
                      right: 0,
                      bottom: 0,
//                  top: 1,
                      child: Container(
                        child: FlatButton(
                          onPressed: (){
                            setState(() {
                              Navigator.pop(context, null);

                            });
                          },
                          child: blackfontstyleBebas(text: "CANCEL",size: 20,),
                        )
                      ),
                    ),
                    Positioned(
                      right: 70,
                      bottom: 0,
//                  top: 1,
                      child: Container(
                          child: IgnorePointer(
                            ignoring: ignoreFunction(),
                            child: FlatButton(
                              onPressed: () async {
//                              print()
                                String textToSendBack = newcontroller.text;
                                String descriptionToSendBack = descriptioncontroller.text;
                                int priorityToSendBack = floatingButtonValue;

                                List<dynamic> dataToSendBack;
                                // PRINTING RESULT! =  [xitcitcytid, , 5, null, 2020-02-11 17:33:47.969479]
                                dataToSendBack = [textToSendBack, descriptionToSendBack, priorityToSendBack, group_id, returnDate()];

                                print("PRINTING RESULT! = $dataToSendBack");


                                if(textToSendBack.isNotEmpty) {
                                    Navigator.pop(context,dataToSendBack);


                                }else{

                                      taskcontroller.dispose();
                                      Navigator.pop(context);

                                  }
                                },
                              child: returnText()
                            ),
                          )
                      ),
                    ),
                  ],
                ),
              ),
          resizeToAvoidBottomPadding: true,
          backgroundColor: Colors.white,
          body: SingleChildScrollView(

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
       crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 10,),
                Row(
                  children: <Widget>[
            SizedBox(width: 16,),
                    RedfontstyleBebas(text: dateToStringtime(widget.date) ,size: 40,),
                  ],
                ),

              SizedBox(height: 16,),

              Container(
                height: 60,
                  width: MediaQuery.of(context).size.width,
//                  color: Colors.grey,
                  child: buildDropdownButton()
              ),

                Container(
                  color: Colors.white,
//                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[


//                        //Todo: remove sized box below and display tasks there
                        TextFormField(

                          validator: (String value) {
                            return value.isEmpty ? "task must have a name" : null;
                          },
                          textAlign: TextAlign.start,
//                          maxLength: 100,
                          controller: newcontroller, // Just an ordinary TextController
                          onChanged: (value) {
                            setState(() {
                              value = newcontroller.text;
                            });
                          },

                          decoration: InputDecoration(
                            border: InputBorder.none,
                              errorText:
                              _validate // Just a boolean value set to false by default
                                  ? 'Value Can\'t Be Empty'
                                  : null,
                              labelText: "Name of task",
                          ),
                          style: TextStyle(height: 1.2, fontSize: 20, color: Colors.black87),
                        ),

                        SizedBox(height: 16,),
                        TextField(

                          controller: descriptioncontroller,
                          onEditingComplete: (){
                            /// IMPLEMENT ADD DETAIL API CALL HERE
                          },
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(Icons.short_text),
                            labelText: "Add details"
                          ),

                        ),

//                        SizedBox(height: 20,),
                        InkWell(
                          onTap: (){
                            _selectDayAndTime(context);
                          },
                          child: TextField(
enabled: false,
//                          controller: descriptioncontroller,
                            onEditingComplete: (){
                              /// IMPLEMENT ADD DETAIL API CALL HERE
                            },
                            decoration: InputDecoration(

                                border: InputBorder.none,
                                icon: Padding(
                                  padding: const EdgeInsets.only(bottom: 0),
                                  child: Icon(Icons.calendar_today),
                                ),
                                contentPadding: EdgeInsets.only(top: 6),
                                hintText: "Add Date/Time"
                            ),
style: TextStyle(fontSize: 16, color: Colors.red.shade600),
                          ),
                        ),
                        SizedBox(height: 20,),


//                        InkWell(
//                          onTap: (){
//                           _showModalSheet();
//                          },
//                          child: Container(
//                            width: MediaQuery.of(context).size.width,
//                            height: 30,
//                            child: Row(
//                              mainAxisAlignment: MainAxisAlignment.start,
//                              children: <Widget>[
//                                Icon(
//                                  Icons.repeat,
//                                  color: Colors.grey.shade500,
//                                ),
//                                SizedBox(width: 19,),
//                                Text("Repeat Task", style: TextStyle(color: Colors.grey.shade500, fontSize: 16),)
//                              ],
//                            ),
//                          ),
//                        ),

//                        SizedBox(height: 20,),
//                        Container(
//
////                        color: Colors.black,
//                          width: MediaQuery.of(context).size.width,
//                          height: 30,
//                          child:InkWell(
//                            onTap: (){
//                              print("clicked alarm yo");
//                            },
//                            child: Row(
//                              mainAxisAlignment: MainAxisAlignment.start,
//                              children: <Widget>[
//                                Icon(
//                                  Icons.alarm,
//                                  color: Colors.grey.shade500,
//                                ),
//                                SizedBox(width: 19,),
//                                Text("Add alarm", style: TextStyle(color: Colors.grey.shade500, fontSize: 17),)
//                              ],
//                            ),
//                          ),
//                        ),
//                      SizedBox(height: 20,),
//                        SizedBox(height: 10,),
                        SizedBox(height: 20,),

//                        ExpansionTile(
//                            title: blackfontstyleBebas(text: "Subtasks (${subtaskList.length})"),
//                            initiallyExpanded: true,
//                            children: subtaskList
//                        ),
//                        TextField(
//
//                          controller: subtaskcontroller,
//                          onTap: (){
//                            /// IMPLEMENT add subtasks here
//                            print("textfield tapped");
//                          },
//                          onEditingComplete: (){
//                            setState(() {
//                              addSubtask(subtaskcontroller.text);
//
//                            });
//                          },
//                          decoration: InputDecoration(
//                            enabled: true,
//                              icon: Icon(Icons.subdirectory_arrow_right),
//                              labelText: "Add Subtasks"
//                          ),
//                        ),


//                        SizedBox(height: 20,),


                        blackfontstyleBebas(text: "Priority", size: 20,),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Transform.scale(
                              scale: 0.8,
                              child: Container(
                                decoration: setBox1(floatingButtonValue),
                                child: FloatingActionButton(

                                  heroTag: UniqueKey(),
                                  backgroundColor: Colors.green,
                                  onPressed: (){

                                    print("PRINTING FAB VALUE = $floatingButtonValue");
                                    setState(() {
                                      setfloatingbuttonvalue1();
                                      /// IMPLEMENT SET PRIORITY HERE
                                    });
                                  },
                                  tooltip: 'very Casual',
                                  child: setIcon1(floatingButtonValue),
                                  elevation: 0.0,
                                ),
                              ),
                            ),
                            Transform.scale(
                              scale: 0.8,
                              child: Container(
                                decoration: setBox2(floatingButtonValue),
                                child: FloatingActionButton(
                                  heroTag: UniqueKey(),
                                  backgroundColor: Colors.lightGreenAccent,
                                  onPressed: (){

                                    print("PRINTING FAB VALUE = $floatingButtonValue");
                                    setState(() {
                                      setfloatingbuttonvalue2();
                                    });
                                  },
                                  tooltip: 'Casual',
                                  child: setIcon2(floatingButtonValue),
                                  elevation: 0.0,
                                ),
                              ),
                            ),
                            Transform.scale(
                              scale: 0.8,
                              child: Container(
                                decoration: setBox3(floatingButtonValue),
                                child: FloatingActionButton(
                                  heroTag: UniqueKey(),
                                  backgroundColor: Colors.yellow,
                                  onPressed: (){

                                    print("PRINTING FAB VALUE = $floatingButtonValue");
                                    setState(() {
                                      setfloatingbuttonvalue3();
                                    });
                                  },
                                  tooltip: 'Moderate',
                                  child: setIcon3(floatingButtonValue),
                                  elevation: 0.0,
                                ),
                              ),
                            ),
                            Transform.scale(
                              scale: 0.8,
                              child: Container(
                                decoration: setBox4(floatingButtonValue),
                                child: FloatingActionButton(
                                  heroTag: UniqueKey(),
                                  backgroundColor: Colors.orange,
                                  onPressed: (){

                                    print("PRINTING FAB VALUE = $floatingButtonValue");
                                    setState(() {
                                      setfloatingbuttonvalue4();
                                    });
                                  },
                                  tooltip: 'Urgent',
                                  child: setIcon4(floatingButtonValue),
                                  elevation: 0.0,
                                ),
                              ),
                            ),
                            Transform.scale(
                              scale: 0.8,
                              child: Container(
                                decoration: setBox5(floatingButtonValue),
                                child: FloatingActionButton(
                                  heroTag: UniqueKey(),
                                  backgroundColor: Colors.red,
                                  onPressed: (){

                                    print("PRINTING FAB VALUE = $floatingButtonValue");
                                    setState(() {
                                      setfloatingbuttonvalue5();
                                    });
                                  },
                                  tooltip: 'Very Urgent',
                                  child: setIcon5(floatingButtonValue),
                                  elevation: 0.0,
                                ),
                              ),
                            ),

                          ],
                        ),
                      ]
                  ),
                ),
                SizedBox(height: 90,)
              ],
            ),
          ),
        ),
    );
  }
}
