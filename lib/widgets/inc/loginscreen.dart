//import '//dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


import 'package:email_validator/email_validator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/register.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/token_class.dart';
import 'package:todolist_unmodified_dup/lib/days_page_view/day_page_view_example.dart';
import 'package:todolist_unmodified_dup/main.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/widgets/inc/password_reset.dart';
import 'package:todolist_unmodified_dup/widgets/inc/sign_up_Screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;


bool validateCredentials = true;

bool validatorPassword = true;

bool navigateTo = false;



class SecureStoreMixin{

  final secureStore = new FlutterSecureStorage();

  void setSecureStore(String key, String data) async {
    await secureStore.write(key: key, value: data);
  }

  void getSecureStore(String key) async {
    await secureStore.read(key: key);
  }

}

class LoginPage extends StatefulWidget with SecureStoreMixin{
final String email;
final String password;

LoginPage({this.email, this.password});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>  with WidgetsBindingObserver{
//  TextEditingController nameController = new TextEditingController();
  TextEditingController mailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  void autoLogIn() async {
    final String userToken = await secureStore.read(key: "userToken");

    if (userToken != null) {
//      setState(() {
//        isLoggedIn = true;
//        name = userId;
        await Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => DayPageViewExample(DateTime.now(),)
            ));
//      });
      return;
    }
  }

  String _email;
  String _password;

  final _storage = FlutterSecureStorage();


  bool isLoggedIn = false;
  var profileData;
  var facebookLogin = FacebookLogin();

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }



VoidCallback _onTap;

  String storedKey;

storeMixin(int i,String key, String value){
    SecureStoreMixin mixinStore = new SecureStoreMixin();
if( i == 1) {
  mixinStore.setSecureStore(key, value);
  }else if(i ==2){
  mixinStore.getSecureStore(key);
}
}


  void getMessage(){
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
          setState(() => _message = message["notification"]["title"]);
        }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      setState(() => _message = message["notification"]["title"]);
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      setState(() => _message = message["notification"]["title"]);
    });
  }
  bool _isLoggedIn = false;
  Map userProfile;
  void initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult =
    await facebookLogin.logIn([ 'public_profile']);
    print("PRINTING FACEBOOK BODY =  ${facebookLogin.logIn([ 'public_profile']).then((value) => {print(value)})} ");

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print(facebookLoginResult.status);
        print(facebookLoginResult.errorMessage);
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.loggedIn:
        final token = facebookLoginResult.accessToken.token;
        final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}');
        final profile = jsonDecode(graphResponse.body);
        print(profile);
        setState(() {
          userProfile = profile;
          _isLoggedIn = true;
        });
        //{
        // name: Anurag Vadavathy,
        // picture: {data: {
        // height: 50,
        // is_silhouette: false,
        // url: https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1315942121947382&height=50&width=50&ext=1583318448&hash=AeQ2LeIVC5Mn-3Cl,
        // width: 50
        // }},
        // email: anurag.vadd@gmail.com,
        // id: 1315942121947382
        // }

        registerFaceBook(baseURL + facebookSignInUrl, body: {"fb_id" : profile["id"], "username" : profile["name"]});
        onLoginStatusChanged(true);
//        var firebaseUser = await (
//            token: facebookLoginResult.accessToken);
        await Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => DayPageViewExample(DateTime.now(),)
            ));
        break;
    }
  }

  String _message = '';

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _register() {
    _firebaseMessaging.getToken().then((token) => print(token));
  }

  final storage = new FlutterSecureStorage();
  var myPassword;


  readSecureStorage()async{
    myPassword = await storage.readAll().then((value) => {print("PRINTING INITSTATE VALUE = $value")});
    
  }


  returnTextEmail(){
    if(widget.email.isNotEmpty){
      return widget.email;
    }else{return "";
    }
  }

  returnTextPassword(){
    if(widget.password.isNotEmpty){
      return widget.password;
    }else{
      return "";
    }
  }

  @override
  void initState(){
    super.initState();
autoLogIn();
    readSecureStorage();

//    mailController.text = returnTextEmail();
//    passwordController.text = returnTextPassword();

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    @override
  Widget build(BuildContext context) {
      _showSnackBar() {
//        final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

        print("Show Snackbar here !");
        final snackBar = new SnackBar(
          content: new Text("Invalid Credentials"),
          duration: new Duration(seconds: 3),
          backgroundColor: Colors.grey,
          action: new SnackBarAction(label: 'Ok', onPressed: (){
            print('press Ok on SnackBar');
          }),
        );
        //How to display Snackbar ?
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }

      _showSnackBarEmptyEmail() {
        print("Show Snackbar here !");
        final snackBar = new SnackBar(
          content: new Text("Please enter Email"),
          duration: new Duration(seconds: 3),
          backgroundColor: Colors.grey,
          action: new SnackBarAction(label: 'Ok', onPressed: (){
            print('press Ok on SnackBar');
          }),
        );
        //How to display Snackbar ?
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }


      _showSnackBarEmptyPassword() {
        print("Show Snackbar here !");
        final snackBar = new SnackBar(
          content: new Text("Please enter Password"),
          duration: new Duration(seconds: 3),
          backgroundColor: Colors.grey,
          action: new SnackBarAction(label: 'Ok', onPressed: (){
            print('press Ok on SnackBar');
          }),
        );
        //How to display Snackbar ?
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }

      return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(whitecolor),

//      floatingActionButton: FloatingActionButton(
//        onPressed: () async {
////          secureStore.deleteAll();
//          _register();
//          await Navigator.pushReplacement(
//              context,
//              MaterialPageRoute(
//                builder: (context) => DayPageViewExample(DateTime.now()),
//              ));
//          WidgetsBinding.instance.removeObserver(this);
//
//        },
//      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          child: Center(
            child: Column(

              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    validator: (val) => !EmailValidator.validate(val, true)
                        ? 'Not a valid email.'
                        : null,
                    onChanged: (val) => _email = val,
                    keyboardType: TextInputType.emailAddress,

                    controller: mailController,
                    enabled: true,
                    decoration: InputDecoration(

                        prefixIcon: Icon(Icons.mail, color: Color(redcolor),),
                        labelText: "Email"
                    ),
                    style: TextStyle(
                        height: 1.2, fontSize: 16, color: Colors.black87),
                  ),
                  TextFormField(
                    onChanged: (value) {
                      return _password = value;
                    },
                    validator: (val) => validatorPassword
                        ? 'Not a valid Password.'
                        : null,

                    controller: passwordController,
                    enabled: true,
                    obscureText: true,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock, color: Color(redcolor),),
                        labelText: "Password",

                    ),
                    style: TextStyle(
                        height: 1.2,
                        fontSize: 16,
                        color: Colors.black87
                    ),
                  ),
                ],
              ),
            ),
                SizedBox(height: 20,),

                Transform.scale(
                  scale: 1.2,
                  child: Center(
                      child: FlatButton(

                          onPressed: ()async{
                            if(mailController.text.isEmpty){
                              _showSnackBarEmptyEmail();
                            }

                            if(passwordController.text.isEmpty){
                              _showSnackBarEmptyPassword();
                            }


                            Login loginRequest = new Login(mailController.text, passwordController.text);
                            Login createloginRequest = await createLogin(baseURL + loginURL, body: loginRequest.toMap());

//                          await storage.write(key: "userData", value: mailController.text);
                            if(navigateTo == true){


                              Future.delayed(Duration(seconds: 3));

                              await Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DayPageViewExample(DateTime.now(),)
                                  ));
                            }else{
                              _showSnackBar();
                              return  null;
                            }

                          },
                          color: Colors.red,

                          child: Text("Log In", style: TextStyle(color: Colors.white, ),)
                      )
                  ),
                ),

                SizedBox(height: 20,),

                GestureDetector(
                    onTap: ()async{
                      myPassword = await storage.read();
                      Map list = await storage.readAll();
                      print("PRINTING MAP = $list");
                      print("PRINTING STRING = $myPassword");
                    },
                    child: Text("Or", style: TextStyle(color: Colors.black, fontSize: 15),)),

                SizedBox(height: 20,),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Transform.scale(
                      scale: 1.1,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.red)
                        ),
                        child: FloatingActionButton(
                          heroTag: UniqueKey(),
                          onPressed: () async {
                            initiateFacebookLogin();
//                          if(navigateTo == true){
//
//
//                            Future.delayed(Duration(seconds: 3));
//
//                            await Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                    builder: (context) => DayPageViewExample(DateTime.now(),)
//                                ));
//                          }
                          },
                          foregroundColor: Colors.white,
                          backgroundColor: Colors.white,
                          elevation: 0,
                          child: Transform.scale(
                            scale: 1.8,
                            child: new IconButton(
                              icon: new Image.asset('assets/facebook.png'),
                            ),
                          )
                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
//                  Container(
//                    decoration: BoxDecoration(
//                        shape: BoxShape.circle,
//                        border: Border.all(color: Colors.red)
//                    ),
//                    child: FloatingActionButton(
//                      heroTag: UniqueKey(),
//
//                      onPressed: (){},
//                      foregroundColor: Colors.white,
//                      backgroundColor: Colors.white,
//                      elevation: 0,
//                      child: Icon(
//                        const IconData(0xf099, fontFamily: "Twitter"),
//                        color: Color(redcolor),
//                      ),
//                    ),
//                  ),
//                  SizedBox(width: 15,),

                    Transform.scale(
                      scale: 1.1,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.red)
                        ),
                        child: FloatingActionButton(
                            heroTag: UniqueKey(),

                            onPressed: () async {
                              signInWithGoogle()
                                  .then((value) async => {

                              await Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                              builder: (context) => DayPageViewExample(DateTime.now(),)
                              ))
                              });
//                            if(navigateTo == true){
//
//
//                              Future.delayed(Duration(seconds: 5));
//
//                              await Navigator.push(
//                                  context,
//                                  MaterialPageRoute(
//                                      builder: (context) => DayPageViewExample(DateTime.now(),)
//                                  ));
//                            }
                            },
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.white,
                            elevation: 0,
                            child: Transform.scale(
                              scale: 0.9,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: new IconButton(
                                  icon: new Image.asset('assets/google-plus.png'),

                                ),
                              ),
                            )
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 20,),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Text("New User?", style: TextStyle(color: Colors.black, fontSize: 15),),
                    SizedBox(width: 5,),
                    InkWell(
                      onTap: () async {
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SignUpPage(),
                            ));
                      },
                      child: Text("Sign Up", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, decoration: TextDecoration.underline),),)
                  ],
                ),

                SizedBox(height: 20,),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Text("Forgot Password?", style: TextStyle(color: Colors.black, fontSize: 15),),
                    SizedBox(width: 5,),
                    InkWell(
                      onTap: () async {
                        String mail = await  secureStore.read(key: "userMail");
                        print("PRINTING MAIL FORGOT PASSWORD = $mail");
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => password_Screen(),
                            ));
                      },
                      child: Text("Click here to reset", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, decoration: TextDecoration.underline),),)
                  ],
                )

              ],
            ),
          ),
        ),
      ),
    );
  }


}

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<String> signInWithGoogle() async {
  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
  await googleSignInAccount.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  //FirebaseUser
  // ({uid: HVqo7Ea9niOU2jYj8OViQbDBpjj1,
  // photoUrl: https://lh3.googleusercontent.com/-Peyw6emDs0M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdnPsxxyhIrcMrsRPjjDI3ISbdDgA/s96-c/photo.jpg,
  // isAnonymous: false,
  // providerData:
  //      [
  //      {
  //      uid: HVqo7Ea9niOU2jYj8OViQbDBpjj1,
  //      photoUrl: https://lh3.googleusercontent.com/-Peyw6emDs0M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdnPsxxyhIrcMrsRPjjDI3ISbdDgA/s96-c/photo.jpg,
  //      providerId: firebase,
  //      displayName: Anurag Vad,
  //      email: anurag.vadd@gmail.com
  //    },
  //    {
  //    uid: 103653182121632479507,
  //    photoUrl: https://lh3.googleusercontent.com/-Peyw6emDs0M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdnPsxxyhIrcMrsRPjjDI3ISbdDgA/s96-c/photo.jpg,
  //    providerId: google.com,
  //    displayName: Anurag Vad,
  //    email: anurag.vadd@gmail.com
  //    }],
  //    providerId: firebase,
  //    displayName: Anurag Vad,
  //    creationTimestamp: 1577354203922,
  //    lastSignInTimestamp: 1580724053772,
  //    email: anurag.vadd@gmail.com,
  // isEmailVerified: true
  // })

  final AuthResult authResult = await _auth.signInWithCredential(credential);
  print("PRINTING FIREBASE SIGN IN = ${authResult.user}");
  final FirebaseUser user = authResult.user;
  var userAPI = user.email;
  print("PRINTING SERAPI = $userAPI");
  var username = user.displayName;

  print("PRINTING SERAPI = $username");
  googleSignUpAPI(baseURL + googleSignInURL, body: {
    "mail" : user.email, "username": user.displayName
  });

  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);

  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);

  return 'signInWithGoogle succeeded: $user';
}
void signOutGoogle() async{
  await googleSignIn.signOut();

  print("User Sign Out");
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(color: Colors.blue[100]),
    );
  }
}

