import 'package:charts_flutter/flutter.dart' as prefix0;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:url_launcher/url_launcher.dart';


class Graph_screen_2 extends StatefulWidget {
  @override
  _Graph_screen_2State createState() => _Graph_screen_2State();
}

class _Graph_screen_2State extends State<Graph_screen_2> {
  bool showAvg = false;

  ToggleMonthly(){
    setState(() {
      setter = 0;
    });
  }

  ToggleYearly(){
    setState(() {
      setter = 1;
    });
  }


  ToggleOverall(){
    setState(() {
      setter = 2;
    });
  }

  int setter = 0;

  Displaygraph(){
    if(setter == 0){
      return avgData();
    }else if(setter == 1){
      return mainData();
    }else if(setter ==2){
      return null;
    }

  }


  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  LineChartData avgData() {
    return LineChartData(
      lineTouchData: const LineTouchData(enabled: false),
      gridData: FlGridData(
        show: true,
        drawHorizontalGrid: false,
//        getDrawingVerticalGridLine: (value) {
//          return const FlLine(
//            color: Color(0xff37434d),
//            strokeWidth: 0.5,
//          );
//        },
        getDrawingHorizontalGridLine: (value) {
          return const FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(

        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: TextStyle(
              color: const Color(blackcolor),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 2:
                return 'MAR';
              case 5:
                return 'JUN';
              case 8:
                return 'SEP';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          textStyle: TextStyle(
            color: const Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '10k';
              case 3:
                return '30k';
              case 5:
                return '50k';
            }
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            FlSpot(0, 3.44),
            FlSpot(2.6, 3.44),
            FlSpot(4.9, 3.44),
            FlSpot(6.8, 3.44),
            FlSpot(8, 3.44),
            FlSpot(9.5, 3.44),
            FlSpot(11, 3.44),
          ],
          isCurved: true,
          colors: [
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2),
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2),
          ],
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(show: true, colors: [
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2)
                .withOpacity(0.1),
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2)
                .withOpacity(0.1),
          ]),
        ),
      ],
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalGrid: false,
//        getDrawingHorizontalGridLine: (value) {
//          return const FlLine(
//            color: Color(0xff37434d),
//            strokeWidth: 1,
//          );
//        },
//        getDrawingVerticalGridLine: (value) {
//          return const FlLine(
//            color: Color(0xff37434d),
//            strokeWidth: 1,
//          );
//        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: TextStyle(
              color: const Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 2:
                return 'MAR';
              case 5:
                return 'JUN';
              case 8:
                return 'SEP';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          textStyle: TextStyle(
            color: const Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '10k';
              case 3:
                return '30k';
              case 5:
                return '50k';
            }
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            FlSpot(0, 3),
            FlSpot(2.6, 2),
            FlSpot(4.9, 5),
            FlSpot(6.8, 3.1),
            FlSpot(8, 4),
            FlSpot(9.5, 3),
            FlSpot(11, 4),
          ],
          isCurved: true,
          colors: gradientColors,
          barWidth: 2,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
            gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton:  Stack(
          children: <Widget>[
            Positioned(
              right: 10,
              top: 270,
              child: Column(
                children: <Widget>[


                    blackfontstyleBebas(text: " 11", size: 40,),

                blackfontstyleBebas(text: "april 12", size: 20,),

                ],
              ),
            ),
            Positioned(
              right: 10.0,
              top: 45.0,
              child: Transform.scale(
                  child: FloatingActionButton(
                    heroTag: UniqueKey(),
                    backgroundColor: Colors.red,
                    onPressed: (){
                      Navigator.pop(context);
                    },
//                tooltip: 'Add a task',
                    child: Icon(Icons.arrow_back),
                    elevation: 0.0,
                  ),
                  scale: 0.8

              ),
            )
          ],
        ),
//        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
//        backgroundColor: Colors.black87,
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10, top: 40),
                child: blackfontstyleBebas(text: "Productivity", size: 35,),
              ),
              SizedBox(height: 40,),
              Padding(
                padding: const EdgeInsets.only(left: 10,),
                child: blackfontstyleBebas(
                  text: "You are on a 49 day streak", size: 25,),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, top: 10),
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 10,),
                    GestureDetector(
                      onTap: () {
                        launch("https://twitter.com");
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        width: 40,
                        height: 40,
                        child: Icon(
                          const IconData(0xf099, fontFamily: "Twitter"),
                          color: Color(whitecolor),
                        ),
                      ),
                    ),
                    SizedBox(width: 10,),
                    GestureDetector(
                      onTap: () {
                        launchURLFacebook();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        width: 40,
                        height: 40,
                        child: Icon(
                          const IconData(0xf09a, fontFamily: "Facebook"),
                          color: Color(whitecolor),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: blackfontstyleBebas(text: "TASK evaluation", size: 25,),
                     ),

              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    onPressed: (){
                      ToggleMonthly();
                    },
                    child: RedfontstyleBebas(text: "Month", size: 20,),
                  ),
                  FlatButton(
                    onPressed: (){
                      ToggleYearly();
                    },
                    child: RedfontstyleBebas(text: "YEar", size: 20,),
                  ),
                  IgnorePointer(
                    ignoring: true,
                    child: FlatButton(
                      onPressed: (){
                        ToggleOverall();
                      },
                      child: RedfontstyleBebas(text: "overall", size: 20,),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width - 30,
                height: 300,
                child: LineChart(
                  Displaygraph()
                )
              ),

            ]
        )
    );
  }
}
