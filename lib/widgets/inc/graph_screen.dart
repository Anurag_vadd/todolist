
//import 'dart:html';

//import 'dart:html';

import 'package:bezier_chart/bezier_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart';
//
//class Post{
//    final int id;
//    final int taskcompleted;
//
//    Post({this.id, this.taskcompleted});
//
//
//    factory Post
//}
// ignore: camel_case_types
class Graph_Screen extends StatefulWidget {

  @override
  _Graph_ScreenState createState() => _Graph_ScreenState();
}

class _Graph_ScreenState extends State<Graph_Screen> {
List <DateTime> practice = [

];

//Widget Line

  Widget Monthly(BuildContext context) {

    List<DateTime> data = [
//      DateTime date1 = DateTime(2019, 1,1);
//    final date2 = DateTime(2019, 1,2);
//
//    final date3 = DateTime(2019, 1,3);
//    final date4 = DateTime(2019, 1,4);
//
//    final date5 = DateTime(2019, 1,5);
//    final date6 = DateTime(2019, 1,6);


    ];

    final fromDate = DateTime(2019,1,1);
    final toDate = DateTime.now();

    final date1 = DateTime(2019, 1,1);
    final date2 = DateTime(2019, 1,2);

    final date3 = DateTime(2019, 1,3);
    final date4 = DateTime(2019, 1,4);

    final date5 = DateTime(2019, 1,5);
    final date6 = DateTime(2019, 1,6);

    return Center(
      child: Container(
        color: Colors.red,
        height: MediaQuery.of(context).size.height / 2,
        width: MediaQuery.of(context).size.width,
        child: BezierChart(
          bezierChartScale: BezierChartScale.WEEKLY,
          fromDate: fromDate,
          toDate: toDate,
          selectedDate: toDate,
          series: [
            BezierLine(
              label: "Duty",
              onMissingValue: (dateTime) {
                if (dateTime.month.isEven) {
                  return 10.0;
                }
                return 5.0;
              },
              data: [
                DataPoint<DateTime>(value: 3, xAxis: date1),
                DataPoint<DateTime>(value: 10, xAxis: date2),
                DataPoint<DateTime>(value: 5, xAxis: date3),
                DataPoint<DateTime>(value: 8, xAxis: date4),
                DataPoint<DateTime>(value: 4, xAxis: date5),
                DataPoint<DateTime>(value: 7, xAxis: date6),
              ],
            ),
          ],
          config: BezierChartConfig(
            displayLinesXAxis: true,
            stepsYAxis: 4,
            startYAxisFromNonZeroValue: true,
            snap: false,
            displayYAxis: true,
            displayDataPointWhenNoValue: true,
            showDataPoints: true,
            verticalIndicatorStrokeWidth: 3.0,
//            verticalIndicatorColor: Colors.black26,
            showVerticalIndicator: true,
            verticalIndicatorFixedPosition: false,
            backgroundColor: Colors.red,
            footerHeight: 30.0,
          ),
        ),
      ),
    );
  }

  Widget Yearly(BuildContext context) {
    final fromDate = DateTime(2019,1,1);
    final toDate = DateTime.now();

    final date1 = DateTime(2019, 1,1);
    final date2 = DateTime(2019, 1,2);

    final date3 = DateTime(2019, 1,3);
    final date4 = DateTime(2019, 1,4);

    final date5 = DateTime(2019, 1,5);
    final date6 = DateTime(2019, 1,6);

    return Center(
      child: Container(
        color: Colors.red,
        height: MediaQuery.of(context).size.height / 2,
        width: MediaQuery.of(context).size.width,
        child: BezierChart(
          bezierChartScale: BezierChartScale.YEARLY,
          fromDate: fromDate,
          toDate: toDate,

          selectedDate: toDate,
          series: [
            BezierLine(
              lineStrokeWidth: 5,
              label: "Duty",
              onMissingValue: (dateTime) {
                if (dateTime.year.isEven) {
                  return 20.0;
                }
                return 5.0;
              },
              data: [
                DataPoint<DateTime>(value: 1, xAxis: date1),
                DataPoint<DateTime>(value: 5, xAxis: date2),
                DataPoint<DateTime>(value: 6, xAxis: date3),
                DataPoint<DateTime>(value: 10, xAxis: date4),
                DataPoint<DateTime>(value: 4, xAxis: date5),
                DataPoint<DateTime>(value: 8, xAxis: date6),
              ],
            ),
            BezierLine(
              label: "Flight",
              lineColor: Colors.black26,
              onMissingValue: (dateTime) {
                if (dateTime.month.isEven) {
                  return 10.0;
                }
                return 3.0;
              },
              data: [
                DataPoint<DateTime>(value: 2, xAxis: date1),
                DataPoint<DateTime>(value: 3, xAxis: date2),
                DataPoint<DateTime>(value: 1, xAxis: date3),
                DataPoint<DateTime>(value: 8, xAxis: date4),
                DataPoint<DateTime>(value: 4, xAxis: date5),
                DataPoint<DateTime>(value: 4, xAxis: date6),
              ],
            ),
          ],
          config: BezierChartConfig(

            stepsYAxis: 4,
            snap: false,
//            displayYAxis: true,
            showDataPoints: true,
            verticalIndicatorStrokeWidth: 3.0,
            verticalIndicatorColor: Colors.black26,
            showVerticalIndicator: true,
            verticalIndicatorFixedPosition: false,
            backgroundGradient: LinearGradient(
              colors: [
                Colors.red[300],
                Colors.red[400],
                Colors.red[400],
                Colors.red[500],
                Colors.red,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
            footerHeight: 30.0,
          ),
        ),
      ),
    );
  }

ToggleMonthly(){
    setState(() {
setter = 0;
    });
}

ToggleYearly(){
    setState(() {
setter = 1;
    });
}


  ToggleOverall(){
    setState(() {
      setter = 2;
    });
  }

int setter = 0;
Displaygraph(){
if(setter == 0){
  return Monthly(context);
}else if(setter == 1){
  return Yearly(context);
}else if(setter ==2){
  return null;
}

}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.black54,
              Colors.black87,
              Colors.black87,
              Colors.black,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(child: new Text("Month"),
                    heroTag: UniqueKey(),
                    onPressed: (){
                    //todo
                    ToggleMonthly();
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(child: new Text("Year"),
                    heroTag: UniqueKey(),
                    onPressed: (){
ToggleYearly();
                      //todo

                    },),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(child: new Text("Overall"),
                    heroTag: UniqueKey(),
                    onPressed: (){

                      //todo

                    },),
                ),
              ],
            ),

            SizedBox(height: 30,),
            Displaygraph()
          ],
        ),
      ),

    );
  }
}