import 'dart:async';

import 'package:flutter/material.dart';
import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';
import 'package:todolist_unmodified_dup/all_database_files/DATA.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/lib/utils/date_time_to_string.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';

import 'Calendar_screen.dart';
import 'Description_Screen.dart';
import '../../all_database_files/provider/all_classes.dart';
import 'input_screen.dart';
import 'modalsheet.dart';


bool loadmoreGroupDetails = false;

class groupDetails extends StatefulWidget {
dynamic groupName;
dynamic groupID;

groupDetails(this.groupName, this.groupID);
  @override
  _groupDetailsState createState() => _groupDetailsState();
}

class _groupDetailsState extends State<groupDetails> with TickerProviderStateMixin {
  ScrollController scrollController = new ScrollController();

  AnimationController controller;
  Animation animation;
  Future _getTaskAsync;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getTaskAsync = getTaskDescription(widget.groupID);
print(widget.groupID);


    controller = new AnimationController(
      duration: new Duration(milliseconds: 225),
      vsync: this,
    );

    final CurvedAnimation curve =
    new CurvedAnimation(parent: controller, curve: Curves.easeOut);

    animation = new Tween(begin: 0.0, end: 0.1).animate(curve)
      ..addListener(() => setState(() {}));
    controller.forward(from: 0.0);

    print(widget.groupID);
print(widget.groupName);
  }

  void _showModalSheet() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return bottom();// return a StatefulWidget widget
        });
  }



  @override
  Widget build(BuildContext context) {

    listViewWidget(List<Post> data) {

//
//
//      bool _loadingMore;
//      bool _hasMoreItems;
//      int _maxItems = data.length;
//      int _numItemsPage = 10;
//      Future _initialLoad;

//      Future _loadMoreItems() async {
//        await Future.delayed(Duration(seconds: 3), () {
//          for (var i = 0; i < _numItemsPage; i++) {
////          getTask().then((value) => data.add());
//          }
//        });
//
//        _hasMoreItems = data.length < _maxItems;
//      }
//      numberOfTasksLeft = data.length;
//
//      data.sort((a, b) => b.priority.compareTo(a.priority));
//
//      bool returnBool(){
//        return true;
//      }

//
//      int returnInt(){
//        return data.length;
//      }

      Future<void> refresh() async {
        if (loadmoreGroupDetails == true) {

          return getTaskDescription(widget.groupID).then((user) {
            setState(() {
              data.replaceRange(0, data.length, user);

            });
          });
        }
        else {
          return null;
        }
      }



      refresh();




      return SingleChildScrollView(
        child: ListView.builder(
//            hasMore: returnBool,
//            loadMore: () async {
//              // can shorten to "loadMore: _loadMoreItems" but this syntax is used to demonstrate that
//              // functions with parameters can also be invoked if needed
//              await _loadMoreItems();
//            },
//            onLoadMore: () {
//              setState(() {
//                _loadingMore = true;
//              });
//            },
//            onLoadMoreFinished: () {
//              setState(() {
//                _loadingMore = false;
//              });
//            },
//            loadMoreOffsetFromBottom: 2,
            controller: scrollController,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: data.length,
            itemBuilder: (BuildContext context, int index) {


              Widget returnFAB() {
                if (data[index].priority == 1) {
                  return Transform.scale(
                    scale: 0.4,
                    child: Container(
                      child: FloatingActionButton(

                        heroTag: UniqueKey(),
                        backgroundColor: Colors.green,
                        onPressed: () {
                          setState(() {
                            /// IMPLEMENT SET PRIORITY HERE
                          });
                        },
                        tooltip: 'very Casual',
                        child: Text(""),
                        elevation: 0.0,
                      ),
                    ),
                  );
                }

                if (data[index].priority == 2) {
                  return Transform.scale(
                    scale: 0.4,
                    child: Container(
                      child: FloatingActionButton(

                        heroTag: UniqueKey(),
                        backgroundColor: Colors.lightGreenAccent,
                        onPressed: () {
                          setState(() {
                            /// IMPLEMENT SET PRIORITY HERE
                          });
                        },
                        tooltip: 'very Casual',
                        child: Text(""),
                        elevation: 0.0,
                      ),
                    ),
                  );
                }
                if (data[index].priority == 3) {
                  return Transform.scale(
                    scale: 0.4,
                    child: Container(
                      child: FloatingActionButton(

                        heroTag: UniqueKey(),
                        backgroundColor: Colors.yellow,
                        onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                        },
                        tooltip: 'very Casual',
                        child: Text(""),
                        elevation: 0.0,
                      ),
                    ),
                  );
                }
                if (data[index].priority == 4) {
                  return Transform.scale(
                    scale: 0.4,
                    child: Container(
                      child: FloatingActionButton(

                        heroTag: UniqueKey(),
                        backgroundColor: Colors.orange,
                        onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                        },
                        tooltip: 'very Casual',
                        child: Text(""),
                        elevation: 0.0,
                      ),
                    ),
                  );
                }
                if (data[index].priority == 5) {
                  return Transform.scale(
                    scale: 0.4,
                    child: Container(
                      child: FloatingActionButton(

                        heroTag: UniqueKey(),
                        backgroundColor: Colors.red,
                        onPressed: () {
//                                  setState(() {
//                                    /// IMPLEMENT SET PRIORITY HERE
//                                  });
                        },
                        tooltip: 'very Casual',
                        child: Text(""),
                        elevation: 0.0,
                      ),
                    ),
                  );
                }
              }

              Widget returnText() {

                return blackfontstyleBebas(
                    text: data[index].taskName.toString(), size: 20);


              }

//bool confirmDismiss = false;
              return InkWell(

                onTap: () async {
                  loadmoreGroupDetails = true;

                  print("PRINTING TASKID ON NAVIGATION = ${data[index].taskID}");

                  List<dynamic> result1 = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            Description_Screen(
                                data[index].taskName,
//                                      data[index].markComplete,

//                                          data[index].date,
                                data[index].taskID,
                                data[index].priority,

                                data[index].groupID,
//                                          data[index].description,

//                                           data[index].subtasks,
                                dateToStringtime(setter),
                                data[index].description
                            ),
                      )
                  );
                  print("11111111111111111111111");
//                            print(data[index].markComplete.runtimeType);
                  if(result1 == null){
                    loadmoreGroupDetails = false;
                  }else{



                    returnDate(){
                      if(result1[5] == null){
                        return data[index].date;
                      }else{
                        return result1[5];
                      }
                    }

                    Post taskPost = new Post(
//                                markComplete: data[index].markComplete,
                        taskName: result1[0],
                        groupID: result1[3],
                        description: result1[1],
                        taskID: result1[4],
                        date: returnDate().toString(),
                        priority: result1[2].toString());
//5e37ff9872828b1ed806386f
//
// PRINTING DATATOSENDBACK = [updating task with no dec and grp, , 3, 5e429ebb9953480007c0f0c9, null, 2020-02-11 18:09:52.505695]
// 11111111111111111111111
// PRINTING TAKPOST = {group_id: 5e429ebb9953480007c0f0c9, todo_card_id: null, date: 2020-02-11 18:10:12.267528, title: true, priority: 3}

                    //[new task, , 3, null, 2020-02-13 00:00:00.000]
//                            [new task1235, , 4, 5e4382efdc0d3f0007287091, null, 2020-02-12 10:23:12.870155]
                    if(result1[1] == null && result1[4] == null){

                      print("PRINTING TAKPOST WITHOUT BOTH = ${taskPost.toMapUpdateWithoutGroupAndDescription()}");

                      Post p = await updatePost(
                          baseURL + updateTodoUrl,
                          body: taskPost.toMapUpdateWithoutGroupAndDescription());

                      refresh();

                      Timer _timer;
                      _timer = new Timer(const Duration(seconds: 1), () {
                        loadmoreGroupDetails = false;
                      });

                    }
                    else if(result1[1] == null && result1[3] != null){


                      print("PRINTING TAKPOST WITHOUT DESC = ${taskPost.toMapUpdateWithoutDescription()}");

                      Post p = await updatePost(
                          baseURL + updateTodoUrl,
                          body: taskPost.toMapUpdateWithoutDescription());
                      refresh();

                      Timer _timer;
                      _timer = new Timer(const Duration(seconds: 1), () {
                        loadmoreGroupDetails = false;
                      });
                    }else if(result1[1] != null && result1[3] == null){


                      print("PRINTING TAKPOST WITHOUT GRUP = ${taskPost.toMapUpdateWithoutGroup()}");

                      Post p = await updatePost(
                          baseURL + updateTodoUrl,
                          body: taskPost.toMapUpdateWithoutGroup());

                     refresh();

                      Timer _timer;
                      _timer = new Timer(const Duration(seconds: 1), () {
                        loadmoreGroupDetails = false;
                      });
                    }else       if(result1[1] != null && result1[3] != null) {


                      print("PRINTING TAKPOST WITH BOTH = ${taskPost
                          .toMapUpdateCreate()}");

                      Post p = await updatePost(
                          baseURL + updateTodoUrl,
                          body: taskPost.toMapUpdateCreate());

                      refresh();


                      Timer _timer;
                      _timer = new Timer(const Duration(seconds: 1), () {
                        loadmoreGroupDetails = false;
                      });

                    }

                  }
                },

                child: Dismissible(
//

                  key: UniqueKey(),
                  onDismissed: (direction) async {

                    if (direction.index == 2) {

                      deletePost(
                        baseURL + deleteTodoUrl, data[index].taskID.toString(),);


                      setState(() {

                        data.removeAt(index);
                      });


                    } else {
                      deletePost(
                        baseURL + deleteTodoUrl, data[index].taskID.toString(),);


                      setState(() {

                        data.removeAt(index);
                      });

                    }
//                  setState(() {
//                    marker =! marker;
//
//                    if(marker == false){
//                      uncompleteTask(data[index], false, index);
//                    }else if(marker == true){
//                      completeTask(data[index], true, index);
//                    }
//                  });

                  },
                  secondaryBackground: Container(
                    transform: new Matrix4.identity()
                      ..scale(animation.value, 1.0),
                    child: Icon(Icons.delete, color: Colors.black,),
                    color: Colors.transparent,
                    alignment: Alignment.centerLeft,
                  ),
                  background: Container(
                    child: Icon(Icons.done),
                    color: Colors.transparent,
                    alignment: Alignment.centerLeft,
                  ),
                  child: Container(
                    width: double.infinity,
                    child: ListTile(
                      leading: returnFAB(),
                      title: returnText(),

                    ),
                  ),
                ),
              );
            }

        ),
      );
    }


    return Scaffold(
      floatingActionButton: Stack(
        children: <Widget>[
          Positioned(

              right: 0,
              top: 40,
              child: Transform.scale(
                scale: 1.2,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    key: UniqueKey(),
                    icon: Image.asset('assets/GroupGrid.png', width: 70, height: 70,),
                    tooltip: 'go to group screen',
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              )

          ),
          Positioned(
            bottom: -14,
            child: InkWell(
              onTap: ()async{
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyHomePage(),
                    ));
              },
              child: Container(
                color: Colors.red,
                width: MediaQuery.of(context).size.width *0.4,
                height: 50,
//           color: Colors.black,
//           child:,
              ),
            ),
          ),

//
          Positioned(
//              right: 0,
            left: 50,
            bottom: 0,
            top: 700,
//              top: 20,
            child: IconButton(
              icon: Icon(Icons.calendar_today, color: Colors.white,size: 30,),
              onPressed: () {
                Navigator.push( // string which stores the user entered value
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return MyHomePage();
                      }, //screen which has TextField
                    ));
              },
            ),

          ),
          Positioned(
            bottom: -14,
            right: -8,
            child: InkWell(
              onTap: (){
                _showModalSheet();
              },
              child: Container(
                color: Colors.red,
                width: MediaQuery.of(context).size.width *0.35,
                height: 50,
//           color: Colors.black,
//           child:,
              ),
            ),
          ),
          Positioned(
            right: 135.5,
            top: 675,
            child: FloatingActionButton(
              heroTag: UniqueKey(),

              backgroundColor: Colors.red,
              onPressed: () async{
                List<dynamic> result1 = await Navigator.push( // string which stores the user entered value
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return InputScreen(setter);
                      }, //screen which has TextField
                    ));
                print(result1);

                returnDate(){
                  if(result1[4] == null){
                    return setter;
                  }else{
                    return result1[4];
                  }
                }
// aaaa = 5e37ff9872828b1ed806386f
                returnGroupId(){
                  if(result1[3] == null){
                    return widget.groupID;
                  }else{
                    return result1[3];
                  }
                }
print("PRINTING GROUP ID = ${returnGroupId()}");
                loadmoreGroupDetails = true;

                Post taskPost = new Post(
//                    markComplete: false.toString(),
                    taskName: result1[0],
                    groupID: returnGroupId(),
                    description: result1[1],
                    date: returnDate().toString(),
                    priority: result1[2].toString());
//                Post p = await createPost(baseURL + createTodo, body: taskPost.toMap());

                if(result1[1] == null && result1[3] == null){
                  Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciptionAndGroup());
                  print("Printing P = $p");
//                  loadmoreGroupDetails = true;

//                  numberOfTasksLeft = numberOfTasksLeft - 1;
                  Timer _timer;
                  _timer = new Timer(const Duration(seconds: 2), () {
                    loadmoreGroupDetails = false;
                  });


                }else if(result1[1] != null && result1[3] == null){
//                  loadmoreGroupDetails = true;

//                  numberOfTasksLeft = numberOfTasksLeft - 1;
                  Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutGroup());
                  print("Printing P = ${p.taskName}");
                  Timer _timer;
                  _timer = new Timer(const Duration(seconds: 2), () {
                    loadmoreGroupDetails = false;
                  });

                }else if(result1[1]== null && result1[3] != null){
//                  loadmoreGroupDetails = true;
//                  numberOfTasksLeft = numberOfTasksLeft - 1;
                  Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreateWithoutDesciption());
                  Timer _timer;
                  _timer = new Timer(const Duration(seconds: 2), () {
                    loadmoreGroupDetails = false;
                  });


                }else if(result1[1] != null && result1[3] != null){

//                  numberOfTasksLeft = numberOfTasksLeft - 1;
                  Post p = await createPost(baseURL + createTodoUrl, body: taskPost.toMapCreate());

                  Timer _timer;
                  _timer = new Timer(const Duration(seconds: 2), () {
                    loadmoreGroupDetails = false;
                  });

                }





              },
              tooltip: 'Add a task',
              child: Icon(Icons.add),
              elevation: 0.0,
            ),
          ),
          Positioned(

            right: 30,
            bottom: -9,

            child: IconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.white,size: 30,
              ),
              onPressed: () {
                _showModalSheet();
//                  fetchData();
              },
            ),
          ),
        ],
      ),

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 22.0, left: 15),
                    child: Container(
//                      color: Colors.black,
                        width: 270,
                        child: RedfontstyleBebas(text: widget.groupName, size: 47,)),
                  ),
//              SizedBox(width: 50,)
                ],
              ),

              FutureBuilder(
                future: _getTaskAsync,
                builder: (context, snapshot){
                  if(snapshot.hasData){

                    return listViewWidget(snapshot.data);

                  }else{
                    return  Text("");
                  }
                },
              ),
            ],
          ),
          
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: Image.asset("assets/navigation.png",
                    color: Colors.red,
                  )
              )

            ],
          ),
        ],
      ),
    );
  }
}
