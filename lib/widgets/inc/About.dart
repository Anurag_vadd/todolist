import 'package:flutter/material.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';

class About_screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    backgroundColor: Color(whitecolor),
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 6),
                    child: RedfontstyleBebas(text: "ABOUT", size: 40,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Transform.scale(
                      scale: 0.9,
                      child: FloatingActionButton(
                        elevation: 0.0,
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                          size: 30,
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              SizedBox(height: 100,),

              Center(child: RedfontstyleBebas(text: "POWER", size: 50,)),
SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(child: blackfontstyleMont(text: "We believe Productivity is an assortment all those"
                    " low to high priority things you work on day. This app not only helps in"
                    " keeping track of them but its productivity feature helps you to stay motivated."
                    " Share feature of it also helps you to show case your p"
                    "rogress to your favorite social walls", size: 14,
                  )
                ),
              ),


              SizedBox(height: 220,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: FlatButton(
                        onPressed: () async{

                        },
                        child: RedfontstyleMont(text: "Privacy Policy", size: 18,)
                    ),
                  ),
//SizedBox(h),
                  FlatButton(
                      onPressed: () {
//                            _showDialog();
                      },
                      child: RedfontstyleMont(text: "Terms & Conditions", size: 18,)
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
