import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/widgets/inc/loginscreen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/sign_up_Screen.dart';

import 'login.dart';



class Register extends SecureStoreMixin{
final String mail;
final String phone;
final String userName;
final String password;

  Register({this.mail, this.phone, this.userName, this.password, });

Register.fromJson(Map jsonMap)
    : mail = jsonMap['mail'],
      phone = jsonMap['phone'],
      userName = jsonMap['username'],
      password = jsonMap['password'];


Map toMap(){
  var mapGroup = new Map<String, dynamic>();
  mapGroup["mail"] = mail;
  mapGroup['phone'] = phone;
  mapGroup['username'] = userName;
  mapGroup['password'] = password;

  return mapGroup;

  }

factory Register.fromJsonForgotPassword(Map<String, dynamic> json) => Register(
mail: json["mail"]
//  error: json["error"],
//  message: json["message"],
//  data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
);

Map toMapForgotPassword(){
  var mapGroup = new Map<String, dynamic>();
  mapGroup["mail"] = mail;
//  mapGroup['phone'] = phone;
//  mapGroup['username'] = userName;
//  mapGroup['password'] = password;

  return mapGroup;

}

}


Future<Register> createRegister(String url, {Map body}) async {
//  print(body);
  final _storage = FlutterSecureStorage();

  return http.post(url, body: body).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR SIGN UP= $statusCode");
    print("PRINTING REGISTRATION BODY = ${response.body}");
    Map toVerifyOtp = jsonDecode(response.body) as Map;
    print("PRINTING TOVERIFYOTP = $toVerifyOtp");
    var toSend = toVerifyOtp["data"]["_id"];
    toSendId = toSend;
    print("PRINTING TOSEND = $toSend");
    print("PRINTING TOSEND = $toSendId");
////    verifyOtp(baseURL + verifyOtpRegistration, body: {"id" : toSend, "otp" : otp});
//    if(statusCode == 200){
//
////    navigateToSignUp = true;
//
//
//    }

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data = $statusCode");
    }
    return Register.fromJson(json.decode(response.body));
  });
}


googleSignUpAPI(String url, {Map body}){
  return http.post(url, body: body).then((http.Response response) async {
//    print(response.body);
    final int statusCode = response.statusCode;

    var data = jsonDecode(response.body);
    if(statusCode == 200){
navigateTo = true;
      navigateToSignUp = true;
String token = data["data"]["token"];
print("PRINTING TOKEN = $token");

String id = data["data"]["userdata"]["_id"];
print("PRINTING TOKEN = $id");

String username = data["data"]["userdata"]["username"];
print("PRINTING TOKEN = $username");

await secureStore.write(key: "userToken", value: token);
await secureStore.write(key: "userId", value: id);
await secureStore.write(key: "userUsername", value: username);


    }

    print("THis is Status code FOR SIGN UP= $statusCode");
    print("PRINTING google sign in  BODY = ${response.body}");

    Map toVerifyOtp = jsonDecode(response.body) as Map;


//    print("PRINTING TOVERIFYOTP = $toVerifyOtp");
//    var toSend = toVerifyOtp["data"]["_id"];
//    print("PRINTING TOSEND = $toSend");
//    verifyOtp(baseURL + verifyOtpRegistration, body: {"id" : toSend, });



    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data = $statusCode");
    }
    return Register.fromJson(json.decode(response.body));
  });
}


registerFaceBook(String url, {Map body}){
  return http.post(url, body: body).then((http.Response response) async {
//    print(response.body);
    final int statusCode = response.statusCode;
    var data = json.decode(response.body);


    if(statusCode == 200) {
      navigateTo = true;
      navigateToSignUp = true;

String token = data["data"]["token"];
print("PRINTING TOKEN = $token");

      String id = data["data"]["userdata"]["_id"];
      print("PRINTING TOKEN = $id");

      String username = data["data"]["userdata"]["username"];
      print("PRINTING TOKEN = $username");

      await secureStore.write(key: "userToken", value: token);
      await secureStore.write(key: "userId", value: id);
      await secureStore.write(key: "userUsername", value: username);


    }
//{"error":"0",
// "message":"Signup is Successful",
// "data":
//      {
//      "userdata":
//            {
//                "_id":"5e37fa0625ba9678443d02ba",
//                "status":true,
//                "username":"Anurag Vadavathy",
//                "login_type":"fb"
//             },
//          "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJRCI6IjVlMzdmYTA2MjViYTk2Nzg0NDNkMDJiYSJ9LCJpYXQiOjE1ODE0MDk2NTIsImV4cCI6MTYxMjk0NTY1Mn0.eze93oICaaS4HTqTBQaTFuld8FCEWKP5t8pPp0JN1sY"
//        }
//   }
    print("THis is Status code FOR FACEBOOK SIGN UP= $statusCode");
    print("PRINTING facebook sign in  BODY = ${response.body}");

    Map toVerifyOtp = jsonDecode(response.body) as Map;



    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data = $statusCode");
    }
    return Register.fromJson(json.decode(response.body));
  });
}


class Otp{
  dynamic id;
  dynamic otp;
dynamic password;
Otp({this.id, this.otp, this.password});

  Otp.fromJsonVerifyOtpRegistration(Map jsonMap)
      : id = jsonMap['id'],
  otp = jsonMap["otp"];

  Map toMapVerifyOtpRegistration(){
    var mapGroup = new Map<String, dynamic>();
    mapGroup["id"] = id;
  mapGroup["otp"] = otp;

    return mapGroup;

  }

  Otp.fromJsonResetPassword(Map jsonMap)
      : id = jsonMap['id'],
        otp = jsonMap["otp"],
  password = jsonMap["password"];


  Map toMapResetPassword(){
    var mapGroup = new Map<String, dynamic>();
    mapGroup["id"] = id;
    mapGroup["otp"] = otp;
mapGroup["password"] = password;
    return mapGroup;

  }


  Otp.fromJsonResendOtpRegistration(Map jsonMap)
      : id = jsonMap['id'];

      Map  toMapResendOtpRegistration() {
    var mapGroup = new Map<String, dynamic>();
    mapGroup["id"] = id;

    return mapGroup;

  }

}



Future<Otp> verifyOtpRegistration(String url, {Map body}) async {

  return http.post(url, body: body).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR Verify otp= $statusCode");
    print("PRINTING REGISTRATION BODY = ${response.body}");

    if(response.statusCode == 200){
      navigateToSignUp = true;
    }

    return Otp.fromJsonVerifyOtpRegistration(json.decode(response.body));
  });
}

Future<Otp> resendOtpRegistration(String url, {Map body}){
  return http.post(url, body: body).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR resend OTP= $statusCode");
    print("PRINTING REGISTRATION BODY = ${response.body}");

    return Otp.fromJsonResendOtpRegistration(json.decode(response.body));
  });
}

Future<Otp> resendOtpForgotPassword(String url, {Map body}){
  return http.post(url, body: body).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR resend OTP= $statusCode");
    print("PRINTING REGISTRATION BODY = ${response.body}");

    return Otp.fromJsonResendOtpRegistration(json.decode(response.body));
  });
}

var toSendId;


Future<Otp> resetpassword(String url, {Map body}){
  return http.post(url, body: body).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR resend OTP= $statusCode");
    print("PRINTING REGISTRATION BODY = ${response.body}");

    return Otp.fromJsonResetPassword(json.decode(response.body));
  });
}

Future<Register> forgotPassword(String url, {Map body}){
  return http.post(url, body: body).then((http.Response response) {
//    print(response.body);
    final int statusCode = response.statusCode;
    print("THis is Status code FOR forgot password = $statusCode");
    print("PRINTING REGISTRATION BODY = ${response.body}");

    return Register.fromJsonForgotPassword(json.decode(response.body));
  });
}