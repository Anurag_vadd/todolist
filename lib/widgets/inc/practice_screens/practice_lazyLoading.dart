import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:http/http.dart' as http;
import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';

class BackendService {
  static Future<List<PostModel>> getPosts(offset, limit) async {
    final responseBody = (await http.get(
        'https://jsonplaceholder.typicode.com/posts')).body;

    // The response body is an array of items
    return PostModel.fromJsonList(json.decode(responseBody));
  }

}






class practiceFetch{
  String name;
  String date;

  practiceFetch.fromJson(obj){
    this.name = obj["name"];
    this.date = obj["date"];
  }
//
//  static List<practiceFetch> fromJsonList(jsonList) {
//    return jsonList.map<PostModel>((obj) => PostModel.fromJson(obj)).toList();
//  }
}

class PostModel {
  String title;
  String body;
  List<practiceFetch> list;

  PostModel.fromJson(obj) {
    this.title = obj['title'];
    this.body = obj['body'];
//    this.list = List<practiceFetch>.from(json["data"].map((x) => practiceFetch.fromJson(x)));
  }

  static List<PostModel> fromJsonList(jsonList) {
    return jsonList.map<PostModel>((obj) => PostModel.fromJson(obj)).toList();
  }
}


class MainFetchData extends StatefulWidget {
  @override
  _MainFetchDataState createState() => _MainFetchDataState();
}
class _MainFetchDataState extends State<MainFetchData> {
//  List list = List();
  var isLoading = false;


  String username = 'test';
  String password = '123£';
  Widget _itemBuilder(context, PostModel entry, _) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(
            Icons.person,
            color: Colors.brown[200],
          ),
//          title: Text(entry),
          subtitle: Text(entry.body),
        ),
        Divider()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Text(""),
        onPressed: (){

          String basicAuth = 'Basic ' + base64Encode(utf8.encode("$username:$password"));

          print(basicAuth);
        },
      ),
      body: PagewiseListView(

          pageSize: 10,
          itemBuilder: _itemBuilder,
          pageFuture: (pageIndex) =>
              BackendService.getPosts(pageIndex * 10, 10)),
    );
  }

}






