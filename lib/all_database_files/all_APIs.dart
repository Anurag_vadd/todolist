String baseURL = "https://lafqwzv955.execute-api.ap-south-1.amazonaws.com/dev/v1.0";

/// GROUPS API GROUPS APIGROUPS APIGROUPS APIGROUPS APIGROUPS APIGROUPS APIGROUPS APIGROUPS APIGROUPS APIGROUPS API

String fetchGroupsUrl = "/groups/get_all_groups";

String updateGroupsUrl = "/groups/update_group";

String deleteGroupUrl = "/groups/delete_group/";

String createGroupUrl = "/groups/create_group";

String groupTodoUrl(String groupId){
  return "/todos/get_todos_list?limit=10&page=0&group_id=$groupId";
}

/// TODO ITEM API TODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM APITODO ITEM API
String getTodoInGroupScreenURL(String id){
  return "/todos/get_todos_list?limit=15&page=0&user_id=$id";
}



String createTodoUrl = "/todos/create_todos";

String getTodoDetailsUrl = "/todos/find_one_todo_details";

String updateTodoUrl = "/todos/update_todo_card";

String deleteTodoUrl = "/todos/remove_todo_card";

String fetchTodoByDateUrl = "/todos/get_todos_by_date";

String fetchTodoInCalendarUrl = "/todos/get_todos_by_month?skip=0&limit=25";

/// registration registration registration registration registration registration registration registration registration registration registration registration registration

String registerURL = "/admin/register";

String verifyOtpRegistrationUrl = "/admin/verify_otp";

String resendOtpUrl = "/admin/resend_otp_reg";

String resetPasswordUrl = "/admin/reset_password";

String forgotPasswordUrl = "/admin/forgot_password";

String resentOtpForgotPasswordUrl = "/admin/resend_otp";

/// Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login Login

String loginURL = "/admin/login";

String googleSignInURL = "/admin/google_singn_up";

String facebookSignInUrl = "/admin/fb_singn_up";

String getUserURL(String userid){
  return "/admin/get_user/$userid";
}

/// SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS SUBAASKS
///

String getSubtaskUrl = "";

String addSubtaskURLUrl = "/todos/add_task";

String deleteSubtaskUrl = "/todos/remove_task";

String updateSubtaskUrl  = "/todos/update_task";

