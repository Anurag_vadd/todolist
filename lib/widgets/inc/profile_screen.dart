
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/widgets/inc/password_reset.dart';



class profile_Screen extends StatefulWidget {
  @override
  _profile_ScreenState createState() => _profile_ScreenState();
}

class _profile_ScreenState extends State<profile_Screen> {

  TextEditingController namecontroller = new TextEditingController();
TextEditingController emailcontroller = new TextEditingController();
  TextEditingController phonecontroller = new TextEditingController();


  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
//          title: new Text("Alert Dialog title"),
          content: new Text("Are you sure you want to Deactivate your account?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("yes"),
              onPressed: () {
//                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("no"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  returnName(){
  return  secureStore.read(key: "userUsername");
  }

  returnPhone(){
    return  secureStore.read(key: "userPhone");
  }
  returnMail(){
    return  secureStore.read(key: "usermail");
  }

  _asyncMethod() async {
  namecontroller.text = await secureStore.read(key: "userUsername");
  phonecontroller.text = await secureStore.read(key: "userPhone");
  emailcontroller.text = await secureStore.read(key: "userMail");
  }

  @override
  void initState(){
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_){
      _asyncMethod();
    });

  }

  listViewWidget(List<UserDetails> data){

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0, left: 1),
                        child: RedfontstyleBebas(text: "PROFILE",size: 40,),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 3.0),
                        child: Transform.scale(
                          scale: 0.9,
                          child: FloatingActionButton(
                            heroTag: UniqueKey(),
                            elevation: 0.0,
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            backgroundColor: Colors.red,
                            foregroundColor: Colors.white,
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),

          Container(
//            width: MediaQuery.of(context).size.width,
                height: 150,
                color: Colors.red,
                child: Center(
                  child: Transform.scale(
                    scale: 1.8,
                    child: FloatingActionButton(
                      onPressed: (){

                      },
                      elevation: 0.0,
                        foregroundColor: Colors.black,
                        backgroundColor: Colors.white,
                        child: Icon(Icons.person, color: Colors.black,)),
                  ),
                ),
          ),

                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        onSaved: (value) {
                          return value;
                        },

//                        onSubmitted: (value) {
//                          String textToSendBack = taskcontroller.text;
//                          Navigator.pop(context, value);
//                          },
//            maxLength: 100,
                        controller: namecontroller,
                        enabled: false,
                        decoration: InputDecoration.collapsed(

                            hintText: "Name"
                        ),
                        style: TextStyle(
                            height: 1.2, fontSize: 16, color: Colors.black87),
                      ),

                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (value) {
                          return value;
                        },
//                        onSubmitted: (value) {
//                          String textToSendBack = taskcontroller.text;
//                          Navigator.pop(context, value);
//                          },
//            maxLength: 100,
                        controller: emailcontroller,
                        enabled: false,
                        decoration: InputDecoration.collapsed(

                            hintText: "Email ID"
                        ),
                        style: TextStyle(
                            height: 1.2, fontSize: 16, color: Colors.black87),
                      ),

                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (value) {
                          return value;
                        },
//                        onSubmitted: (value) {
//                          String textToSendBack = taskcontroller.text;
//                          Navigator.pop(context, value);
//                          },
//            maxLength: 100,
                        controller: phonecontroller,
                        enabled: false,
                        decoration: InputDecoration.collapsed(

                            hintText : "Phone Number"
                        ),
                        style: TextStyle(
                            height: 1.2, fontSize: 16, color: Colors.black87),
                      ),
                    ],
                  ),
                ),
              ],
            ),

          Padding(
            padding: const EdgeInsets.only(
                left: 2,
//                right: 8
            bottom: 18
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[

                Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: FlatButton(
                        onPressed: () async{
                          await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return password_Screen();
                                }, //screen which has TextField
                              ));
                        },
                        child: RedfontstyleMont(text: "Change Password", size: 18,)
                      ),
                    ),
//SizedBox(h),
                    FlatButton(
                      onPressed: () {
                        _showDialog();
                      },
                        child: RedfontstyleMont(text: "Deactivate Account", size: 18,)
                    ),
                  ],
                )
              ],
            ),
          ),
//          SingleChildScrollView(
//            child: Row(
//              crossAxisAlignment: CrossAxisAlignment.start,
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//
//                Expanded(
//
//                  child: Image.asset(
//                    "assets/navigation.png", color: Colors.red,
//                    fit: BoxFit.fill,
//                  ),
//                ),
//              ],
//            ),
//          )
//          SizedBox(height: 1,)
        ],
      ),
    );
  }
}
