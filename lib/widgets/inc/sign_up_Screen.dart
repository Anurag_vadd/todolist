import 'dart:convert';


import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:todolist_unmodified_dup/all_database_files/Login_register/register.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/lib/days_page_view/day_page_view_example.dart';

import 'loginscreen.dart';

bool navigateToSignUp = false;

class SignUpPage extends StatefulWidget with SecureStoreMixin{
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignUpPage> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController mailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
TextEditingController otpController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  var profileData;
  var facebookLogin = FacebookLogin();
  bool isLoggedIn = false;

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }

  bool _isLoggedIn = false;
  Map userProfile;
  @override
  Widget build(BuildContext context) {




    _displayDialog(BuildContext context) async {
      return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Please enter Otp'),
              content: TextField(
                controller: otpController,
                decoration: InputDecoration(hintText: "Otp"),
              ),
              actions: <Widget>[
                FlatButton(
                  child: new Text('Resend Otp'),
                  onPressed: () async {
                    Otp otp = new Otp(id: toSendId);
                    Otp p = await resendOtpRegistration(baseURL + registerURL, body: otp.toMapResendOtpRegistration());
                  },
                ),
                new FlatButton(
                  child: new Text('Send Otp'),
                  onPressed: () async{
                    if(otpController.text.isNotEmpty) {
                      Otp otp = new Otp(id: toSendId, otp: otpController.text);
                      var p = await verifyOtpRegistration(baseURL + verifyOtpRegistrationUrl, body: otp.toMapVerifyOtpRegistration());
                    }
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }



    void initiateFacebookLogin() async {
      var facebookLogin = FacebookLogin();
      var facebookLoginResult =
      await facebookLogin.logIn([ 'public_profile']);
      print("PRINTING FACEBOOK BODY =  ${facebookLogin.logIn([ 'public_profile']).then((value) => {print(value)})} ");

      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.error:
          print(facebookLoginResult.status);
          print(facebookLoginResult.errorMessage);
          onLoginStatusChanged(false);
          break;
        case FacebookLoginStatus.cancelledByUser:
          print("CancelledByUser");
          onLoginStatusChanged(false);
          break;
        case FacebookLoginStatus.loggedIn:
          final token = facebookLoginResult.accessToken.token;
          final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}');
          final profile = jsonDecode(graphResponse.body);
          print(profile);
          setState(() {
            userProfile = profile;
            _isLoggedIn = true;
          });
          //{
          // name: Anurag Vadavathy,
          // picture: {data: {
          // height: 50,
          // is_silhouette: false,
          // url: https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1315942121947382&height=50&width=50&ext=1583318448&hash=AeQ2LeIVC5Mn-3Cl,
          // width: 50
          // }},
          // email: anurag.vadd@gmail.com,
          // id: 1315942121947382
          // }

          registerFaceBook(baseURL + facebookSignInUrl, body: {"fb_id" : profile["id"], "username" : profile["name"]});
          onLoginStatusChanged(true);
//        var firebaseUser = await (
//            token: facebookLoginResult.accessToken);
          break;
      }
    }


    _showSnackBar() {
      print("Show Snackbar here !");
      final snackBar = new SnackBar(
        content: new Text("All fields are mandatory"),
        duration: new Duration(seconds: 3),
        backgroundColor: Colors.grey,
        action: new SnackBarAction(label: 'Ok', onPressed: (){
          print('press Ok on SnackBar');
        }),
      );
      //How to display Snackbar ?
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    _showSnackBarEmail() {
      print("Show Snackbar here !");
      final snackBar = new SnackBar(
        content: new Text("Incorrect email"),
        duration: new Duration(seconds: 3),
        backgroundColor: Colors.grey,
        action: new SnackBarAction(label: 'Ok', onPressed: (){
          print('press Ok on SnackBar');
        }),
      );
      //How to display Snackbar ?
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(whitecolor),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Center(
            child: Column(

              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 100,),
                      TextFormField(
                        onSaved: (value) {
                          return value;
                        },


                        controller: nameController,
                        enabled: true,
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.person, color: Color(redcolor),),
                            labelText: "Name"
                        ),
                        style: TextStyle(
                            height: 1.2, fontSize: 16, color: Colors.black87),
                      ),

                      TextFormField(

                        keyboardType: TextInputType.emailAddress,
                        onSaved: (value) {
                          return value;
                        },
validator: (val) => !EmailValidator.validate(val, true)
    ? 'Not a valid email.'
    : null,
                        controller: mailController,
                        enabled: true,
                        decoration: InputDecoration(
                        prefixIcon: Icon(Icons.mail, color: Color(redcolor),),
                            labelText: "Email"
                        ),
                        style: TextStyle(
                            height: 1.2, fontSize: 16, color: Colors.black87
                        ),
                      ),


                      TextFormField(
                        onSaved: (value) {
                          return value;
                        },


                        controller: passwordController,
                        enabled: true,
                        obscureText: true,
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.lock, color: Color(redcolor),),
                            labelText: "Password"

                        ),
                        style: TextStyle(
                            height: 1.2,
                            fontSize: 16,
                            color: Colors.black87
                        ),
                      ),

                      TextFormField(
                        onSaved: (value) {
                          return value;
                        },

                        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                        keyboardType: TextInputType.number,

                        controller: phoneController,
                        enabled: true,
//                        obscureText: true,

                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.phone_android, color: Color(redcolor),),
                            labelText: "Phone Number"

                        ),
                        style: TextStyle(
                            height: 1.2,
                            fontSize: 16,
                            color: Colors.black87
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(height: 20,),

                Center(
                    child: Transform.scale(
                      scale: 1.2,
                      child: FlatButton(

                        onPressed: ()async{
                          if(EmailValidator.validate(mailController.text) == false){
                            _showSnackBarEmail();
                          }

                          if(nameController.text.isEmpty || mailController.text.isEmpty || passwordController.text.isEmpty || phoneController.text.isEmpty) {
_showSnackBar();
//{
// "error":"0",
// "message":"Please verify your OTP",
// "data":
//      {
//      "indian":false,
//      "banck_account":false,
//      "_id":"5e37d7423a4c2e2e250f07fa",
//      "mail":"anurag.vadd@gmail.com",
//      "phone":"123456789",
//      "username":"whudjd",
//      "login_type":"manual",
//      "signupDate":"2020-02-03T08:18:10.248Z"
//      ,"updatedDate":"2020-02-03T08:18:10.248Z",
//      "__v":0
//      }
// }
                          }else {
                            Register register = new Register(

                            mail: mailController.text,
                              password: passwordController.text,
                              userName: nameController.text,
                              phone: phoneController.text
                            );


//if(otpController.text.isNotEmpty) {
  Register p = await createRegister(
      baseURL + registerURL, body: register.toMap());
////}
                            _displayDialog(context);

                            if(navigateToSignUp == true){


//                            Future.delayed(Duration(seconds: 5));

                              await Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginPage()
                                  ));
                            }else{
                              return null;
                            }
                          }
                        },
                        color: Colors.red,

                        child: Text("Sign Up", style: TextStyle(color: Colors.white, ), )
                      ),
                    )
                ),

//                SizedBox(height: 20,),
//
//                Text("Or", style: TextStyle(color: Colors.black, fontSize: 15),),
//
//                SizedBox(height: 20,),
//
//                Row(
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    Container(
//                      decoration: BoxDecoration(
//                          shape: BoxShape.circle,
//                          border: Border.all(color: Colors.red)
//                      ),
//                      child: FloatingActionButton(
//                        heroTag: UniqueKey(),
//
//                        onPressed: (){
//                          initiateFacebookLogin();
//                        },
//                        foregroundColor: Colors.white,
//                        backgroundColor: Colors.white,
//                        elevation: 0,
//                        child: Icon(
//                          const IconData(0xf09a, fontFamily: "Facebook"),
//                          color: Color(redcolor),
//                        ),
//                      ),
//                    ),
//                    SizedBox(width: 15,),
//
//
//                    Container(
//                      decoration: BoxDecoration(
//                          shape: BoxShape.circle,
//                          border: Border.all(color: Colors.red)
//                      ),
//                      child: FloatingActionButton(
//                          heroTag: UniqueKey(),
//
//                          onPressed: (){
//                            signInWithGoogle();
//                          },
//                        foregroundColor: Colors.white,
//                        backgroundColor: Colors.white,
//                        elevation: 0,
//                        child: Text("G", style: TextStyle(color: Color(redcolor), fontSize: 18),)
//                      ),
//                    ),
//                  ],
//                ),

                SizedBox(height: 20,),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Text("Already have an account?", style: TextStyle(color: Colors.black, fontSize: 15),),
                    SizedBox(width: 5,),
                    InkWell(
                      onTap: () async {
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginPage(),
                            ));
                      },
                      child: Text("Login", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, decoration: TextDecoration.underline),),)
                  ],
                ),
                SizedBox(height: 40,)
              ],


            ),
          ),
        ),
      ),
    );
  }


}

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<String> signInWithGoogle() async {
  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
  await googleSignInAccount.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  //FirebaseUser
  // ({uid: HVqo7Ea9niOU2jYj8OViQbDBpjj1,
  // photoUrl: https://lh3.googleusercontent.com/-Peyw6emDs0M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdnPsxxyhIrcMrsRPjjDI3ISbdDgA/s96-c/photo.jpg,
  // isAnonymous: false,
  // providerData:
  //      [
  //      {
  //      uid: HVqo7Ea9niOU2jYj8OViQbDBpjj1,
  //      photoUrl: https://lh3.googleusercontent.com/-Peyw6emDs0M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdnPsxxyhIrcMrsRPjjDI3ISbdDgA/s96-c/photo.jpg,
  //      providerId: firebase,
  //      displayName: Anurag Vad,
  //      email: anurag.vadd@gmail.com
  //    },
  //    {
  //    uid: 103653182121632479507,
  //    photoUrl: https://lh3.googleusercontent.com/-Peyw6emDs0M/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdnPsxxyhIrcMrsRPjjDI3ISbdDgA/s96-c/photo.jpg,
  //    providerId: google.com,
  //    displayName: Anurag Vad,
  //    email: anurag.vadd@gmail.com
  //    }],
  //    providerId: firebase,
  //    displayName: Anurag Vad,
  //    creationTimestamp: 1577354203922,
  //    lastSignInTimestamp: 1580724053772,
  //    email: anurag.vadd@gmail.com,
  // isEmailVerified: true
  // })

  final AuthResult authResult = await _auth.signInWithCredential(credential);
  print("PRINTING FIREBASE SIGN IN = ${authResult.user}");
  final FirebaseUser user = authResult.user;
var userAPI = user.email;
print("PRINTING SERAPI = $userAPI");
var username = user.displayName;

  print("PRINTING SERAPI = $username");
googleSignUpAPI(baseURL + googleSignInURL, body: {
  "mail" : user.email, "username": user.displayName
});

  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);

  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);

  return 'signInWithGoogle succeeded: $user';
}

void signOutGoogle() async{
  await googleSignIn.signOut();

  print("User Sign Out");
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(color: Colors.blue[100]),
    );
  }
}