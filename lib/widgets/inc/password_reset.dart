import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/register.dart';

bool hideText = true;

class password_Screen extends StatefulWidget {
  @override
  _password_ScreenState createState() => _password_ScreenState();
}

class _password_ScreenState extends State<password_Screen> {
  TextEditingController controller = new TextEditingController();
  TextEditingController secondcontroller = new TextEditingController();
  TextEditingController thirdcontroller = new TextEditingController();
  TextEditingController otpController = new TextEditingController();




  Widget returnText(){
    if(
//    controller.text.isEmpty ||
        secondcontroller.text.isEmpty || thirdcontroller.text.isEmpty){
      return blackfontstyleBebasDone(text: "Done", size: 20,);
    }else{
      return blackfontstyleBebas(text: "Done",size: 20,);
    }
  }

  ignoreFunction(){
    print("inside ignorefunction");
    print(controller.text);
    if(
//    controller.text.isEmpty ||
        secondcontroller.text.isEmpty || thirdcontroller.text.isEmpty){
      return true;
    }else{
      return false;
    }
  }
  bool _obscureText = true;
  bool _obscureText1 = true;
  bool _obscureText2 = true;

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Please enter Otp'),
            content: TextField(
              controller: otpController,
              decoration: InputDecoration(hintText: "Otp"),
            ),
            actions: <Widget>[
              FlatButton(
                child: new Text('Resend Otp'),
                onPressed: () async {
                  String id = await secureStore.read(key: "userId");

                  Otp otp = new Otp(id: id);
                  Otp p = await resendOtpForgotPassword(baseURL + resentOtpForgotPasswordUrl , body: otp.toMapResendOtpRegistration());
                },
              ),
              new FlatButton(
                child: new Text('Send Otp'),
                onPressed: () async{
                  if(otpController.text.isNotEmpty) {
                    String id = await secureStore.read(key: "userId");
                    Otp otp = new Otp(id: id, password: secondcontroller.text, otp: otpController.text);
                    Otp p = await resetpassword(baseURL + resetPasswordUrl, body: otp.toMapResetPassword());
                  }
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }


  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  returnEyeIcon(){
    if (_obscureText == true){
      return Icon(Icons.remove_red_eye, color: Colors.black,);
    }else if(_obscureText == false){
      return Icon(Icons.remove_red_eye, color: Colors.grey.shade500,);
    }
  }

  returnEyeIcon1(){
    if (_obscureText1 == true){
      return Icon(Icons.remove_red_eye, color: Colors.black,);
    }else if(_obscureText1 == false){
      return Icon(Icons.remove_red_eye, color: Colors.grey.shade500,);
    }
  }

  returnEyeIcon2(){
    if (_obscureText2 == true){
      return Icon(Icons.remove_red_eye, color: Colors.black,);
    }else if(_obscureText2 == false){
      return Icon(Icons.remove_red_eye, color: Colors.grey.shade500,);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Stack(
        children: <Widget>[
          Positioned(
            right: 0,
            bottom: 0,
//                  top: 1,
            child: Container(
                child: FlatButton(
                  onPressed: (){
                    setState(() {
                      Navigator.pop(context, null);

                    });
                  },
                  child: blackfontstyleBebasDone(text: "Cancel",size: 20,),
                )
            ),
          ),
          Positioned(
            right: 80,
            bottom: 0,
//                  top: 1,
            child: Container(
                child: FlatButton(
                    onPressed: () async {
//                              print()
                    var id = secureStore.read(key: "userId");
                    var password = controller.text;
                    var mail = await secureStore.read(key: "userMail");
                    print("PRINTING MAIL FORGOT PASSWORD = $id");
                    Register register = new Register(mail: mail.toString());
                    Register p = await forgotPassword(baseURL + forgotPasswordUrl , body: register.toMapForgotPassword());

//                    String textToSendBack = controller.text;
////                        if(textToSendBack.isNotEmpty) {
////                          Navigator.pop(context,textToSendBack);
                      _displayDialog(context);
////                        }
//                        else{
//                          controller.dispose();
//                          secondcontroller.dispose();
//                          thirdcontroller.dispose();
//                          Navigator.pop(context);
//                        }
                    },
                    child: returnText()
                )
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RedfontstyleBebas(text: "PROFILE",size: 40,),
                Padding(
                  padding: const EdgeInsets.only(right: 3.0),
                  child: Transform.scale(
                    scale: 0.9,
                    child: FloatingActionButton(
                      elevation: 0.0,
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      backgroundColor: Colors.red,
                      foregroundColor: Colors.white,
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 15,),
            RedfontstyleBebas(text: "Change Password", size: 20,),

SizedBox(height: 20,),
//            TextFormField(
//
//              onSaved: (value){
//                return value;
//              },
//
//              controller: controller,
//              enabled: true,
//              obscureText: _obscureText,
//              decoration: InputDecoration(
//                suffixIcon: IconButton(
//                  icon: returnEyeIcon(),
//                  onPressed: (){
//                    setState(() {
//                      _toggle();
//                    });
//                  },
//                ),
//                  labelText: "enter old password"
//              ),
//              style: TextStyle(height: 1.2, fontSize: 20, color: Colors.black87),
//            ),


            TextFormField(
              obscureText: _obscureText1,
              onSaved: (value){
                return value;
              },

              controller: secondcontroller,
              enabled: true,
              decoration: InputDecoration(
                  suffixIcon: IconButton(
                    icon: returnEyeIcon1(),
                    onPressed: (){
                      setState(() {
                       _toggle1();
                      });
                    },
                  ),
                  labelText: "enter new password"
              ),
              style: TextStyle(height: 1.2, fontSize: 20, color: Colors.black87),
            ),
            TextFormField(
              onSaved: (value){
                return value;
              },

              controller: thirdcontroller,
              enabled: true,
              obscureText: _obscureText2,
              decoration: InputDecoration(

                  suffixIcon: IconButton(
                    icon: returnEyeIcon2(),
                    onPressed: (){
                      setState(() {
                    _toggle2();
                      });
                    },
                  ),
                  labelText: "confirm new password"
              ),
              style: TextStyle(height: 1.2, fontSize: 20, color: Colors.black87),
            ),
          ],
        ),
      ),
    );
  }
}
