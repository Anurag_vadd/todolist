import 'dart:core';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:todolist_unmodified_dup/all_database_files/post_class.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import '../../widgets/inc/starting_screen.dart';
import 'date_time_to_string.dart';

/// A simple widget that shows a checkbox and some date-time information.
///
/// This widget uses [AutomaticKeepAliveClientMixin].
class Page extends StatefulWidget {

  Page({
    @required this.days,
    @required this.initialdayoverride
  }) : showDayOfMonth = true;
//
//  Page.forMonth({
//    @required DateTime month, this.initialdayoverride,
//  })  : showDayOfMonth = false,
//        days = <DateTime>[];

  final bool showDayOfMonth;
  final List<DateTime> days;
final DateTime initialdayoverride;
  

  @override
  _PageState createState() => new _PageState();
}

class _PageState extends State<Page> with AutomaticKeepAliveClientMixin<Page> {



  @override
  bool get wantKeepAlive => false;



  TextEditingController controller = new TextEditingController();
  TextEditingController cardController = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    super.build(context);
    return ChangeNotifierProvider<todoState>(
      create: (_) => todoState(),
      child: Scaffold(
//      floatingActionButton:    new RaisedButton(
//        child: new Text("Jump To Today"),
//        onPressed: () {
//          _selectDate();
//        },
//      ),
          body: new Column(
              children: widget.days.map(((day) => Container(

//            constraints: new BoxConstraints(
//                maxHeight: MediaQuery.of(context).size.height,
//                maxWidth: MediaQuery.of(context).size.width
//            ),
                child:  Expanded(child: Starting_screen(day)
                ),
              )
              ),
              ).toList()
          )
      ),
    );
  }
}

