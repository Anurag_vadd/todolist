import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
const redcolor = 0xFFF0002F;
const whitecolor = 0xFFFFFFFF;
const blackcolor = 0xFF000000;

//class whitefontstylemont extends StatelessWidget{
//  Color color = Color(whitecolor);
//  String text = "";
//  double size;
////  String font = "Mont";
//  whitefontstylemont({@required this.text, this.size,});
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: color, decoration: TextDecoration.lineThrough),);
//  }
//}

//class whitefontstylemontdone extends StatelessWidget{
//  Color color = Color(whitecolor);
//  String text = "";
//  double size;
////  String font = "Mont";
//  whitefontstylemontdone({@required this.text, this.size,});
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: Colors.green, decoration: TextDecoration.lineThrough),);
//  }
//}
//
//
//
//


launchURLFacebook() async {
  const url = 'https://facebook.com';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

launchURLTwitter() async {
  const url = 'https://twitter.com';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}



class whitefontstyleBebas extends StatelessWidget{
  Color color = Color(whitecolor);
  String text = "";
  double size;
  String font = "bebas";

  whitefontstyleBebas({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: color, fontFamily: font),);
  }
}

class blackfontstyleMont extends StatelessWidget{
  Color color = Color(blackcolor);
  String text = "";
  double size;
  String font = "Mont";
  blackfontstyleMont({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: color, fontFamily: font),);
  }
}

class blackfontstyledoneMont extends StatelessWidget{
  Color color = Color(blackcolor);
  String text = "";
  double size;
  String font = "Mont";
  blackfontstyledoneMont({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: Colors.grey.shade400,fontFamily: font, decoration: TextDecoration.lineThrough), );
  }
}

class blackfontstyledoneMontBold extends StatelessWidget{
  Color color = Color(blackcolor);
  String text = "";
  double size;
  String font = "MontBold";
  blackfontstyledoneMontBold({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: Colors.grey.shade400,fontFamily: font, decoration: TextDecoration.lineThrough), );
  }
}


class blackfontstyleBebas extends StatelessWidget{
  Color color = Color(blackcolor);
  String text = "";
  double size;
  String font = "Bebas";

  blackfontstyleBebas({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: color,fontFamily: font),);
  }
}

class blackfontstyleBebasDone extends StatelessWidget{
  Color color = Colors.grey.shade500;
  String text = "";
  double size;
  String font = "Bebas";

  blackfontstyleBebasDone({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: color,fontFamily: font),);
  }
}


class blackfontstyleBebasBold extends StatelessWidget{
  Color color = Color(blackcolor);
  String text = "";
  double size;
  String font = "BebasBold";
  blackfontstyleBebasBold({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text", style: TextStyle(height: 1.2, fontSize: size, color: color,fontFamily: font),);
  }
}

class RedfontstyleBebas extends StatelessWidget{
  Color color = Color(redcolor);
  String text = "";
  double size;
  String font = "Bebas";
  RedfontstyleBebas({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text",textAlign: TextAlign.start, style: TextStyle(height: 1.2, fontSize: size, color: color, fontFamily: font),);
  }

}

class RedfontstyleMont extends StatelessWidget{
  Color color = Color(redcolor);
  String text = "";
  double size;
  String font = "Mont";
  RedfontstyleMont({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text",textAlign: TextAlign.start, style: TextStyle(height: 1.2, fontSize: size, color: color, fontFamily: font),);
  }

}


class RedfontstyleMontBold extends StatelessWidget{
  Color color = Color(redcolor);
  String text = "";
  double size;
  String font = "MontBold";
  RedfontstyleMontBold({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text",textAlign: TextAlign.start, style: TextStyle(height: 1.2, fontSize: size, color: color, fontFamily: font),);
  }

}

class RedfontstyleBebasbold extends StatelessWidget{
  Color color = Color(redcolor);
  String text = "";
  double size;
  String font = "BebasBold";
  RedfontstyleBebasbold({@required this.text, this.size,});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text("$text",textAlign: TextAlign.start, style: TextStyle(height: 1.2, fontSize: size, color: color, fontFamily: font),);
  }

}

class ThemeChooser extends StatelessWidget {
  static int color;
  ThemeData data = ThemeData(scaffoldBackgroundColor: Color(color));
  ThemeChooser({color});
  @override
  Widget build(BuildContext context) {
    return ThemeChooser(color: color,);
  }
}





