import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:todolist_unmodified_dup/all_database_files/DATA.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/all_classes.dart';
import 'package:todolist_unmodified_dup/all_database_files/subtasks.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';



class Description_ScreenFromGroups extends StatefulWidget {

//  final TodoItem description;
//  dynamic done;
  dynamic taskName;
  dynamic taskId;
  int priority;
  dynamic groupId;
  final String dateValue;
  dynamic description;
  Description_ScreenFromGroups(
      this.taskName , //this is the class where i want to access the index
      this.taskId,
      this.priority,
      this.groupId,
      this.dateValue,
      this.description
      );
  @override
  _Description_ScreenFromGroupsState createState() => _Description_ScreenFromGroupsState();
}

class _Description_ScreenFromGroupsState extends State<Description_ScreenFromGroups> {


  bool LoadMore = false;


//  DateTime _selectedDate;
  TextEditingController taskcontroller = new TextEditingController();
  TextEditingController newcontroller = new TextEditingController();
  TextEditingController descriptioncontroller = new TextEditingController();
  TextEditingController subtaskcontroller = new TextEditingController();
  bool _validate = false;

  Future _getTaskAsync;

  ignoreFunction(){
    print(taskcontroller.text);
    if(newcontroller.text.isEmpty || floatingButtonValue == null){
      return true;
    }else{
      return false;
    }
  }



  Widget returnText(){
    if(newcontroller.text.isEmpty || floatingButtonValue == null){
      return blackfontstyleBebasDone(text: "DONE", size: 20,);
    }else{
      return blackfontstyleBebas(text: "DONE",size: 20,);
    }
  }



  DateTime _fromDate = DateTime.now();
  TimeOfDay _fromTime = const TimeOfDay(hour: 7, minute: 28);
  DateTime _toDate = DateTime.now();
  TimeOfDay _toTime = const TimeOfDay(hour: 7, minute: 28);


//  Future _selectDayAndTime(BuildContext context) async{
//
//    DateTime _selectedDay = await showDatePicker(
//        context: context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2018),
//        lastDate: DateTime(2030),
//        builder: (BuildContext context, Widget child) => child
//    );
//
//
//
//    if(_selectedDay != null) {
//      //a little check
//      print("$_selectedDay");
//      //todo : ADD REMINDER HERE
//    }
//  }

  List<Widget> subtaskList = [];
  List<dynamic> subtaskListAPI = [];

  List<dataArray> _list = [];
  dataArray _selectedMenuItem;

  String selected = "";
  dynamic group_id;


  Future<List<dataArray>> loadJsonFromAsset() async {
    String link =
        baseURL + fetchGroupsUrl;
    var res = await http
        .get(Uri.encodeFull(link), headers: {"Accept": "application/json"});
    final decoded = json.decode(res.body);
    try {
      return (decoded != null)
          ? decoded["data"]
          .map<dataArray>((item) => dataArray.fromJson(item))
          .toList()
          : [];
    } catch (e) {
      debugPrint(e.toString());
      return [];
    }
  }

  Future initJson() async {
    _list = await loadJsonFromAsset();

    setState(() {
      for(int i =0; i<=_list.length - 1; i++) {
        _selectedMenuItem = _list[i];
      }

    });

  }

  DropdownMenuItem<dataArray> buildDropdownMenuItem(dataArray item) {
    return DropdownMenuItem(

      value: item, // you must provide a value
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Text(item.groupName),
      ),
    );
  }



  Widget buildDropdownButton() {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: DropdownButton<dataArray>(
          elevation: 1,
          hint: Text("Select one"),
          isExpanded: true,
          underline: Container(
            height: 2,
            color: Colors.black12,
          ),
          items: _list.map((item) => buildDropdownMenuItem(item)).toList(),
          value: _selectedMenuItem, // values should match
          onChanged: (dataArray item) {
            setState(() {

              return _selectedMenuItem = item;
            });
            print("Printing itemid = ${item.id}");
            group_id = item.id;
            return item.id;
          },
        ),
      ),
    );
  }

  addSubtaskList(Subtask subtasks){
//    setState(() {
    subtaskListAPI.add(subtasks);

//    });
  }

  addSubtask(String item){
    subtaskList.add(
        ListTile(
          leading: Transform.scale(
            scale: 0.6,
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,

                  border: Border.all(color: Colors.red)
              ),
              child: FloatingActionButton(
                  heroTag: UniqueKey(),

                  foregroundColor: Colors.white,
                  backgroundColor: Colors.white,
                  elevation: 0,
                  child: Text("")
              ),
            ),
          ),
          title: blackfontstyleBebas(text: subtaskcontroller.text,),
          trailing: IconButton(
            onPressed: (){
              setState(() {

                subtaskList.remove(subtaskList);
                subtaskListAPI.remove(subtaskListAPI);

                print("PRINTING SUBTASKLISTAPI = $subtaskListAPI");
              });

              deleteSubtasks(widget.taskId, widget.taskId);
            },
            icon: Icon(Icons.cancel),
            color: Colors.black,
          ),
        ));
  }



  void setfloatingbuttonvalue1(){
    setState(() {
      floatingButtonValue = 1;
    });
  }

  void setfloatingbuttonvalue2(){
    setState(() {
      floatingButtonValue = 2;
    });
  }
  void setfloatingbuttonvalue3(){
    setState(() {
      floatingButtonValue = 3;
    });
  }
  void setfloatingbuttonvalue4(){
    setState(() {
      floatingButtonValue = 4;
    });
  }
  void setfloatingbuttonvalue5(){
    setState(() {
      floatingButtonValue = 5;
    });
  }

  setIcon1(int value){
    if(value == 1){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon2(int value){
    if(value == 2){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon3(int value){
    if(value == 3){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon4(int value){
    if(value == 4){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setIcon5(int value){
    if(value == 5){
      return Icon(Icons.done, color: Colors.white,);
    }else{
      return Text("");
    }
  }

  setBox1(int value){
    if(value == 1){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox2(int value){
    if(value == 2){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox3(int value){
    if(value == 3){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox4(int value){
    if(value == 4){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  setBox5(int value){
    if(value == 5){
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.blueAccent)
      );
    }else{
      return BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white)
      );
    }
  }

  var dateValueToSendBack;

  Future _selectDayAndTime(BuildContext context) async{


    DateTime _selectedDay = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2018),
        lastDate: DateTime(2030),
        builder: (BuildContext context, Widget child) => child
    );

//    TimeOfDay _selectedTime = await showTimePicker(
//      context: context,
//      initialTime: TimeOfDay.now(),
//
//    );


    if(_selectedDay != null) {
      //a little check
      print("PRINTING DATE TIME = $_selectedDay");
//      print("PRINTING  TIME = $_selectedTime");
//_selectedDay.add(Duration(hours: _selectedTime.hour, minutes: _selectedTime.minute));

      var dateValue = new DateTime(_selectedDay.year, _selectedDay.month, _selectedDay.day, );
      print(dateValue);
      dateValueToSendBack = dateValue;

//      notificationDuration = DateTime.now().difference(dateValue).inMinutes;
      return dateValue;
//      print(_selectedDay);
//      return _selectedDay;
      //todo : ADD REMINDER HERE
    }
  }

  int floatingButtonValue;
  @override
  void initState() {
    // TODO: implement initState
    floatingButtonValue = widget.priority;
    print("PRINTING TASKID AT INITSTATE = ${widget.taskId}");
    descriptioncontroller = new TextEditingController(text: widget.description);
    newcontroller = new TextEditingController(text: widget.taskName);
    initJson();
    _getTaskAsync = getEventSubtasks(baseURL + getTodoDetailsUrl, widget.taskId);
//    getEventSubtasks(baseURL + getTodoDetailsUrl, widget.taskId).then((value) => {print("PRINTING VALUE = $value")});
//    fetchTodoDetails(widget.taskId).then((value) => {print("PRINTING VALUE $value")});
  }
  listViewWidget(Map<dynamic, dynamic> data) {


//
//    if(data["data"]["tasks"] != null) {
//      subtaskListAPI = data["data"]["tasks"];
//    }else{
//      subtaskListAPI = new List<dynamic>();
//    }

//    Future<void> refresh() async {
//      if (loadMore == true) {
//
//        return getEventSubtasks(baseURL + getTodoDetailsUrl, widget.taskId).then((user) {
//          setState(() {
//            data = user;
//
//          });
//        });
//      }
//      else {
//        return null;
//      }
//    }
//
//
//
//
//
//    refresh();

    TextEditingController value = new TextEditingController();

    return Container(
      color: Colors.white,
      alignment: Alignment.topLeft,
      margin: const EdgeInsets.all(13),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

//                        SizedBox(height: 10,),

            Container(
                height: MediaQuery.of(context).size.height * 0.15,
                width: MediaQuery.of(context).size.width,
                child: buildDropdownButton()
            ),
            SizedBox(height: 10,),
            //Todo: remove sized box below and display tasks there
            TextFormField(
              keyboardType: TextInputType.text,
//                          initialValue: widget.title,
              validator: (String value) {
                return value.isEmpty ? "task must have a name" : null;
              },
              textAlign: TextAlign.start,
              maxLength: 100,
              controller: newcontroller, // Just an ordinary TextController
              onChanged: (value) {
                setState(() {
                  value = newcontroller.text;
                });
              },

              decoration: InputDecoration(
                  errorText:
                  _validate // Just a boolean value set to false by default
                      ? 'Value Can\'t Be Empty'
                      : null,
                  labelText: "name of task"
              ),
              style: TextStyle(height: 1.2, fontSize: 20, color: Colors.black87),
            ),

            TextField(

              controller: descriptioncontroller,
              onEditingComplete: (){
                /// IMPLEMENT ADD DETAIL API CALL HERE
              },
              decoration: InputDecoration(
                  icon: Icon(Icons.short_text),
                  labelText: "Add details"

              ),

            ),

            SizedBox(height: 20,),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 30,
              child: InkWell(
                onTap: (){
                  _selectDayAndTime(context);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.calendar_today,
                      color: Colors.grey.shade500,
                    ),
                    SizedBox(width: 19,),
                    Text("Add Date/Time", style: TextStyle(color: Colors.grey.shade500, fontSize: 16),)
                  ],
                ),
              ),
            ),

//                      SizedBox(height: 20,),
            SizedBox(height: 10,),

//
            SizedBox(height: 20,),

//            ListView.builder(
//
//                scrollDirection: Axis.vertical,
//                shrinkWrap: true,
//                itemCount: data["data"]["tasks"].length,
//                itemBuilder: (BuildContext context, int index) {
//
//
//                  int dataLength = data["data"]["tasks"].length;
//
//                  // ignore: missing_return
//                  Widget returnTextListTile() {
//                    int i = 0;
//                    while(i < dataLength){
//                      Future.delayed(Duration(seconds: 2));
////                      refresh();
//                      return blackfontstyleBebasDone(
//                        text: subtaskListAPI[index]["name"], size: 20,);
//                    }
//                    i++;
//
//                  }
//
////bool confirmDismiss = false;
//                  return Container(
//
//                    width: MediaQuery.of(context).size.width,
//                    height: 40,
//                    child: ListTile(
//                      leading: Container(
//
//                        width: 20,
//                        height: 20,
//                        child: Transform.scale(
//                          scale: 0.6,
//                          child: Container(
//
//                            width: 20,
//                            height: 20,
//                            decoration: BoxDecoration(
//                                shape: BoxShape.circle,
//
//                                border: Border.all(color: Colors.red)
//                            ),
//                          ),
//                        ),
//                      ),
//
//                      title: returnTextListTile(),
//                      trailing: Container(
////color: Colors.black,
//                        width: 40,
//                        height: 40,
//                        child: IconButton(
//                          onPressed: (){
//
//
//                            deleteSubtasks(widget.taskId, subtaskListAPI[index]["_id"]);
//                            setState(() {
//                              subtaskListAPI.removeAt(index);
//
////                                print("PRINTING SUBTASKLISTAPI = $subtaskListAPI");
//                            });
//                          },
//                          icon: Icon(Icons.cancel),
//                          color: Colors.black,
//                        ),
//                      ),
//
//                    ),
//                  );
//                }
//
//            ),
//            TextField(
//
//              controller: subtaskcontroller,
//              onTap: (){
//                /// IMPLEMENT add subtasks here
//                print("textfield tapped");
//              },
//              onEditingComplete: () async {
////                loadMore = true;
////                  setState(() {
////
////
////                  });
//
////                refresh();
//
//                if(subtaskcontroller.text.isNotEmpty){
//                  Subtask post = new Subtask(
//                      id: widget.taskId,
//                      name: subtaskcontroller.text,
//                      status: false.toString()
//                  );
//
//                  addSubtask(subtaskcontroller.text);
//
//
//
//                  Subtask p = await addSubtasksRequest(widget.taskId, subtaskcontroller.text);
//
//                  addSubtaskList(post);
////                  refresh();
//
//                  Timer _timer;
//                  _timer = new Timer(const Duration(microseconds: 1), () {
//                    loadMore = false;
//                  });
//                }else{
////                  refresh();
//
//                  Timer _timer;
//                  _timer = new Timer(const Duration(microseconds: 1), () {
//                    loadMore = false;
//                  });
//
//                }
//
//              },
//              decoration: InputDecoration(
//                  enabled: true,
//                  icon: Icon(Icons.subdirectory_arrow_right),
//                  labelText: "Add Subtasks"
//              ),
//            ),
            blackfontstyleBebas(text: "Priority", size: 20,),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Transform.scale(
                  scale: 0.8,
                  child: Container(
                    decoration: setBox1(floatingButtonValue),
                    child: FloatingActionButton(

                      heroTag: UniqueKey(),
                      backgroundColor: Colors.green,
                      onPressed: (){
                        setState(() {
                          print("PRINTING FAB VALUE = $floatingButtonValue");

                          setfloatingbuttonvalue1();
                          /// IMPLEMENT SET PRIORITY HERE
                        });
                      },
                      tooltip: 'very Casual',
                      child: setIcon1(floatingButtonValue),
                      elevation: 0.0,
                    ),
                  ),
                ),
                Transform.scale(
                  scale: 0.8,
                  child: Container(
                    decoration: setBox2(floatingButtonValue),
                    child: FloatingActionButton(
                      heroTag: UniqueKey(),
                      backgroundColor: Colors.lightGreenAccent,
                      onPressed: (){


                        setState(() {
                          print("PRINTING FAB VALUE = $floatingButtonValue");

                          setfloatingbuttonvalue2();
                        });
                      },
                      tooltip: 'Casual',
                      child: setIcon2(floatingButtonValue),
                      elevation: 0.0,
                    ),
                  ),
                ),
                Transform.scale(
                  scale: 0.8,
                  child: Container(
                    decoration: setBox3(floatingButtonValue),
                    child: FloatingActionButton(
                      heroTag: UniqueKey(),
                      backgroundColor: Colors.yellow,
                      onPressed: (){

                        setState(() {
                          print("PRINTING FAB VALUE = $floatingButtonValue");

                          setfloatingbuttonvalue3();
                        });
                      },
                      tooltip: 'Moderate',
                      child: setIcon3(floatingButtonValue),
                      elevation: 0.0,
                    ),
                  ),
                ),
                Transform.scale(
                  scale: 0.8,
                  child: Container(
                    decoration: setBox4(floatingButtonValue),
                    child: FloatingActionButton(
                      heroTag: UniqueKey(),
                      backgroundColor: Colors.orange,
                      onPressed: (){

                        setState(() {
                          print("PRINTING FAB VALUE = $floatingButtonValue");

                          setfloatingbuttonvalue4();
                        });
                      },
                      tooltip: 'Urgent',
                      child: setIcon4(floatingButtonValue),
                      elevation: 0.0,
                    ),
                  ),
                ),
                Transform.scale(
                  scale: 0.8,
                  child: Container(
                    decoration: setBox5(floatingButtonValue),
                    child: FloatingActionButton(
                      heroTag: UniqueKey(),
                      backgroundColor: Colors.red,
                      onPressed: (){

                        setState(() {
                          print("PRINTING FAB VALUE = $floatingButtonValue");

                          setfloatingbuttonvalue5();
                        });
                      },
                      tooltip: 'Very Urgent',
                      child: setIcon5(floatingButtonValue),
                      elevation: 0.0,
                    ),
                  ),
                ),

              ],
            ),

            SizedBox(height: 20,),

          ]
      ),
    );
  }

  @override
  Widget build(BuildContext context) {




    return Material(
      child: Scaffold(

        floatingActionButton: Container(
          width: MediaQuery.of(context).size.width,
          height: 50,
          color: Colors.white,
          child: Stack(

            children: <Widget>[

              Positioned(
                right: 0,
                bottom: 0,
//                  top: 1,
                child: Container(
                    child: FlatButton(
                      onPressed: (){
                        setState(() {
                          loadMore = false;
                          Navigator.pop(context, null);

                        });
                      },
                      child: blackfontstyleBebas(text: "CANCEL",size: 20,),
                    )
                ),
              ),
              Positioned(
                right: 70,
                bottom: 0,
//                  top: 1,
                child: Container(
                    child: IgnorePointer(
                      ignoring: ignoreFunction(),
                      child: FlatButton(
                          onPressed: () async {
//                              print()
                            dynamic textToSendBack = newcontroller.text;
                            dynamic descriptiontoSentBack = descriptioncontroller.text;
                            dynamic prioritytoSendBack = floatingButtonValue;
                            dynamic groupID = group_id;
                            dynamic taskId = widget.taskId;
                            dynamic dateToSendBack = dateValueToSendBack;
                            print("PRINTING SELECTED DATE SENT BACK = $dateValueToSendBack");
                            print("PRINTING groupID SENT BACK COPLETE = $groupID");
                            print("PRINTING group_ID SENT BACK COMPLETE= $group_id");
                            print("PRINTING taskId SENT BACK = ${widget.taskId}");

                            List<dynamic> datatosendbackUpdate;
                            //[new task, , 4, null, null, null]
//                             [new task123, , 2, null, 5e4382efdc0d3f0007287091, null]
                            datatosendbackUpdate = [textToSendBack, descriptiontoSentBack, prioritytoSendBack, group_id, taskId, dateToSendBack];
                            print("PRINTING DATATOSENDBACK FROM DESCRIPTION SCREEN GROUPS= $datatosendbackUpdate");
                            if(textToSendBack.isNotEmpty) {
                              Navigator.pop(context, datatosendbackUpdate);

                            }else{
                              taskcontroller.dispose();
                              Navigator.pop(context);
                            }
                          },
                          child: returnText()
                      ),
                    )
                ),
              ),
            ],
          ),
        ),
        resizeToAvoidBottomPadding: true,
        backgroundColor: Colors.white,
        body: SingleChildScrollView(

          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10,),
              RedfontstyleBebasbold(text: widget.dateValue ,size: 50,),


                SizedBox(height: 20,),

              FutureBuilder(
                  future: _getTaskAsync,
                  builder: (context, snapshot){
                    if(snapshot.hasData) {
                      return listViewWidget(snapshot.data);
                    }else{
                      return CircularProgressIndicator();
                    }
                  }),
              SizedBox(height: 20,),

//              blackfontstyleBebas(text: "Priority", size: 20,),

              SizedBox(height: 50,)
            ],
          ),
        ),
      ),
    );
  }
}
