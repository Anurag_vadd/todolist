
import 'dart:convert';
//import 'dart:html';
//import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:quiver/collection.dart';
import 'package:todolist_unmodified_dup/all_database_files/Login_register/login.dart';
import 'package:todolist_unmodified_dup/all_database_files/all_APIs.dart';
import 'package:todolist_unmodified_dup/all_database_files/provider/appState.dart';
import 'package:todolist_unmodified_dup/widgets/inc/input_screen.dart';
import 'package:todolist_unmodified_dup/widgets/inc/starting_screen.dart';

bool isLoading = false;

class Post {
//  String markComplete;
  dynamic groupID;
  dynamic date;
  dynamic taskName;
  dynamic taskID;
//List<dynamic> subtasks;

  dynamic priority;
  dynamic description;
  dynamic userId;
//  VoidCallback statusCodeValue;


  Post({
//    this.markComplete,
    this.groupID,
    this.taskID,
    this.date,
    this.taskName,
//    this.subtasks,

    this.priority,
    this.description,
    this.userId
  });

  /// ==========================================================================================================

  factory Post.fromJsonCreate(Map<String, dynamic> json) {
    return Post(

//        markComplete : json['status'],
        groupID : json['group_id'],
      taskID: json['_id'],
      date : json['date'],
      taskName : json['title'],
//      subtasks : List<dynamic>.from(json["tasks"].map((x) => x)),

      priority : json['priority'],
      description : json['description'],
//      userId: json['user_id']
    );
  }

  factory Post.fromJsonUpdate(Map<String, dynamic> json) {
    return Post(

//        markComplete : json['status'],
        groupID : json['group_id'],
        taskID: json['todo_card_id'],
        date : json['date'],
        taskName : json['title'],
//      subtasks : List<dynamic>.from(json["tasks"].map((x) => x)),

        priority : json['priority'],
        description : json['description'],
//      userId : json['user_id']
    );
  }

  factory Post.fromJsonCreateWithoutDecscription(Map<String, dynamic> json) {
    return Post(
//        markComplete : json['status'],
        groupID : json['group_id'],
        taskID: json['_id'],
        date : json['date'],
        taskName : json['title'],
//      subtasks : List.from(json["tasks"].map((x) => x)),

        priority : json['priority'],
//        description : json['description']
//    userId : json['user_id']
    );
  }

  factory Post.fromJsonCreateWithoutGroup(Map<String, dynamic> json) {
    return Post(
//      markComplete : json['status'],
//      groupID : json['group_id'],
      taskID: json['_id'],
      date : json['date'],
      taskName : json['title'],
//      subtasks : List.from(json["tasks"].map((x) => x)),

      priority : json['priority'],
        description : json['description'],
//      userId: json['user_id']
    );
  }

  factory Post.fromJsonCreateWithoutGroupAndDescription(Map<String, dynamic> json) {
    return Post(
//        markComplete : json['status'],
//      groupID : json['group_id'],
        taskID: json['_id'],
        date : json['date'],
        taskName : json['title'],
//      subtasks : List.from(json["tasks"].map((x) => x)),

        priority : json['priority'],
//        description : json['description']

//        userId: json['user_id']
    );
  }


  factory Post.fromJsonCalendar(Map<String, dynamic> json) {
    return Post(

//        markComplete : json['status'],
        groupID : json['group_id'],
        taskID: json['_id'],
        date : json['date'],
        taskName : json['title'],
//      subtasks : List<dynamic>.from(json["tasks"].map((x) => x)),

        priority : json['priority'],
        description : json['description'],
//        userId: json['user_id']
    );
  }

  factory Post.fromJsonSubtasks(Map<String, dynamic> json) {
    return Post(

//        markComplete : json['status'],
        groupID : json['group_id'],
        taskID: json['todo_card_id'],
        date : json['date'],
        taskName : json['title'],
//        subtasks : List<dynamic>.from(json["tasks"].map((x) => x)),

        priority : json['priority'],
        description : json['description'],

//        userId: json['user_id']
    );
  }

/// =================================================================================================

  Map toMapCreate() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
    map['group_id'] = groupID;
//    map["_id"] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List.from(subtasks.map((x) => x));

    map['priority'] = priority;
    map['description'] = description;
map['user_id'] = userId;

    return map;
  }

  Map toMapCreateWithoutGroup() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
//    map['group_id'] = groupID;
//    map["_id"] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List.from(subtasks.map((x) => x));

    map['priority'] = priority;
    map['description'] = description;

    map['user_id'] = userId;

    return map;
  }


  Map toMapCreateWithoutDesciption() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
    map['group_id'] = groupID;
//    map["_id"] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List.from(subtasks.map((x) => x));

    map['priority'] = priority;
//    map['description'] = description;

    map['user_id'] = userId;

    return map;
  }

  Map toMapCreateWithoutDesciptionAndGroup() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
//    map['group_id'] = groupID;
//    map["_id"] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List.from(subtasks.map((x) => x));

    map['priority'] = priority;
//    map['description'] = description;

    map['user_id'] = userId;

    return map;
  }

//  Map toMapCalendar() {
//    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
//    map['group_id'] = groupID;
//    map['_id'] = taskID;
//    map['date'] = date;
//    map['title'] = taskName;
////    map['tasks'] = List.from(subtasks.map((x) => x));
//
//    map['priority'] = priority;
//    map['description'] = description;
//
//
//    return map;
//  }

  /// ============================================================================================================

  Map toMapUpdateCreate() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
    map['group_id'] = groupID;
    map['todo_card_id'] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));

    map['priority'] = priority;
    map['description'] = description;

    map['user_id'] = userId;

    return map;
  }

  Map toMapUpdateWithoutDescription() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
    map['group_id'] = groupID;
    map['todo_card_id'] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));

    map['priority'] = priority;
//    map['description'] = description;


    map['user_id'] = userId;
    return map;
  }

  Map toMapUpdateWithoutGroup() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
//    map['group_id'] = groupID;
    map['todo_card_id'] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));

    map['priority'] = priority;
    map['description'] = description;


    map['user_id'] = userId;
    return map;
  }

  Map toMapUpdateWithoutGroupAndDescription() {
    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete;
//    map['group_id'] = groupID;
    map['todo_card_id'] = taskID;
    map['date'] = date;
    map['title'] = taskName;
//    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));

    map['priority'] = priority;
//    map['description'] = description;

    map['user_id'] = userId;

    return map;
  }


  /// ===============================================================================================================================

//  Map toMapUpdateCreate() {
//    var map = new Map<dynamic, dynamic>();
////    map['status'] = markComplete;
//    map['group_id'] = groupID;
//    map['todo_card_id'] = taskID;
//    map['date'] = date;
//    map['title'] = taskName;
////    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));
//
//    map['priority'] = priority;
//    map['description'] = description;
//
//
//    return map;
//  }
//
//  Map toMapUpdateWithoutDescription() {
//    var map = new Map<dynamic, dynamic>();
////    map['status'] = markComplete;
//    map['group_id'] = groupID;
//    map['todo_card_id'] = taskID;
//    map['date'] = date;
//    map['title'] = taskName;
////    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));
//
//    map['priority'] = priority;
////    map['description'] = description;
//
//
//    return map;
//  }
//
//  Map toMapUpdateWithoutGroup() {
//    var map = new Map<dynamic, dynamic>();
////    map['status'] = markComplete;
////    map['group_id'] = groupID;
//    map['todo_card_id'] = taskID;
//    map['date'] = date;
//    map['title'] = taskName;
////    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));
//
//    map['priority'] = priority;
//    map['description'] = description;
//
//
//    return map;
//  }
//
//  Map toMapUpdateWithoutGroupAndDescription() {
//    var map = new Map<dynamic, dynamic>();
////    map['status'] = markComplete;
////    map['group_id'] = groupID;
//    map['todo_card_id'] = taskID;
//    map['date'] = date;
//    map['title'] = taskName;
////    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));
//
//    map['priority'] = priority;
////    map['description'] = description;
//
//
//    return map;
//  }


//  Map toMapUpdateMarkComplete() {
//    var map = new Map<dynamic, dynamic>();
//    map['status'] = markComplete.toString();
//    map['group_id'] = groupID.toString();
//    map['todo_card_id'] = taskID.toString();
//    map['date'] = date.toString();
//    map['title'] = taskName.toString();
////    map['tasks'] = List<dynamic>.from(subtasks.map((x) => x));
//
//    map['priority'] = priority;
//    map['description'] = description.toString();
//
//
//    return map;
//  }
}

Future<List<Post>> getTask(int i) async {
  String id = await secureStore.read(key: "userId");


  List<Post> list;

  String link = baseURL + fetchTodoByDateUrl;
//  print("printing from get task = $setter");
  Stopwatch stopwatchbefore = new Stopwatch()..start();


  var token = await secureStore.read(key: "userToken");
  print("PRINTING TOKEN FROM GETTASK = $token");
  var res = await http.post(link, headers: {"Accept": "application/json",
    "x-access-code" : token,
  }, body: {"date" : setter.toIso8601String(), "user_id" : id});

  print('fetch executed in ${stopwatchbefore.elapsed}');

  if (res.statusCode == 200 && i == 1) {
    Stopwatch stopwatchafter = new Stopwatch()..start();

    var data = json.decode(res.body);
    var rest = data["data"] as List;

    list = rest.map<Post>((json) {

      return Post.fromJsonCreate(json);
    }).toList();

    print("PRINTING GETTASK BY DATE RESBODY = ${res.body}");
    print('statuscode executed in ${stopwatchafter.elapsed}');
//    Future.delayed(Duration(seconds: 5));
    return list;
  }
//  else   if (res.statusCode == 200 && i == 2) {
//    Stopwatch stopwatchafter = new Stopwatch()..start();
//
//    var data = json.decode(res.body);
//    var rest = data["data"] as List;
//
//
//    list = rest.map<Post>((json) {
////      print("Printing JSON = ${json + 1 }");
//
//      return Post.fromJsonCreate(json);
//    }).where((i) => i.markComplete == "true").toList();
////    print("PRINTING LIST = ${res.body}");
//    print('statuscode executed in ${stopwatchafter.elapsed}');
//    print("PRINTING LIST = ${res.body}");
////    Future.delayed(Duration(seconds: 5));
//    return list;
//  }
}

bool navigateToTaskCreate = false;

Future<List<Post>> fetchTodoDetails(dynamic TaskId) async {
  List<Post> list;
  Stopwatch stopwatchbefore = new Stopwatch()..start();
String id = await secureStore.read(key: "userId");

  String link = baseURL + getTodoDetailsUrl;
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM FETCH TODO DETAILS = $token");
  var res = await http.post(link, headers: {"Accept": "application/json",
    "x-access-code" : token
  }, body: {"todo_card_id" : TaskId, "user_id" : id});



    var data = json.decode(res.body);
    var rest = data["data"] as List;
    list = rest.map<Post>((json) {

      return Post.fromJsonSubtasks(json);
    }).toList();
  print('fetch executed in ${stopwatchbefore.elapsed}');

    return list;

}

Future<Post> createPost(String url, {Map body}) async {
//  BuildContext context;

  Stopwatch stopwatchbefore = new Stopwatch()..start();

//  print("PRIN")
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM CREATE POST = $token");
  return http.post(url, body: body,
      headers: {"x-access-code" : token}
      ).then((http.Response response) {

    if(response.statusCode == 401){
      showSnackBarForCreateTask = 401;
    }

    if(response.statusCode != 200){
      loadMore = false;
    }



    print("PRINTING RESPONSE = ${response.body}");
    final int statusCode = response.statusCode;
print("PRINTING STATUSCODE = $statusCode");
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }

    if(statusCode == 200){
      navigateToTaskCreate = true;
    }

    print('fetch executed in ${stopwatchbefore.elapsed}');

    return Post.fromJsonCreate(json.decode(response.body));
  });
}


Future<Post> deletePost(String url, dynamic taskid,{Map body}) async {
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM DELET POST = $token");

  var id = await secureStore.read(key: "userId");

  return http.post(url, body: {"todo_card_id" : taskid, "user_id" : id}, headers: {"x-access-code" : token}).then((http.Response response) {
    final int statusCode = response.statusCode;
    print(statusCode);
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }
    return Post.fromJsonUpdate(json.decode(response.body));
  });
}
//
//Future<Post> updatePostmarkComplete(String url, {Map body}) async {
//  return http.post(url, body: body).then((http.Response response) {
//    final int statusCode = response.statusCode;
//    print("PRINTING STATSU CODE FOR UPDATE = $statusCode");
//    if (statusCode < 200 || statusCode > 400 || json == null) {
//      throw new Exception("Error FOR UPDATE = $statusCode");
//    }
//    return Post.fromJsonMarkComplete(json.decode(response.body));
//  });
//}

Future<Post> updatePost(String url, {Map body}) async {
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM UPDATE POST = $token");

  return http.post(url, body: body, headers: {"x-access-code" : token}).then((http.Response response) {
    final int statusCode = response.statusCode;
    print("PRINTING STATUSCODE FOR UPDATE POST = ${statusCode}");
print("PRINTING RESBODY FOR UPDATE TASK = ${response.body}");
    if(response.statusCode == 401){
      showSnackBarForCreateTask = 401;
    }

    if(statusCode != 200){
      loadMore = false;
    }

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }


    return Post.fromJsonUpdate(json.decode(response.body));
  });
}

Future<Post> getTaskValue(int limit, page) async {
  String id = await secureStore.read(key: "userId");

  print("PRINTING USETOKEN FROM GET TASK VALUE = $id");

  String token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM GET TASK VALUE = $token");

  print("PRINTING GETTASK VALUE LINK -= ${baseURL + getTodoInGroupScreenURL(id)}");
  return http.get(baseURL + getTodoInGroupScreenURL(id),  headers: {"Accept": "application/json", "x-access-code" : jsonEncode(token)}).then((http.Response response) {
    final int statusCode = response.statusCode;
    print(statusCode);
    print("PRINTING RESBODY FOR Fetch todo list group screen = ${response.body}");
//    if(response.statusCode == 401){
//      showSnackBarForCreateTask = 401;
//    }

//    if(statusCode != 200){
//      loadMore = false;
//    }

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error = $statusCode");
    }


    return Post.fromJsonCreate(json.decode(response.body));
  });
}


Future<List<Post>> getTaskInGroupScreen(int limit, page) async {

var id = await secureStore.read(key: "userId");
  List<Post> list;

  String link = baseURL + fetchTodoByDateUrl;
//  print("printing from get task = $setter");
  Stopwatch stopwatchbefore = new Stopwatch()..start();
  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM GET TASK IN GROUP SCREEN = $token");
  print("PRINTING LINK SENT IN GETTASKINGROUPSCREEN = https://lafqwzv955.execute-api.ap-south-1.amazonaws.com/dev/v1.0/todos/get_todos_list?limit=$limit&page=$page&user_id=$id");
  var res = await http.get("https://lafqwzv955.execute-api.ap-south-1.amazonaws.com/dev/v1.0/todos/get_todos_list?limit=$limit&page=$page&user_id=$id", headers: {"Accept": "application/json", "x-access-code" : token}, );
  print("PRINTING LIST FOR ALL TASKS = ${res.body}");

  print('fetch executed in ${stopwatchbefore.elapsed}');

  if (res.statusCode == 200 ) {
    Stopwatch stopwatchafter = new Stopwatch()..start();

    var data = json.decode(res.body);
    var rest = data["data"] as List;
print("PRINTING LIST FOR ALL TASKS = $list");
    list = rest.map<Post>((json) {

      return Post.fromJsonCreate(json);
    }).toList();
    print('statuscode executed in ${stopwatchafter.elapsed}');
    Future.delayed(Duration(seconds: 2));
    return list;
  }
//  else
//    if (res.statusCode == 200 && i == 2) {
//    Stopwatch stopwatchafter = new Stopwatch()..start();
//
//    var data = json.decode(res.body);
//    var rest = data["data"] as List;
//
//
//    list = rest.map<Post>((json) {
//      return Post.fromJson(json);
//    }).where((i) => i.markComplete == "true").toList();
////    print("PRINTING LIST = ${res.body}");
//    print('statuscode executed in ${stopwatchafter.elapsed}');
//    print("PRINTING LIST FOR ALL TODOS = ${res.body}");
////    Future.delayed(Duration(seconds: 5));
//    return list;
//  }
}
/// ============================================================================================================================

Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));

//String welcomeToJson(Welcome data) => json.encode(data.toJson());

class Welcome {
  dynamic error;
  dynamic message;
  List<DatumCalendar> data;
  dynamic totalCount;
  dynamic skip;

  Welcome({
    this.error,
    this.message,
    this.data,
    this.totalCount,
    this.skip,
  });

  factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
    error: json["error"],
    message: json["message"],
    data: List<DatumCalendar>.from(json["data"].map((x) => DatumCalendar.fromJson(x))),
    totalCount: json["total_count"],
    skip: json["skip"],
  );

  Map<String, dynamic> toJson() => {
    "error": error,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "total_count": totalCount,
    "skip": skip,
  };
}

DatumCalendar welcomeFromJsonDatumCalendar(String str) => DatumCalendar.fromJson(json.decode(str));


class DatumCalendar {
  dynamic status;
  dynamic  id;
  dynamic   groupId;
  DateTime date;
  dynamic   title;
  dynamic    priority;
  dynamic   description;

  DatumCalendar({
    this.status,
    this.id,
    this.groupId,
    this.date,
    this.title,
    this.priority,
    this.description,
  });

  factory DatumCalendar.fromJson(Map<String, dynamic> json) => DatumCalendar(
    status: json["status"],
    id: json["_id"],
    groupId: json["group_id"],
    date: DateTime.parse(json["date"]),
    title: json["title"],
    priority: json["priority"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "_id": id,
    "group_id": groupId,
    "date": date.toIso8601String(),
    "title": title,
    "priority": priority,
    "description": description,
  };
}

Future<DatumCalendar> fetchPostCalendar({Map body}) async {
  Stopwatch stopwatchbefore = new Stopwatch()..start();

  var token = await secureStore.read(key: "userToken");
  print("PRINTING USETOKEN FROM fetch post calendar = $token");

  final response =
  await http.post(fetchTodoInCalendarUrl, body: body, headers: {"x-access-code" : token});
  print('fetch CALENDAR executed in ${stopwatchbefore.elapsed}');

  if(response.statusCode != 200){
//    loadMoreGroup = false;
  }
  if (response.statusCode == 200) {


    Stopwatch stopwatchafter = new Stopwatch()..start();
//    print("PRINTING RESBODY FOR FETCHPOSTCALENDAR IN PSOT CLASS>DART = ${response.body}");

    print('statuscode CALENDR executed in ${stopwatchafter.elapsed}');
    // If server returns an OK response, parse the JSON.
    return DatumCalendar.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post = error = ${response.statusCode}, BODY = ${response.body}');
  }
}


//




///============================================================================================================================================
//Event eventFromJson(String str) => Event.fromJson(json.decode(str));
//
////String eventToJson(Event data) => json.encode(data.toJson());
//
//class Event {
//  String error;
//  String message;
//  List<Datum> data;
//
//
//  Event({
//    this.error,
//    this.message,
//    this.data,
//  });
//
//  factory Event.fromJson(Map<String, dynamic> json) => Event(
//    error: json["error"],
//    message: json["message"],
//    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
//
//  );
//
//  Map<String, dynamic> toJson() => {
//    "error": error,
//    "message": message,
//    "data": List<dynamic>.from(data.map((x) => x.toJson())),
//  };
//
//  static List<Event> fromJsonList(jsonList) {
//    return jsonList.map<Event>((obj) => Event.fromJson(obj)).toList();
//  }
//}
//
//
//class DatumList {
//  dynamic status;
//  dynamic id;
//  String groupId;
//  DateTime date;
//  dynamic title;
//  int priority;
//  String description;
//  List<dynamic> tasks;
//  dynamic createdDate;
////  dynamic v;
//
//  DatumList({
//    this.status,
//    this.id,
////    this.groupId,
////    this.date,
//    this.title,
////    this.priority,
////    this.description,
////    this.tasks,
//    this.createdDate,
////    this.v,
//  });
//
//  factory DatumList.fromJson(Map<String, dynamic> json) => DatumList(
//    status: json["status"],
//    id: json["_id"],
////    groupId: json["group_id"],
////    date: DateTime.parse(json["date"]),
//    title: json["name"],
////    priority: json["priority"],
////    description: json["description"],
////    tasks: List<dynamic>.from(json["tasks"].map((x) => x)),
//    createdDate: DateTime.parse(json["created_date"]),
////    v: json["__v"],
//  );
//
//  Map<String, dynamic> toJson() => {
//    "status": status,
//    "_id": id,
////    "group_id": groupId,
////    "date": date.toIso8601String(),
//    "title": title,
////    "priority": priority,
////    "description": description,
////    "tasks": List<dynamic>.from(tasks.map((x) => x)),
//    "created_date": createdDate,
////    "__v": v,
//  };
//}



///==========================================================================================================================================

//String jsonmodelToJson(Jsonmodel data) => json.encode(data.toJson());
//Data dataFromJson(String str) => Data.fromJson(json.decode(str));
//
//String dataToJson(Data data) => json.encode(data.toJson());
//
//class Data {
//  String error;
//  String message;
//  List<Datum> data;
//
//  Data({
//    this.error,
//    this.message,
//    this.data,
//  });
//
//  factory Data.fromJson(Map<String, dynamic> json) => Data(
//    error: json["error"],
//    message: json["message"],
//    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
//  );
//
//  Map<String, dynamic> toJson() => {
//    "error": error,
//    "message": message,
//    "data": List<dynamic>.from(data.map((x) => x.toJson())),
//  };
//}
//
//class Datum {
//  dynamic status;
//dynamic      id;
//dynamic      groupId;
//dynamic    date;
//dynamic    title;
//dynamic      priority;
//dynamic      description;
//  List<dynamic>       tasks;
//dynamic    createdDate;
//dynamic    v;
//
//  Datum({
//    this.status,
//    this.id,
//    this.groupId,
//    this.date,
//    this.title,
//    this.priority,
//    this.description,
//    this.tasks,
//    this.createdDate,
//    this.v,
//  });
//
//  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
//    status: json["status"],
//    id: json["_id"],
//    groupId: json["group_id"],
//    date: DateTime.parse(json["date"]),
//    title: json["title"],
//    priority: json["priority"],
//    description: json["description"],
//    tasks: List<dynamic>.from(json["tasks"].map((x) => x)),
//    createdDate: DateTime.parse(json["created_date"]),
//    v: json["__v"],
//  );
//
//  Map<String, dynamic> toJson() => {
//    "status": status,
//    "_id": id,
//    "group_id": groupId,
//    "date": date.toIso8601String(),
//    "title": title,
//    "priority": priority,
//    "description": description,
//    "tasks": List<dynamic>.from(tasks.map((x) => x)),
//    "created_date": createdDate.toIso8601String(),
//    "__v": v,
//  };
//}

//getCompleteTask() async {
//  var token = await secureStore.read(key: "userToken");
//  print("PRINTING USETOKEN FROM GET COMPLETE TASK = $token");
//  var res = await http.post(baseURL + fetchTodoByDateUrl, headers: {"Accept": "application/json",  "x-access-code" : token}, body: {"date" : setter.toString()});
//
//return res.body;
//}